state = {}

window.addEventListener("load", () => {
    esb_sync_wrapper = document.getElementById("ChangeESBMeterDataTrigger");
    if(esb_sync_wrapper != null) {
        esb_sync_input = document.getElementById("changeESBMeterDataCheckbox");
        esb_sync_input.addEventListener('change', blockUnblockESBInputs)
    }
    state['id_esb_meter_id'] = document.getElementById("id_esb_meter_id").value
    state['id_esb_meter_contract_id'] = document.getElementById("id_esb_meter_contract_id").value
})

function blockUnblockESBInputs() {
    esb_sync_input = document.getElementById("changeESBMeterDataCheckbox");
    esb_meter_id = document.getElementById("id_esb_meter_id");
    esb_meter_contract_id = document.getElementById("id_esb_meter_contract_id");

    if(esb_sync_input.checked) {
        swal({
            title: "¿Estás seguro?",
            text: "Los cambios de los datos de Blockchain puede provocar la inconsestencia de los datos",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
          },
          function(isConfirm) {
            if (isConfirm) {
              swal({
                        html: true,
                        title:"Condiciones",
                        text:  "<b>Meter Id:</b> Ya tiene que estar creado <br>"
                               + "<b>Meter-Contract Id:</b>"
                               + " Id de contrato que permite a Bettergy (EnergySequence) registrar los consumos y las recomendaciones",
                        type: "warning"
                    });
              esb_meter_id.disabled = false;
              esb_meter_contract_id.disabled = false;
              esb_meter_id.required = true
              esb_meter_contract_id.required = true
            } else {
              esb_sync_input.checked = false
              swal("Cancelado", "Hemos deseleccionado esta acción por ti", "error");

            }
          });
    } else {
        esb_meter_id.disabled = true;
        esb_meter_contract_id.disabled = true;
        esb_meter_id.required = false
        esb_meter_contract_id.required = false
        document.getElementById("id_esb_meter_id").value = state['id_esb_meter_id']
        document.getElementById("id_esb_meter_contract_id").value = state['id_esb_meter_contract_id']
    }

}
