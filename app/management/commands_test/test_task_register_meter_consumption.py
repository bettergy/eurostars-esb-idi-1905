from unittest.mock import patch
import django
from parameterized import parameterized
from app.management.commands.task_register_meter_consumption import Command, MeterConsumption
from app.management.commands_test.models.dummy_meter_consumption_model import DummyOneHourMeterConsumptionModel, \
    DummyFifteenMinutesMeterConsumptionModel, DummyOneHourMeterConsumptionModelQuarterTo, \
    DummyOneHourMeterConsumptionModelWithTwoConsumptionsThatHasValueZero, \
    DummyFifteenMinutesMeterConsumptionModelWithTwoConsumptionsThatHasValueZero, \
    DummyOneHourMeterConsumptionModelWithTwoConsumptionsThatHasNegativeValues, \
    DummyFifteenMinutesMeterConsumptionModelWithTwoConsumptionThatHasNegativeValues, \
    DummyOneHourMeterConsumptionModelForDailyRegistration, \
    DummyFifteenMinutesMeterConsumptionModelForDailyRegistration, \
    DummyOneHourMeterConsumptionModelWithTwoConsumptionsThatHasValueZeroForDailyRegistration, \
    DummyFifteenMinutesMeterConsumptionModelWithTwoConsumptionsThatHasValueZeroForDailyRegistration, \
    DummyOneHourMeterConsumptionModelWithTwoConsumptionsThatHasNegativeValuesForDailyRegistration, \
    DummyFifteenMinutesModelWithTwoConsumptionsThatHasNegativeValuesZeroForDailyRegistration
from app.management.commands_test.models.meter import MeterTestModel
from core.devices.models import SupplyPoint
from estests import MongoTestCase


class TestTaskRegisterMeterConsumption(MongoTestCase):
    CLEAR_CACHE = True

    @classmethod
    def setUpClass(cls) -> None:
        super(TestTaskRegisterMeterConsumption, cls).setUpClass()
        django.setup()

    @parameterized.expand([
        (DummyOneHourMeterConsumptionModel(), MeterTestModel.get_default_meter()),
        (DummyFifteenMinutesMeterConsumptionModel(), MeterTestModel.get_default_meter()),
        (DummyOneHourMeterConsumptionModelWithTwoConsumptionsThatHasValueZero(), MeterTestModel.get_default_meter()),
        (DummyOneHourMeterConsumptionModelWithTwoConsumptionsThatHasNegativeValues(), MeterTestModel.get_default_meter()),
        (DummyFifteenMinutesMeterConsumptionModelWithTwoConsumptionsThatHasValueZero(), MeterTestModel.get_default_meter()),
        (DummyFifteenMinutesMeterConsumptionModelWithTwoConsumptionThatHasNegativeValues(), MeterTestModel.get_default_meter()),
        # TODO: Should be uncommented when get_data method will be fixed
        #(DummyOneHourMeterConsumptionModelQuarterTo(), MeterTestModel.get_default_meter())
    ])
    @patch('app.management.commands.task_register_meter_consumption.get_data')
    def test_should_return_valid_data_for_history_of_consumption(self, dummy_df, dummy_meter, mock_get_data):
        # Arrange
        dummy_df.generate_data()
        mock_get_data.return_value = dummy_df.get_df()
        inserted_meter = self.db.meter.insert_one(dummy_meter)
        meter = SupplyPoint.objects.get(id=inserted_meter.inserted_id)
        esb_meter_id = meter.esb_meter_id

        # Act
        result: [MeterConsumption] = Command().get_history_of_consumption(meter)

        # Assert
        self.assertEqual(esb_meter_id in result[0].meter_id, True)
        self.assertEqual(len(result[0].consumption), len(dummy_df.dates))
        for expected_date in dummy_df.dates:
            self.assertEqual(expected_date in result[0].consumption, True)
        self.assertEqual(result[0].consumption[dummy_df.dates[0]][0].consumption,
                         dummy_df.expected_consumption_of_first_portion())

        for date in result[0].consumption.keys():
            portions = len(result[0].consumption[date])
            self.assertEqual(portions, dummy_df.expected_portion)

    @parameterized.expand([
        (DummyOneHourMeterConsumptionModelForDailyRegistration(), MeterTestModel.get_default_meter()),
        (DummyFifteenMinutesMeterConsumptionModelForDailyRegistration(), MeterTestModel.get_default_meter()),
        (DummyOneHourMeterConsumptionModelWithTwoConsumptionsThatHasValueZeroForDailyRegistration(), MeterTestModel.get_default_meter()),
        (DummyFifteenMinutesMeterConsumptionModelWithTwoConsumptionsThatHasValueZeroForDailyRegistration(), MeterTestModel.get_default_meter()),
        (DummyOneHourMeterConsumptionModelWithTwoConsumptionsThatHasNegativeValuesForDailyRegistration(), MeterTestModel.get_default_meter()),
        (DummyFifteenMinutesModelWithTwoConsumptionsThatHasNegativeValuesZeroForDailyRegistration(), MeterTestModel.get_default_meter())
    ])
    @patch('app.management.commands.task_register_meter_consumption.get_data')
    def test_should_return_valid_data_for_daily_consumption_registration(self, dummy_df, dummy_meter, mock_get_data):
        # Arrange
        dummy_df.generate_data()
        mock_get_data.return_value = dummy_df.get_df()
        inserted_meter = self.db.meter.insert_one(dummy_meter)
        meter = SupplyPoint.objects.get(id=inserted_meter.inserted_id)
        esb_meter_id = meter.esb_meter_id

        # Act
        result: [MeterConsumption] = Command().get_daily_consumptions([meter])

        # Assert
        self.assertEqual(esb_meter_id in result[0].meter_id, True)
        self.assertEqual(len(result[0].consumption), len(dummy_df.dates))
        for expected_date in dummy_df.dates:
            self.assertEqual(expected_date in result[0].consumption.keys(), True)
        self.assertEqual(result[0].consumption[dummy_df.dates[0]][0].consumption,
                         dummy_df.expected_consumption_of_first_portion())

        for date in result[0].consumption.keys():
            portions = len(result[0].consumption[date])
            self.assertEqual(portions, dummy_df.expected_portion)
