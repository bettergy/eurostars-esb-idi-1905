class MeterTestModel:

    @staticmethod
    def get_default_meter():
        """This method can be extended but NOT modified"""
        return {
            "_cls": "Meter.SupplyPoint",
            "name": "DefaultSupplyPoint",
            "esb_meter_id": "esb_meter_id_default_supply_point"
        }
