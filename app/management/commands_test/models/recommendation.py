class RecommendationTestModels:

    # PvSelfConsumptionRecommendation
    @staticmethod
    def get_valid_pv_self_consumption_recommendation():
        return {
            "_cls": "Recommendation.PvInstallationRecommendation.PvSelfConsumptionRecommendation",
            "cost_savings": 1462.74,
            "params": {
                "recommended_peak_power": 10.0,
                "perc_reduction": 15.8,
                "date_range": "2019-06-02 a 2020-06-01",
                "quantity_of_data": 25.25,
                "energy_consumption_series": [
                    11626.0,
                    9447.0,
                    6126.0,
                    3257.0,
                    3413.0,
                    6327.0,
                    5409.0,
                    3564.0,
                    7291.0,
                    10061.0,
                    11294.0,
                    8869.0
                ],
                "energy_self_consumption_series": [
                    896.7291999999997,
                    1175.8738,
                    1238.186,
                    1027.5669000000003,
                    1161.6342999999997,
                    1294.7989000000007,
                    1505.443,
                    1218.7241999999992,
                    1357.973399999999,
                    923.2857999999998,
                    998.8233000000004,
                    895.3759000000001
                ],
                "payback_series": [
                    1235.25
                ],
                "irr_series": [
                    6.5,
                    6.5,
                    6.4,
                    6.7,
                    7.000000000000001,
                    7.1,
                    7.199999999999999,
                    7.199999999999999,
                    7.199999999999999,
                    7.3,
                    7.000000000000001,
                    6.6000000000000005,
                    6.4,
                    6.1,
                    5.8999999999999995
                ],
                "minimum_tir": 13.0,
                "recommended_irr": 7.3,
                "optimum_irr": 7.3,
                "optimum_peak_power": 10.0,
            }
        }

    @staticmethod
    def get_invalid_pv_self_consumption_recommendation():
        return {
            "_cls": "Recommendation.PvInstallationRecommendation.PvSelfConsumptionRecommendation",
            "params": {
                "recommended_peak_power": 10.0,
                "date_range": "2019-06-02 a 2020-06-01",
                "quantity_of_data": 25.25,
                "minimum_tir": 13.0,
                "recommended_irr": 7.3,
                "optimum_irr": 7.3,
                "optimum_peak_power": 10.0,
            }
        }
