import datetime
from typing import List
import pandas as pd
import enum
from django.core.management import BaseCommand
from core.devices.models import Meter
from datasources.esb.api_managers.meter_api_manager.models.public.consumption_data import ConsumptionData
from datasources.esb.api_managers.meter_api_manager.register_consumption.models.public.register_consumption_request import \
    RegisterConsumptionRequest
from datasources.esb.api_managers.meter_api_manager.register_consumption.models.public.register_consumption_response import \
    RegisterConsumptionSuccess, RegisterConsumptionFailure
from datasources.esb.api_managers.meter_api_manager.register_consumption.register_consumption_service import RegisterConsumptionService
from metering.data_read import get_data


class PossibleTimeResolution(enum.Enum):
    One_Hour = 'H'
    Fifteen_Minutes = '15T'


class PossibleRegisterTypes(enum.Enum):
    Daily = 'Daily'
    Historical = 'Historical'


class MeterConsumption:
    meter_id: str
    meter_contract_id: str
    consumption: {str, List[ConsumptionData]}

    def __init__(self, meter_id: str, meter_contract_id: str, consumption: {str, List[ConsumptionData]}):
        self.meter_id = meter_id
        self.meter_contract_id = meter_contract_id
        self.consumption = consumption


class Command(BaseCommand):
    prev_day: datetime.datetime

    def add_arguments(self, parser):
        parser.add_argument('-m', '--meter', type=str, required=False)

    def handle(self, *args, **options):
        try:
            meter_id = options['meter']
            meters_dict_consumptions: [MeterConsumption] = []
            if meter_id:
                meter = Meter.objects.get(id=meter_id)
                meters_dict_consumptions = self.get_history_of_consumption(meter)
            else:
                meters = Meter.objects.filter(esb_sync=True)
                meters_dict_consumptions = self.get_daily_consumptions(meters)

            for meter in meters_dict_consumptions:
                for date, consumption in meter.consumption.items():
                    if len(consumption) > 0:
                        self.register_consumptions_of_meter(meter.meter_id, meter.meter_contract_id, date, consumption)
        except Exception as e:
            print(str(e))

    def get_daily_consumptions(self, meters: [Meter]) -> List[MeterConsumption]:
        meters_dict_consumptions: List[MeterConsumption] = []
        for meter in meters:
            print(f' ---> ({meter.id})  {meter.name}')
            dict_consumptions: {str, [ConsumptionData]} = {}
            date_from, date_to, consumption_date = self.get_initial_dates(PossibleRegisterTypes.Daily)
            # date_from = datetime.datetime(2020, 9, 26)  # Test date_from
            # date_to = datetime.datetime(2020, 9, 27)  # Test date_to
            # consumption_date = date_from.date()  # Test consumption_date
            dict_consumptions[str(consumption_date)] = []

            data_frame: pd.DataFrame = get_data(meter, date_from, date_to)
            if not data_frame.empty and meter.esb_meter_id:
                time_resolution = self.get_time_resolution(data_frame.index.freqstr)
                portions: [] = data_frame["values"]
                print(f'  #### {len(portions)} portions')
                print(f'  #### {time_resolution.name}')
                for i in range(len(portions)):
                    end_portion_time = data_frame.index[i]

                    if i == 0 and self.is_midnight(end_portion_time):
                        continue

                    consumption = data_frame["values"][i]
                    start_portion_time = data_frame.index[i - 1]

                    if self.is_midnight(end_portion_time):
                        end_portion_time = self.remove_time(end_portion_time, seconds=1)

                    if consumption_date != end_portion_time.date():
                        consumption_date = end_portion_time.date()
                        dict_consumptions[str(consumption_date)] = []
                    consumption_data: ConsumptionData = self.create_consumption_data(start_portion_time,
                                                                                     end_portion_time,
                                                                                     consumption,
                                                                                     time_resolution)
                    if consumption_data.consumption > 0.0:
                        dict_consumptions[str(consumption_date)].append(consumption_data)
                    else:
                        print('Consumption is not greater than 0.0 '
                              '(that portion will not be registered in blockchain) \n')

            if dict_consumptions:
                meters_dict_consumptions.append(MeterConsumption(meter.esb_meter_id,
                                                                 meter.esb_meter_contract_id,
                                                                 dict_consumptions))
        return meters_dict_consumptions

    def get_history_of_consumption(self, meter) -> List[MeterConsumption]:
        meters_dict_consumptions: List[MeterConsumption] = []
        first_item = True
        meter_esb_id = meter.esb_meter_id
        dict_consumptions: {str, [ConsumptionData]} = {}
        print(f' ---> ({meter.id})  {meter.name}')
        date_from, date_to, consumption_date = self.get_initial_dates(PossibleRegisterTypes.Historical)

        data_frame: pd.DataFrame = get_data(meter, date_from, date_to)
        if not data_frame.empty and meter_esb_id:
            time_resolution = self.get_time_resolution(data_frame.index.freqstr)
            portions: [] = data_frame["values"]
            print(f'  #### {len(portions)} portions')
            print(f'  #### {time_resolution.name}')
            for i in range(len(portions)):
                end_portion_time = data_frame.index[i]

                consumption = data_frame["values"][i]
                start_portion_time = data_frame.index[i - 1]

                if first_item and self.is_midnight(end_portion_time):
                    first_item = False
                    continue
                elif first_item:
                    first_item = False
                    start_portion_time = self.calculate_start_portion_time(data_frame, i, time_resolution)

                if self.is_midnight(end_portion_time):
                    end_portion_time = self.remove_time(end_portion_time, seconds=1)

                if consumption_date != end_portion_time.date():
                    consumption_date = end_portion_time.date()
                    dict_consumptions[str(consumption_date)] = []
                consumption_data: ConsumptionData = self.create_consumption_data(start_portion_time,
                                                                                 end_portion_time,
                                                                                 consumption,
                                                                                 time_resolution)
                if consumption_data.consumption > 0.0:
                    dict_consumptions[str(consumption_date)].append(consumption_data)
                else:
                    print('Consumption is not greater than 0.0 (that portion will not be registered in blockchain) \n')

        if dict_consumptions:
            meters_dict_consumptions.append(MeterConsumption(meter.esb_meter_id,
                                                             meter.esb_meter_contract_id,
                                                             dict_consumptions))
        return meters_dict_consumptions

    @staticmethod
    def get_initial_dates(register_type: PossibleRegisterTypes) -> (datetime.datetime,
                                                                    datetime.datetime,
                                                                    datetime.date):
        current_date = datetime.datetime.now()
        date_to = datetime.datetime(current_date.year, current_date.month, current_date.day)
        date_from = current_date
        if register_type.name == PossibleRegisterTypes.Daily.name:
            date_from = date_to - datetime.timedelta(days=1)
        elif register_type.name == PossibleRegisterTypes.Historical.name:
            date_from = datetime.datetime(2000, 1, 1)
        consumption_date = datetime.datetime(date_from.year, date_from.month, date_from.day).date()
        return date_from, date_to, consumption_date

    def calculate_start_portion_time(self, data_frame: pd.DataFrame, i: int,
                                     time_resolution: PossibleTimeResolution) -> datetime.datetime:
        start_portion_time: datetime.datetime = data_frame.index[i]
        if time_resolution.name == PossibleTimeResolution.Fifteen_Minutes.name:
            start_portion_time = self.remove_time(start_portion_time, minutes=15)
        elif time_resolution.name == PossibleTimeResolution.One_Hour.name:
            start_portion_time = self.remove_time(start_portion_time, hours=1)
        return start_portion_time

    @staticmethod
    def remove_time(previous_portion_time: datetime.datetime,
                    hours: int = 0, minutes: int = 0, seconds: int = 0) -> datetime.datetime:
        return previous_portion_time - datetime.timedelta(hours=hours, minutes=minutes, seconds=seconds)

    @staticmethod
    def is_midnight(current_date: datetime.datetime) -> bool:
        return current_date.hour == 0 and current_date.minute == 0 and current_date.second == 0

    @staticmethod
    def get_time_resolution(frequent: str) -> PossibleTimeResolution:
        return PossibleTimeResolution(frequent)

    @staticmethod
    def create_consumption_data(datetime_from: datetime.datetime, datetime_to: datetime.datetime,
                                consumption: str, time_resolution: PossibleTimeResolution) -> ConsumptionData:
        start_time = int(datetime_from.replace(tzinfo=datetime.timezone.utc).timestamp())
        end_time = int(datetime_to.replace(tzinfo=datetime.timezone.utc).timestamp())
        print(f'start_portion_time:  {start_time}, {datetime.datetime.fromtimestamp(start_time, tz=datetime.timezone.utc)}')
        print(f'end_portion_time: {end_time}, {datetime.datetime.fromtimestamp(end_time, tz=datetime.timezone.utc)}')
        if time_resolution.name == PossibleTimeResolution.Fifteen_Minutes.name:
            consumption *= 4
        print(f'consumption: {float(consumption)} \n')
        return ConsumptionData(start_time, end_time, float(consumption))

    @staticmethod
    def register_consumptions_of_meter(meter_id: str, meter_contract_id: str, date: str, consumptions: [ConsumptionData]):
        request = RegisterConsumptionRequest(consumptions, meter_id, meter_contract_id)
        register_consumption_service = RegisterConsumptionService()
        response = register_consumption_service.register_consumption(request)
        if isinstance(response, RegisterConsumptionSuccess):
            print(f'##########  ({date}) Consumption of {meter_id} meter, registered Successfully #########')
        elif isinstance(response, RegisterConsumptionFailure):
            print(f'##########  ({date}) Error during registration of meter\'s consumption. Status code: {response.status_code}'
                  f', Reason: {response.status_str}, Additional info: {response.additional_info} #########')
