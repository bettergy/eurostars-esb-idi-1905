from django.forms import fields as d_fields
from django.utils.translation import ugettext_lazy as _
from mongodbforms import DocumentForm
from mongoengine import fields as m_fields

from core.tariff_system.common.models import Utility, ContractBase, TariffGroup


class SelectDataAttributes(d_fields.Select):

    def __init__(self, attrs=None, choices=(), data=None):
        super(SelectDataAttributes, self).__init__(attrs, choices)
        if data is None:
            data = {}
        self.data = data

    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):  # noqa
        option = super(SelectDataAttributes, self).create_option(name, value, label, selected, index, subindex=None,
                                                                 attrs=None)  # noqa
        # adds the data-attributes to the attrs context var
        for data_attr, values in self.data.items():
            option['attrs'][data_attr] = values[option['value']]

        return option


class ContractForm(DocumentForm):
    PERIODOS = ContractBase.PERIOD
    supply_point = d_fields.HiddenInput()
    comercializadoras = []
    retailer = m_fields.ListField(required=True, choices=[(f.id, str(f.name)) for f in comercializadoras])
    date_from = d_fields.DateField(widget=d_fields.DateInput(format='%d/%m/%Y', attrs={'class': 'form-control', 'id': 'dateSince', 'placeholder': 'Fecha inicio'}))
    date_to = d_fields.DateField(widget=d_fields.DateInput(format='%d/%m/%Y', attrs={'class': 'form-control', 'id': 'dateUntil', 'placeholder': 'Fecha fin'}))
    emission_coefficient = m_fields.FloatField(localize=True, required=False)
    simulated = m_fields.BooleanField(required=False)
    simulated_name = m_fields.StringField(label=_('Nombre de contrato simulado'), required=False)
    energy_prices = m_fields.ListField(required=False)


class TariffGroupForm(DocumentForm):
    TARIFF_MODE = TariffGroup.TARIFF_MODE
    utility = d_fields.HiddenInput()
    name = m_fields.StringField(label=_('Nombre de contrato simulado'), required=True)
    active = m_fields.BooleanField(label=_('Activo'), required=False)
    tariff = m_fields.ListField(required=True)
    min_consumption = m_fields.FloatField(required=False, default=- float("inf"))
    max_consumption = m_fields.FloatField(required=False, default=+float("inf"))
