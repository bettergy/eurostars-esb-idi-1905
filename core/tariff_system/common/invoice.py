from metering.data_read import get_data, get_zeros
from datetime import datetime, timedelta
from core.tariff_system.common.models import ContractBase, Tariff, Utility
import pandas as pd
from reportlab.lib.pagesizes import cm
from django.utils.translation import gettext as _
from core.formatters import *
from dateutil.relativedelta import relativedelta
import logging
logger = logging.getLogger(__name__)


class InvoiceHelper(object):

    contract_model = ContractBase

    def __init__(self, supply_point, date_from=None, date_to=None, energy=None, flag=True, contract=None):
        if supply_point:
            self.supply_point = supply_point

        if supply_point and date_from is not None and date_to is not None:
            self.date_from = date_from
            self.date_to = date_to
            if contract:
                self.contract = contract
            else:
                self.contract = self.supply_point.get_current_contract(date_from, date_to)

            if flag and energy is None or flag and energy.empty:
                energy = get_data(supply_point, date_from, date_to)
            self.energy = energy

    def get_tariff_types(self):
        contract_types = list(dict(self.contract_model.CONTRACT_TYPE).keys())
        tariffs = Tariff.objects.filter(contract_type__in=contract_types).order_by("name")
        return tariffs

    def get_contract_forms(self, contract, nb_periods, current_form=None):
        pass

    def get_contract_form_with_post(self, supply_point, contract, post, contract_type):
        pass

    def clean_Field(self, datos, campo):
        if campo not in datos:
            return None

        if datos[campo] == '':
            datos[campo] = 0
        else:
            try:
                datos[campo] = float(datos[campo].replace(',', '.'))
            except:
                return None
        return datos[campo]

    def initialization_values(self, form, inputs, fields, nb_periods):
        for input in inputs:
            try:
                for i in range(1, nb_periods + 1):
                    form.fields[input % i].initial = fields[inputs.index(input)][i - 1]
            except Exception as e:
                print("Fallo en inicializar el campo " + input)
        return form

    def get_mean_energy_price(self, mode):
        mean_energy_price = None
        try:
            # not self.energy.empty
            if mode == 2:
                df = self.set_energy_prices()
                df['cost'] = df['values'] * df['prices']
                mean_energy_price = df['cost'].sum() / df["values"].sum()
            elif mode in [0, 1]:
                reports = self.get_invoices_in_period(mode)
                total_energy_period = energy_period = 0
                for report in reports:
                    total_energy_period += sum(report.details.total_energy_period)
                    energy_period += sum(report.details.energy_period)
                    mean_energy_price = total_energy_period / energy_period
        except Exception as e:
            logger.error(e)
        return mean_energy_price

    def get_real_energy_prices(self, mode):
        return None

    def get_invoices_in_period(self, mode):
        from core.reporting.models import Report
        from core.utils import get_invoices_by_building_id
        reports = None
        try:
            if mode == 0:
                origin = Report.REAL_INVOICE
            else:
                origin = Report.INVOICE_SIPS

            date_start = self.date_from
            date_end = date_start + relativedelta(day=1, months=+1)
            reports = get_invoices_by_building_id(self.supply_point.building, date_start, date_end, origin)
        except DoesNotExist:
            logger.error("No existe una factura en el rango de fechas {0} y {1} para el medidor {2}"
                          .format(str(self.date_from), str(self.date_to), self.supply_point.name))
        except Exception as e:
            logger.error(e)
        return reports

    def update_contract_field_retailer_by_user(self, form, utility_id):
        comercializadoras = Utility.objects(profile__in=[1, 5, 6, 7, 11, 12, 13, 15])
        comercializadoras = [(f.id, str(f.name)) for f in comercializadoras]
        form.fields["retailer"].choices = comercializadoras

        if utility_id and ('retailer' in form.fields):
            utility = Utility.objects.get(id=utility_id)
            if utility.name != "Bettergy":
                qs = Utility.objects.filter(name=utility.name)
                form.fields["retailer"].queryset = qs
                form.fields["retailer"].choices = qs.values_list('id', 'name')

            form.fields["retailer"].empty_label = None

        return form

    def update_contract_field_supply_point_by_id(self, form, supply_point_id):
        from core.devices.models import Meter
        qs = Meter.objects.filter(id=supply_point_id)
        form.fields["supply_point"].queryset = qs

        return form

    def update_contract_field_tariff_by_contract_type(self, form, contract_type):
        pass

    def update_contract_field_contract_type_by_utility(self, form, retailer_id):
        pass

    def get_tariff_scheduler(self):
        pass

    def get_nb_periods(self):
        pass

    def set_period(self, df):
        ts = self.get_tariff_scheduler()
        return ts.set_period(self.contract, df)

    def set_power_discount(self, cost, discount=None):
        pass

    def set_energy_discount(self, cost, discount=None):
        pass

    def get_energy_curve(self):
        return self.energy.copy()

    def get_power_curve(self, set_period=False):
        df_power = self.energy.copy()
        # TODO 1 find a better way to ensure the time resolution, for example adapt this:
        # resolution = "H" if df.index.resolution == "hour" else "15T"
        if not df_power.empty:
            df_power = self.set_period(df_power, power=1) if set_period else df_power
            time_between_dates = df_power.index[1] - df_power.index[0]
            multiplier = timedelta(hours=1) / time_between_dates
            df_power['values'] = df_power['values'] * multiplier
            return df_power
        else:
            return pd.DataFrame()

    def draw_contract_power(self, canvas, contracted_tariff):
        pass

    def draw_tax(self, canvas, canva_height, details, total, Total3, request):
        return 0, canva_height

    def draw_rent(self, canvas, canva_height, details, request):
        return canva_height

    def draw_others_concepts(self, canvas, canva_height, details, request):
        canva_height = canva_height - 0.4
        canvas.setFont("Helvetica-Bold", 10)
        canvas.drawString(2 * cm, (canva_height) * cm, _('Otros conceptos'))
        canvas.drawRightString(19 * cm, (canva_height) * cm, (str(details.other_concepts or 0)))
        return canva_height

    def draw_tax_base(self, canvas, canva_height, details, total, electricity_tax, request):
        return 0, canva_height

    def draw_iva(self, canvas, canva_height, details, tax_base, request):
        return 0, canva_height

    def has_power_excess(self):
        return False

    def get_tariff_converter(self, **kwargs):
        return None
