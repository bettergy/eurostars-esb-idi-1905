import abc


class TariffGroupHelper(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def _update_tariff_field(self, form, tariff_mode):
        pass

    @abc.abstractmethod
    def get_tariff_mode(self):
        pass

    @abc.abstractmethod
    def get_form(self, post_data=None):
        pass
