from core.tariff_system.co.common.tariff import TariffSchedule as TS_co
from core.tariff_system.es.common.tariff import TariffSchedule as TS_es


class TariffScheduleFactory(object):

    ENGINE_TYPES = {
        'es_ES': (TS_es()),
        'es_CO': (TS_co()),
    }

    def get_handler(self, retailer):
        handler = self.ENGINE_TYPES.get(retailer.language.locale_name)
        return handler
