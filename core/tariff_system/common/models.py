from mongoengine import Document, EmbeddedDocument, EmbeddedDocumentListField
from mongoengine.fields import ListField, FloatField, IntField, EmbeddedDocumentField, \
    ReferenceField, StringField, DateTimeField, BooleanField, ImageField, DictField, URLField
# from app.catalog.models import ReportTemplate
from efficiency.models import UtilityParameterSet
from django.utils.translation import ugettext_lazy as _
from mongoengine.errors import ValidationError
from datetime import datetime

import numpy as np

import logging

logger = logging.getLogger(__name__)


class Market(Document):
    MARKETS = (
        (0, _('Mercado Cali')),
        (1, _('Mercado Valle')),
        (2, _('Mercado Nariño')),
        (3, _('Mercado Antioquia Unif')),
        (4, _('Mercado Bogota')),
        (5, _('Mercado Cauca')),
        (6, _('Mercado Boyaca')),
        (7, _('Mercado Caribe')),
        (8, _('Mercado Caldas')),
        (9, _('Mercado Cartago')),
        (10, _('Mercado Tolima')),
        (11, _('Mercado Pereira')),
        (12, _('Mercado Quindio')),
        (13, _('Mercado Cetsa')),
        (14, _('Mercado Santander'))
    )

    code = IntField(min_value=1, max_value=46)
    name = StringField(max_length=250, null=False, blank=False)
    operator = StringField(max_length=250, null=False, blank=False)

    def __str__(self):
        return '%s' % self.name


class MarketEmbedded(EmbeddedDocument):
    code = IntField(min_value=1, max_value=46)
    name = StringField(max_length=250, null=False, blank=False)
    operator = StringField(max_length=250, null=False, blank=False)
    reference = ReferenceField(Market, null=True, blank=True)

    def __str__(self):
        return '%s' % self.name

    def create(self, code, **kwargs):
        self.code = code
        if self.code:
            try:
                self.reference = Market.objects.get(code=int(self.code))
                self.name = self.code
                self.operator = self.reference.operator
                logger.info(self.reference.operator)
            except Exception as e:
                logger.info(e)


class Tariff(Document):
    CONTRACT_TYPE = (
        (0, _('Fijo')),
        (1, _('Indexado OMIE cuartohorario')),
        (2, _('Indexado OMIE horario')),
        (3, _('Indexado OMIE diario')),
        (4, _('Indexado OMIP mensual')),
        (5, _('Indexado OMIP trimestral')),
        (6, _('Indexado OMIP anual')),
        (7, _('Monomia Horaria')),
        (8, _('Monomia Simple')),
        (9, _('Regulated Transition')),
        (10, _('Regulated Social')),
        (11, _('Non-regulated')),
        (12, _('PVPC')),
    )

    name = StringField(null=True, blank=True)
    contract_type = IntField(choices=CONTRACT_TYPE, required=True, default=0)
    retailer = ReferenceField('Utility', null=True, blank=True)
    market = ReferenceField('Market', null=True, blank=True)
    tariff_market = EmbeddedDocumentField(MarketEmbedded, null=True, blank=True)
    voltage_level = StringField(null=True, blank=True)
    is_regulated = BooleanField(default=False)  # THIS IS UNIQUE OF COLOMBIA
    num_periods = IntField(min_value=1, max_value=24)
    num_periods_power = IntField(min_value=1, max_value=24)
    kp_coefficient = ListField(required=False)

    meta = {'collection': 'tariff', 'strict': False, 'allow_inheritance': True}

    def save(self, *args, **kwargs):
        if self.num_periods and self.num_periods_power is None:
            self.num_periods_power = self.num_periods
        super(Tariff, self).save(*args, **kwargs)

    @property
    def is_regulated_js(self):
        return int(self.is_regulated)

    def to_string(self):
        contract_type = self.CONTRACT_TYPE[self.contract_type][1]
        tariff_type = self.name
        return '%s' % contract_type + " " + tariff_type

    def __str__(self):
        return self.to_string()


class TariffPrices(Document):
    tariff = ReferenceField('Tariff', required=True)
    product = StringField(null=True, blank=True)
    date_from = DateTimeField()
    component = DictField(default={})

    def __str__(self):
        return '%s' % (self.tariff.name + " " + str(self.date_from))


class UtilityConfigurationSet(EmbeddedDocument):
    # configuration parameters
    pv_promotion_url = URLField(required=False, null=True, blank=True)
    ive_promotion_url = URLField(required=False, null=True, blank=True)
    sips_customer_key = StringField(required=False, null=True, blank=True)
    sips_customer_secret = StringField(required=False, null=True, blank=True)
    audinfor_server = StringField(required=False, null=True, blank=True)
    audinfor_database = StringField(required=False, null=True, blank=True)
    audinfor_username = StringField(required=False, null=True, blank=True)
    audinfor_password = StringField(required=False, null=True, blank=True)
    audinfor_curves_server = StringField(required=False, null=True, blank=True)
    audinfor_curves_database = StringField(required=False, null=True, blank=True)
    audinfor_curves_username = StringField(required=False, null=True, blank=True)
    audinfor_curves_password = StringField(required=False, null=True, blank=True)

    meta = {'strict': False}


class UtilityReportConfigurationSet(EmbeddedDocument):
    table_color = StringField(required=False, null=False, blank=False, default='#FFFFFF')
    table_background_color = StringField(required=False, null=False, blank=False, default="#028002")

    meta = {'strict': False}


class UtilityApisConfigurationSet(EmbeddedDocument):
    photovoltaic_service_report_template_id = StringField(required=False, null=True, blank=True, default=None)

    meta = {'strict': False}


class Utility(Document):
    VALIDATORS_UTILITY_PARAMETER_SET = {
        "es": {
            "default": {
                "default_formula": "({PHD} + {PC} + {POM} + {POS} + {PHI} + {INT} + {RP} + {BS} + {PBF} + {TR} + {SA} "
                                   "+ {SPO} + {SD} + {CD} + {FN}) * (1 + {TM}) * (1 + {PER}) + "
                                   "({FNE} + {FEE}) * (1 + {TM}) + {TA}",
                "maintenance_cost_ratio_PVA": 0.014,
                "decision_power_adjust": 1,
                "limit1_power_retailer": 10.0,
                "limit2_power_retailer": 100.0,
                "value1_retailer_fee": 3,
                "value2_retailer_fee": 3,
                "value3_retailer_fee": 3,
                "percent_grant_year": 0.0,
                "years_amortisation": 0,
                "price_over_energy_recharge": 0.05,
                "days_use_year": 365,
                "hours_use_day": 8,
                "rate_ocupation_slow_IVE": 1 / 20,
                "rate_surface_slow_IVE": 1 / 200,
                "rate_ocupation_fast_IVE": 1 / 40,
                "rate_surface_fast_IVE": 1 / 400,
                "first_year10_interest_rate": 1.8,
                "future_year10_interest_rate": 0.79,
                "first_year7_interest_rate": 1.2,
                "future_year7_interest_rate": 0.81,
                "cargo_libre_utility": 6.4,
                "porciento_beneficio_por_ahorro": 2.6,
                "power_modulePV": 310.0,
                "rate_surfacePV": 10.0,
                "module_guarantee": 25,
                "investor_guarantee": 5,
                "pv_alert_low_consumption_thres": 25,
                "pv_alert_low_production_thres": 70,
                "epc_costs": {},
                "energy_prices_international": {},
                "is_international": False,
                "fne": 0.0,
                "financial_cost_contrated_power": 0.0,
                'early_cancellation_discount_fixed_tariffs': 0.0,
                'early_cancellation_discount_until_30A': 0.0,
                'early_cancellation_discount_after_30A': 0.0,
                "min_peak_power": 1.0,
                "max_peak_power_without_energy_sold": 15,
                "max_peak_power_factor": 80,
                "renting_financing_years": 0,
                "renting_financing_int_rate": 0.0,
                "min_irr_for_pv": 200.0,
                "optimization_steps": 20,
                "degradation_first_year": 1.5,
                "degradation_next_years": 0.6,
                "discount_tax": 2,
                "CPI_increase": 2,
                "annual_increment_energy_price": 4,
                "annual_ibi_payment": 0,
                "commercial_margin_20A": 0.0,
                "commercial_margin_20DHA": 0.0,
                "commercial_margin_20DHS": 0.0,
                "commercial_margin_21A": 0.0,
                "commercial_margin_21DHA": 0.0,
                "commercial_margin_21DHS": 0.0,
                "commercial_margin_30A": 0.0,
                "commercial_margin_31A": 0.0,
                "commercial_margin_61A": 0.0,
                "commercial_margin_61B": 0.0,
                "commercial_margin_62": 0.0,
                "commercial_margin_63": 0.0,
                "commercial_margin_64": 0.0,
                "commercial_margin_65": 0.0,
                "commercial_margin_20TD": 0.0,
                "commercial_margin_20TDA": 0.0,
                "commercial_margin_30TD": 0.0,
                "commercial_margin_30TDA": 0.0,
                "commercial_margin_30TDVE": 0.0,
                "commercial_margin_61TD": 0.0,
                "commercial_margin_62TD": 0.0,
                "commercial_margin_63TD": 0.0,
                "commercial_margin_64TD": 0.0,
                "commercial_margin_61TDA": 0.0,
                "commercial_margin_62TDA": 0.0,
                "commercial_margin_63TDA": 0.0,
                "commercial_margin_64TDA": 0.0,
                "commercial_margin_61TDVE": 0.0,
                "commercial_margin_62TDVE": 0.0,
                "commercial_margin_63TDVE": 0.0,
                "commercial_margin_64TDVE": 0.0,
                "fixed_energy_sold_price": 0.05,
                "margin_energy_sold_price": 0.005,
                "utility_construction_tax": 3.0,
                "epc_costs_iva": 21,
                "edop_t3_normal": True,
                "edop_t6_normal": True,
                "peak_power_excess_percentage": 10.0,
                # Visalia Model 3 params:
                "roof_rent": 0.0,
                # End of Visalia model 3 params
            },
            "units": {
                "maintenance_cost_ratio_PVA": "{0}/Wp",
                "decision_power_adjust": "",
                "limit1_power_retailer": "kW",
                "limit2_power_retailer": "kW",
                "value1_retailer_fee": "%",
                "value2_retailer_fee": "%",
                "value3_retailer_fee": "%",
                "percent_grant_year": "%",
                "years_amortisation": "Años",
                "price_over_energy_recharge": "{0}/kWh",
                "days_use_year": "días/año",
                "hours_use_day": "horas/día",
                "rate_ocupation_slow_IVE": "punto recarga/usuario",
                "rate_surface_slow_IVE": "punto recarga/\u33A1",
                "rate_ocupation_fast_IVE": "punto recarga/usuario",
                "rate_surface_fast_IVE": "punto recarga/\u33A1",
                "first_year10_interest_rate": "%",
                "future_year10_interest_rate": "%",
                "first_year7_interest_rate": "%",
                "future_year7_interest_rate": "%",
                "cargo_libre_utility": "%",
                "porciento_beneficio_por_ahorro": "%",
                "power_modulePV": "	W",
                "rate_surfacePV": "\u33A1/kW",
                "module_guarantee": "",
                "investor_guarantee": "",
                "pv_alert_low_consumption_thres": "%",
                "pv_alert_low_production_thres": "%",
                "fne": "{0}/MWh",
                "financial_cost_contrated_power": '%',
                'early_cancellation_discount_fixed_tariffs': '%',
                'early_cancellation_discount_until_30A': '%',
                'early_cancellation_discount_after_30A': "{0}/kWh",
                "min_peak_power": "kWp",
                "max_peak_power_without_energy_sold": "kWp",
                "max_peak_power_factor": "%",
                "renting_financing_years": "Años",
                "renting_financing_int_rate": "%",
                "min_irr_for_pv": "%",
                "degradation_first_year": "%",
                "degradation_next_years": "%",
                "discount_tax": "%",
                "CPI_increase": "%",
                "annual_increment_energy_price": "%",
                "annual_ibi_payment": "€",
                "commercial_margin_20A": "",
                "commercial_margin_20DHA": "",
                "commercial_margin_20DHS": "",
                "commercial_margin_21A": "",
                "commercial_margin_21DHA": "",
                "commercial_margin_21DHS": "",
                "commercial_margin_30A": "",
                "commercial_margin_31A": "",
                "commercial_margin_61A": "",
                "commercial_margin_61B": "",
                "commercial_margin_62": "",
                "commercial_margin_63": "",
                "commercial_margin_64": "",
                "commercial_margin_65": "",
                "commercial_margin_20TD": "",
                "commercial_margin_20TDA": "",
                "commercial_margin_30TD": "",
                "commercial_margin_30TDA": "",
                "commercial_margin_30TDVE": "",
                "commercial_margin_61TD": "",
                "commercial_margin_62TD": "",
                "commercial_margin_63TD": "",
                "commercial_margin_64TD": "",
                "commercial_margin_61TDA": "",
                "commercial_margin_62TDA": "",
                "commercial_margin_63TDA": "",
                "commercial_margin_64TDA": "",
                "commercial_margin_61TDVE": "",
                "commercial_margin_62TDVE": "",
                "commercial_margin_63TDVE": "",
                "commercial_margin_64TDVE": "",
                "fixed_energy_sold_price": "{0}/kWh",
                "margin_energy_sold_price": "{0}/kWh",
                "utility_construction_tax": "%",
                "epc_costs_iva": "%",
                "peak_power_excess_percentage": "%",
                # Visalia model 3 params:
                "roof_rent": "{0}/kWp",
                "five_years_price_20TD_P1": "",
                "five_years_price_20TD_P2": "",
                "five_years_price_20TD_P3": "",
                "ten_years_price_20TD_P1": "",
                "ten_years_price_20TD_P2": "",
                "ten_years_price_20TD_P3": "",
                "five_years_price_30TD_P1": "",
                "five_years_price_30TD_P2": "",
                "five_years_price_30TD_P3": "",
                "five_years_price_30TD_P4": "",
                "five_years_price_30TD_P5": "",
                "five_years_price_30TD_P6": "",
                "ten_years_price_30TD_P1": "",
                "ten_years_price_30TD_P2": "",
                "ten_years_price_30TD_P3": "",
                "ten_years_price_30TD_P4": "",
                "ten_years_price_30TD_P5": "",
                "ten_years_price_30TD_P6": "",
                # End of Visalia model 3 params
            },
            "min": {
                "maintenance_cost_ratio_PVA": 0,
                "decision_power_adjust": 0,
                "limit1_power_retailer": 0,
                "limit2_power_retailer": 0,
                "value1_retailer_fee": 0,
                "value2_retailer_fee": 0,
                "value3_retailer_fee": 0,
                "percent_grant_year": 0,
                "years_amortisation": 0,
                "price_over_energy_recharge": 0,
                "days_use_year": 0,
                "hours_use_day": 0,
                "rate_ocupation_slow_IVE": 0,
                "rate_surface_slow_IVE": 0,
                "rate_ocupation_fast_IVE": 0,
                "rate_surface_fast_IVE": 0,
                "first_year10_interest_rate": 0,
                "future_year10_interest_rate": 0,
                "first_year7_interest_rate": 0,
                "future_year7_interest_rate": 0,
                "cargo_libre_utility": 0,
                "porciento_beneficio_por_ahorro": 0,
                "power_modulePV": 0,
                "rate_surfacePV": 0,
                "pv_alert_low_consumption_thres": 0,
                "pv_alert_low_production_thres": 0,
                "module_guarantee": 0,
                "investor_guarantee": 0,
                "min_peak_power": 0.2,
                "max_peak_power_without_energy_sold": 0,
                "max_peak_power_factor": 0.0,
                "renting_financing_int_rate": 0,
                "min_irr_for_pv": 0,
                "optimization_steps": 10,
                "degradation_first_year": 0,
                "degradation_next_years": 0,
                "discount_tax": 0,
                "CPI_increase": 0,
                "annual_increment_energy_price": 0,
                "annual_ibi_payment": 0,
                "fixed_energy_sold_price": 0,
                "utility_construction_tax": 0,
                "epc_costs_iva": 0,
                "peak_power_excess_percentage": 0,
            },
            "max": {
                "value1_retailer_fee": 100.0,
                "value2_retailer_fee": 100.0,
                "value3_retailer_fee": 100.0,
                "pv_alert_low_consumption_thres": 100,
                "pv_alert_low_production_thres": 100,
                "renting_financing_int_rate": 100,
                "min_irr_for_pv": 200,
                "optimization_steps": 100000,
                "degradation_first_year": 100,
                "degradation_next_years": 100,
                "discount_tax": 100,
                "CPI_increase": 100,
                "annual_increment_energy_price": 100,
                "utility_construction_tax": 100,
                "epc_costs_iva": 100,
                "peak_power_excess_percentage": 100,
            },
        },
        "es-co": {
            "default": {
                "maintenance_cost_ratio_PVA": 67.5,
                "value1_retailer_fee": 3,
                "value2_retailer_fee": 3,
                "value3_retailer_fee": 3,
                "percent_grant_year": 0.0,
                "years_amortisation": 5,
                "price_over_energy_recharge": 0.04,
                "days_use_year": 365,
                "hours_use_day": 8,
                "rate_ocupation_slow_IVE": 1 / 30,
                "rate_surface_slow_IVE": 1 / 300,
                "rate_ocupation_fast_IVE": 1 / 50,
                "rate_surface_fast_IVE": 1 / 500,
                "first_year10_interest_rate": 8,
                "future_year10_interest_rate": 6,
                "first_year7_interest_rate": 7,
                "future_year7_interest_rate": 5.8,
                "power_modulePV": 310.0,
                "rate_surfacePV": 10.0,
                "min_peak_power": 1,
                "max_peak_power_without_energy_sold": 0,
                "max_peak_power_factor": 80,
                "renting_financing_years": 0,
                "renting_financing_int_rate": 0.0,
                "min_irr_for_pv": 200,
                "optimization_steps": 20,
                "degradation_first_year": 1.5,
                "degradation_next_years": 0.6,
                "discount_tax": 2,
                "CPI_increase": 2,
                "annual_increment_energy_price": 4,
                "annual_ibi_payment": 0,
                "utility_construction_tax": 0,
                "epc_costs_iva": 0,
            },
            "units": {
                "maintenance_cost_ratio_PVA": "{0}/Wp",
                "decision_power_adjust": "",
                "limit1_power_retailer": "kW",
                "limit2_power_retailer": "kW",
                "value1_retailer_fee": "%",
                "value2_retailer_fee": "%",
                "value3_retailer_fee": "%",
                "percent_grant_year": "%",
                "years_amortisation": "Años",
                "price_over_energy_recharge": "{0}/kWh",
                "days_use_year": "días/año",
                "hours_use_day": "horas/día",
                "rate_ocupation_slow_IVE": "punto recarga/usuario",
                "rate_surface_slow_IVE": "punto recarga/\u33A1",
                "rate_ocupation_fast_IVE": "punto recarga/usuario",
                "rate_surface_fast_IVE": "punto recarga/\u33A1",
                "first_year10_interest_rate": "%",
                "future_year10_interest_rate": "%",
                "first_year7_interest_rate": "%",
                "future_year7_interest_rate": "%",
                "cargo_libre_utility": "%",
                "porciento_beneficio_por_ahorro": "%",
                "power_modulePV": "	W",
                "rate_surfacePV": "\u33A1/kW",
                "module_guarantee": "",
                "investor_guarantee": "",
                "min_peak_power": "kWp",
                "max_peak_power_without_energy_sold": "kWp",
                "max_peak_power_factor": "%",
                "renting_financing_years": "Años",
                "renting_financing_int_rate": "%",
                "min_irr_for_pv": "%",
                "degradation_first_year": "%",
                "degradation_next_years": "%",
                "discount_tax": "%",
                "CPI_increase": "%",
                "annual_increment_energy_price": "%",
                "annual_ibi_payment": "€",
                "utility_construction_tax": "%",
                "epc_costs_iva": "%",
            },
            "min": {
                "maintenance_cost_ratio_PVA": 0,
                "decision_power_adjust": 0,
                "limit1_power_retailer": 0,
                "limit2_power_retailer": 0,
                "value1_retailer_fee": 0,
                "value2_retailer_fee": 0,
                "value3_retailer_fee": 0,
                "percent_grant_year": 0,
                "years_amortisation": 0,
                "price_over_energy_recharge": 0,
                "days_use_year": 0,
                "hours_use_day": 0,
                "rate_ocupation_slow_IVE": 0,
                "rate_surface_slow_IVE": 0,
                "rate_ocupation_fast_IVE": 0,
                "rate_surface_fast_IVE": 0,
                "first_year10_interest_rate": 0,
                "future_year10_interest_rate": 0,
                "first_year7_interest_rate": 0,
                "future_year7_interest_rate": 0,
                "cargo_libre_utility": 0,
                "porciento_beneficio_por_ahorro": 0,
                "power_modulePV": 0,
                "rate_surfacePV": 0,
                "module_guarantee": 0,
                "investor_guarantee": 0,
                "min_peak_power": 0.2,
                "max_peak_power_without_energy_sold": 0,
                "max_peak_power_factor": 0,
                "renting_financing_int_rate": 0,
                "min_irr_for_pv": 0,
                "optimization_steps": 10,
                "degradation_first_year": 0,
                "degradation_next_years": 0,
                "discount_tax": 0,
                "CPI_increase": 0,
                "annual_increment_energy_price": 0,
                "annual_ibi_payment": 0,
                "utility_construction_tax": 0,
                "epc_costs_iva": 0,
            },
            "max": {
                "value1_retailer_fee": 100.0,
                "value2_retailer_fee": 100.0,
                "value3_retailer_fee": 100.0,
                "percent_grant_year": 50,
                "years_amortisation": 5,
                "renting_financing_int_rate": 100,
                "min_irr_for_pv": 200,
                "optimization_steps": 100000,
                "degradation_first_year": 100,
                "degradation_next_years": 100,
                "discount_tax": 100,
                "CPI_increase": 100,
                "annual_increment_energy_price": 100,
                "utility_construction_tax": 100.0,
                "epc_costs_iva": 100,
            },
        }
    }

    PROFILES = (
        (1, _("Comercializadora")),
        (2, _("Distribuidora")),
        (3, _("Generadora")),
        (4, _("Canal")),
        (5, _("Comercializadora y distribuidora")),
        (6, _("Comercializadora y generadora")),
        (7, _("Comercializadora y canal")),
        (8, _("Distribuidora y generadora")),
        (9, _("Distribuidora y canal")),
        (10, _("Generadora y canal")),
        (11, _("Comercializadora, distribuidora y generadora")),
        (12, _("Comercializadora, distribuidora y canal")),
        (13, _("Comercializadora, generadora y canal")),
        (14, _("Distribuidora, generadora y canal")),
        (15, _("Comercializadora, distribuidora, generadora y canal")),
    )

    # If we add a new ACTIVITY_ORIGINS we have to update the ActivityStandard.ACTIVITY_CODE
    ACTIVITY_ORIGINS = (
        (1, _('Actividades de plataforma')),
        (2, _('CIIU')),
        (3, _('CNAE')),
    )

    SIPS_FIXED_CONTRACT = 0
    SIPS_INDEXED_CONTRACT = 1

    SIPS_TYPE_CONTRACT_GENERATION = (
        (SIPS_FIXED_CONTRACT, _('Fijo')),
        (SIPS_INDEXED_CONTRACT, _('Indexado')),
    )

    SIPS_CONTRACT_ENERGY_PRICES = {
        str(SIPS_FIXED_CONTRACT): {
            "20A": [0.11],
            "20DHA": [0.12, 0.06],
            "20DHS": [0.12, 0.07, 0.05],
            "21A": [0.12],
            "21DHA": [0.13, 0.07],
            "21DHS": [0.13, 0.08, 0.05],
            "30A": [0.11, 0.08, 0.05],
            "31A": [0.105, 0.075, 0.05],
            "61A": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "62": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "63": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "64": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "65": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            # NEW TARIFFS
            "20TD": [0.12, 0.07, 0.05],
            "20TDA": [0.12, 0.07, 0.05],

            "30TD": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "30TDA": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],

            "61TD": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "62TD": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "63TD": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "64TD": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "61TDA": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "62TDA": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "63TDA": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "64TDA": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],

            "30TDVE": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "61TDVE": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "62TDVE": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "63TDVE": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05],
            "64TDVE": [0.11, 0.08, 0.07, 0.06, 0.06, 0.05]
        },
        str(SIPS_INDEXED_CONTRACT): {}
    }

    MAXIMETER_DISTRIBUTION = 0
    NORMAL_DISTRIBUTION = 1
    TYPICAL_CURVE = 2
    REE_PROFILE = 3

    LOAD_PROFILING_TYPE = (
        (NORMAL_DISTRIBUTION, _('Aproximación de curva de carga simple')),
        (REE_PROFILE, _('Aproximación de curva de carga según REE')),
        # (TYPICAL_CURVE, _('Aproximación de curva de carga tipica')),
        # (MAXIMETER_DISTRIBUTION, _('Aproximación de curva de carga avanzada'))
    )

    name = StringField(max_length=250, required=False)
    is_enabled = BooleanField()
    description = StringField(required=False, null=True, blank=True)
    datalogger_id = StringField(required=True, null=True, blank=True)
    logo = ImageField(required=False)
    address = StringField(max_length=250, null=True, blank=True)
    phone = StringField(max_length=25, required=False, null=True, blank=True)
    email = StringField(max_length=250, required=False, null=True, blank=True)
    country = StringField(max_length=250, required=True, null=False, blank=False)
    language = ReferenceField('Language', required=True, null=True, blank=True)
    currency = ReferenceField('Currency', required=True, null=True, blank=True)
    profile = IntField(choices=PROFILES, required=True, null=False, blank=False)

    max_users = IntField(required=True, null=False, blank=False, default=3)  # Utility users
    max_client = IntField(required=True, null=False, blank=False, default=3)  # Clients of utility
    max_client_users = IntField(required=True, null=False, blank=False, default=3)  # Client users
    activity = IntField(choices=ACTIVITY_ORIGINS, required=False, default=1)
    is_demo = BooleanField('demo', default=False, required=False, null=False)

    # configuration parameters  # TODO Move this fields to EmbeddedDocument UtilityConfigurationSet
    pv_promotion_url = URLField(required=False, null=True, blank=True)
    ive_promotion_url = URLField(required=False, null=True, blank=True)
    power_bi_url = URLField(required=False, null=True, blank=True)
    metrics_power_bi_url = URLField(required=False, null=True, blank=True)
    commercial_calc_url = URLField(required=False, null=True, blank=True)
    sips_customer_key = StringField(required=False, null=True, blank=True)
    sips_customer_secret = StringField(required=False, null=True, blank=True)
    nemon_sips_client_url = StringField(required=False, null=True, blank=True)
    nemon_sips_client_token = StringField(required=False, null=True, blank=True)
    sips_contract_type = IntField(choices=SIPS_TYPE_CONTRACT_GENERATION, required=False, default=0)
    sips_contract_energy_prices = DictField(default=SIPS_CONTRACT_ENERGY_PRICES)
    audinfor_server = StringField(required=False, null=True, blank=True)
    audinfor_database = StringField(required=False, null=True, blank=True)
    audinfor_username = StringField(required=False, null=True, blank=True)
    audinfor_password = StringField(required=False, null=True, blank=True)
    audinfor_curves_server = StringField(required=False, null=True, blank=True)
    audinfor_curves_database = StringField(required=False, null=True, blank=True)
    audinfor_curves_username = StringField(required=False, null=True, blank=True)
    audinfor_curves_password = StringField(required=False, null=True, blank=True)
    contract_allow_pvpc = BooleanField(default=False)
    pv_reco_simple_mode = BooleanField(default=False)
    load_profiling_type = IntField(choices=LOAD_PROFILING_TYPE, required=False, default=NORMAL_DISTRIBUTION)

    parameters = EmbeddedDocumentField(UtilityParameterSet, default=UtilityParameterSet())
    configurations = EmbeddedDocumentField(UtilityConfigurationSet, default=UtilityConfigurationSet())
    configurationsReport = EmbeddedDocumentField(UtilityReportConfigurationSet,
                                                 default=UtilityReportConfigurationSet())
    configurationsApis = EmbeddedDocumentField(UtilityApisConfigurationSet, default=UtilityApisConfigurationSet())

    parent = ReferenceField('Utility', default=None)
    compute_metrics = BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.pv_promotion_url == "":
            self.pv_promotion_url = None
        if self.ive_promotion_url == "":
            self.ive_promotion_url = None
        if self.sips_customer_key == "":
            self.sips_customer_key = None
        if self.sips_customer_secret == "":
            self.sips_customer_secret = None
        if self.nemon_sips_client_url == "":
            self.nemon_sips_client_url = None
        if self.nemon_sips_client_token == "":
            self.nemon_sips_client_token = None
        if self.power_bi_url == "":
            self.power_bi_url = None
        if self.metrics_power_bi_url == "":
            self.metrics_power_bi_url = None
        if self.commercial_calc_url == "":
            self.commercial_calc_url = None
        # self.sips_contract_energy_prices = self.SIPS_CONTRACT_ENERGY_PRICES.get(str(self.sips_contract_type), {})
        super(Utility, self).save(*args, **kwargs)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_sige_authenticated(self):
        # It just checks if we have credentials available, it does not check if we have access
        return bool(self.audinfor_curves_database and self.audinfor_curves_password and self.audinfor_curves_server
                    and self.audinfor_curves_username)

    def __str__(self):
        return '%s' % self.name

    def get_sips_contract_type_name(self, sips_contract_type):
        return dict(self.SIPS_TYPE_CONTRACT_GENERATION).get(sips_contract_type, "")

    def get_load_profiling_type_name(self, load_profiling_type):
        return dict(self.LOAD_PROFILING_TYPE).get(load_profiling_type, "")

    def get_initials_from_code(self):
        if self.language:
            code = self.language.code
        else:
            return {}
        return self.VALIDATORS_UTILITY_PARAMETER_SET[code][
            "default"] if code in self.VALIDATORS_UTILITY_PARAMETER_SET else {}

    def get_units_from_code(self):
        formatting_func = [self.currency.code if self.currency else "-"]
        if self.language:
            code = self.language.code
        else:
            return {}
        units = self.VALIDATORS_UTILITY_PARAMETER_SET[code][
            "units"] if code in self.VALIDATORS_UTILITY_PARAMETER_SET else {}
        for u in units:
            units[u] = units[u].format(*formatting_func)

        return units

    def get_validators_from_code(self):
        if self.language:
            code = self.language.code
        else:
            return {}
        return self.VALIDATORS_UTILITY_PARAMETER_SET[code] if code in self.VALIDATORS_UTILITY_PARAMETER_SET else {}

    @property
    def sips_authenticated(self):
        return bool((self.sips_customer_key and self.sips_customer_secret) or (
            self.nemon_sips_client_url and self.nemon_sips_client_token))

    @property
    def is_utility_group(self):
        return False

    @property
    def grouped_utilities(self):
        from mongoengine.queryset.visitor import Q
        return Utility.objects(Q(**{'parent': self}) | Q(**{'id': self.id}))

    meta = {'strict': False}


class AgentRelations(EmbeddedDocument):
    UTILITY = 1
    AGENT = 2
    SUBAGENT = 3

    AGENT_TYPE = (
        (UTILITY, 'Distribuidora'),
        (AGENT, 'Agente'),
        (SUBAGENT, 'Subagente'),
    )

    parent = ReferenceField('Agent', null=True, blank=True)
    a_type = IntField(choices=AGENT_TYPE, default=2)
    start_date = DateTimeField(null=True, blank=True)
    end_date = DateTimeField(null=True, blank=True)


class Agent(Document):
    name = StringField(null=False, blank=False)
    address = StringField(null=False, blank=False)
    phone = StringField(max_length=250, null=True, blank=True, required=False)
    mobile_phone = StringField(max_length=250, null=True, blank=True, required=False)
    email = StringField(max_length=250, required=False, null=True, blank=True)
    relations = EmbeddedDocumentListField(AgentRelations, default=[])
    ref_integration_id = IntField(required=False)  # Id for the integration with audinfor or other datasources
    utility = ReferenceField(Utility, null=False, blank=False)
    user_profile = ReferenceField('UserProfile', null=True, blank=True)

    def __str__(self):
        return '%s' % self.name

    def get_parent_profile(self, contract_date):
        parent_profile = None
        parent = None
        for rel in self.relations:
            if (rel.start_date if rel.start_date else contract_date) <= contract_date <= (
            rel.end_date if rel.end_date else contract_date):
                parent = rel.parent
                break
        if parent and parent.user_profile:
            parent_profile = parent.user_profile
        return parent_profile

    def get_parent_agent(self, search_date):
        parent_agent = None
        for rel in self.relations:
            if (rel.start_date if rel.start_date else search_date) <= search_date <= (
            rel.end_date if rel.end_date else search_date):
                parent_agent = rel.parent
                break
        return parent_agent


class TariffProductLine(EmbeddedDocument):
    product_ref_integration_id = IntField(null=False, blank=False)
    product_group_ref_integration_id = IntField(null=False, blank=False)
    product_name = StringField(null=False, blank=False)
    product_group_name = StringField(null=False, blank=False)
    amount = FloatField(null=False, blank=False)
    start_date = DateTimeField(null=True, blank=True)
    end_date = DateTimeField(null=True, blank=True)


class TariffGroup(Document):
    FIXED = 0
    INDEXED = 1
    SELF_CONSUMPTION_FIXED = 2

    TARIFF_MODE = (
        (FIXED, _('Fijo')),
        (INDEXED, _('Indexado')),
        (SELF_CONSUMPTION_FIXED, _('Autoconsumo Fijo')),
    )

    active = BooleanField(default=True)
    name = StringField(max_length=150, required=True)
    utility = ReferenceField(Utility, required=True)
    tariff = ReferenceField(Tariff, required=True)
    tariff_mode = IntField(choices=TARIFF_MODE, required=True, default=0, blank=False)
    ref_integration_id = IntField(required=False)
    product_lines = EmbeddedDocumentListField(TariffProductLine, default=[])
    min_consumption = FloatField(required=False, default=None)
    max_consumption = FloatField(required=False, default=None)

    meta = {'strict': False, 'allow_inheritance': True}

    @property
    def tariff_mode_name(self):
        tariff_mode_dict = dict(self.TARIFF_MODE)
        return tariff_mode_dict.get(self.tariff_mode, "")

    @property
    def early_cancellation_discount(self):
        return 0.0

    @property
    def penalty(self):
        units = self.utility.get_units_from_code()
        if self.tariff.name in ["2.0A", "2.0DHA", "2.0DHS", "2.1A", "2.1DHA", "2.1DHS", "3.0A"]:
            percent_of_penalty = self.utility.parameters.early_cancellation_discount_until_30A
            unit = units['early_cancellation_discount_until_30A']
        else:
            percent_of_penalty = self.utility.parameters.early_cancellation_discount_after_30A
            unit = units['early_cancellation_discount_after_30A']
        return f'{percent_of_penalty} {unit}'

    @property
    def flexible(self):
        return False

    @property
    def inherit_from_flexible(self):
        return False

    def __str__(self):
        return f"{self.name} - {self.tariff.name}"

    def get_commission_value(self, consumptions_by_period, energy_prices=None, search_date=None):
        raise NotImplemented()

    def product_amount(self, eval_date=None):
        product_amount = 0
        eval_date = datetime.now() if not eval_date else eval_date
        try:
            for p in self.product_lines:
                if (p.start_date if p.start_date is not None else eval_date) <= eval_date <= (
                    p.end_date if p.end_date is not None else eval_date):
                    product_amount += p.amount
        except Exception as e:
            product_amount = 0

        return product_amount

    def allow_min_and_max(self, consumption):
        # from core.utils import get_mwh_from_cups_consumption
        # consumption = get_mwh_from_cups_consumption(consumption)
        consumption = consumption / 1000
        if self.min_consumption and self.min_consumption > consumption:
            return False
        return not self.max_consumption or self.max_consumption >= consumption


class CommissionScale(EmbeddedDocument):
    from_energy = FloatField(null=True, blank=True)
    to_energy = FloatField(null=True, blank=True)
    commission = FloatField(null=False, blank=False)

    @property
    def From_Energy(self):
        return self.from_energy if not np.isnan(self.from_energy) else None

    @property
    def To_Energy(self):
        return self.to_energy if not np.isnan(self.to_energy) else None


class Commission(Document):
    BY_MWH = 0
    BY_SCALE = 1
    COMMISSION_TYPE = (
        (BY_MWH, 'Por MWh'),
        (BY_SCALE, 'Por escalas'),
    )

    utility = ReferenceField(Utility, null=False, blank=False)
    name = StringField(null=False, blank=False, unique_with=('utility'))
    c_type = IntField(choices=COMMISSION_TYPE, default=1)
    start_date = DateTimeField(null=True, blank=True)
    end_date = DateTimeField(null=True, blank=True)
    scales = EmbeddedDocumentListField(CommissionScale, default=[])

    def get_commission_by_consumption(self, consumption):
        commission = 0
        for scale in self.scales:
            from_energy = scale.from_energy if not np.isnan(scale.from_energy) else -float("inf")
            to_energy = scale.to_energy if not np.isnan(scale.to_energy) else float("inf")
            if from_energy <= consumption <= to_energy:
                commission = scale.commission
                break
        if self.c_type == self.BY_MWH:
            commission = consumption * commission if commission else 0
        return commission


class TariffGroupCommission(Document):
    tariff_group = ReferenceField(TariffGroup, required=False)
    commission = ReferenceField(Commission, required=False)


class ContractBase(Document):
    CURRENCY = (('CAD', _('Dolar Canadiense')),
                ('COP', _('Peso Colombiano')),
                ('GBP', _('Libra Esterlina')),
                ('MXN', _('Peso Mexicano')),
                ('CHF', _('Franco Suizo')),
                ('EUR', _('Euro')),
                ('DKK', _('Corona Danesa')),
                ('SEK', _('Corona Sueca')),
                ('JPY', _('Yen Japones')),
                ('CUP', _('Peso Cubano')),
                ('PAB', _('Balboa Panama')),
                ('AUD', _('Dolar Australiano')),
                ('USD', _('Dolar Estadounidense')),
                ('NOK', _('Corona Noruega'))
                )

    PERIOD = (
        (1, _('Mensual')),
        (12, _('Anual'))
    )

    # INDEXED_TYPE = (
    #     (0, 'pass pool'),
    #     (1, 'pass through')
    # )
    CONTRACT_TYPE = (
        (0, _('Fijo')),
        (1, _('Indexado OMIE cuartohorario')),
        (2, _('Indexado OMIE horario')),
        (3, _('Indexado OMIE diario')),
        (4, _('Indexado OMIP mensual')),
        (5, _('Indexado OMIP trimestral')),
        (6, _('Indexado OMIP anual')),
        (7, _('Monomia Horaria')),
        (8, _('Monomia Simple')),
        (9, _('Regulated Transition')),
        (10, _('Regulated Social')),
        (11, _('Non-regulated')),
        (12, _('PVPC')),
        (13, _('PPA - Power Purchase Agreement')),
    )

    supply_point = ReferenceField('Meter', required=True)
    retailer = ReferenceField(Utility, required=True, empty_label=None)
    code = StringField(max_length=250, null=True, blank=True)
    # currency_code = StringField(max_length=3, choices=CURRENCY, required=False, default="EUR")

    tariff = ReferenceField(Tariff, required=False)  # TODO 1 verify required=True
    contract_type = IntField(choices=CONTRACT_TYPE, required=True, default=0, null=False)

    date_from = DateTimeField(required=True)
    date_to = DateTimeField(required=False)  # TODO 0 review

    emission_coefficient = FloatField(null=True, blank=True)

    observations = StringField(null=True, blank=True)
    simulated = BooleanField(default=False)
    simulated_name = StringField(max_length=100, required=False, default="")

    activate = BooleanField(default=False)

    energy_prices = ListField(FloatField(null=False, blank=False), default=[])
    power_prices = ListField(FloatField(null=False, blank=False), default=[])
    power_contract = ListField(FloatField(null=False, blank=False), default=[])

    term_fixed_energy = ListField(FloatField(null=False, blank=False))
    coefficient_OMIE_price = ListField(FloatField(null=False, blank=False))
    commercial_margin = ListField(FloatField(null=False, blank=False))

    tariff_group = ReferenceField(TariffGroup, required=False, null=True)  # TODO 1 verify required=True
    ref_integration_id = IntField(required=False)  # Id for the integration with audinfor or other datasources
    commission_value = FloatField(null=True, required=False, default=None)
    agent = ReferenceField(Agent, required=False)


    meta = {'collection': 'contract', 'strict': False, 'allow_inheritance': True}

    def save(self, *args, **kwargs):
        super(ContractBase, self).save(*args, **kwargs)

    def clean(self):
        cleaned_data = self

        # Clean data
        if cleaned_data.date_from and cleaned_data.date_to:
            cleaned_data.date_from = datetime.combine(cleaned_data.date_from, datetime.min.time())
            cleaned_data.date_to = datetime.combine(cleaned_data.date_to, datetime.min.time())

        errors = {}
        if cleaned_data.simulated == False and cleaned_data.date_from and cleaned_data.date_to:
            supply_point = cleaned_data.supply_point
            contratos = ContractBase.objects.filter(supply_point=supply_point)
            for contrato in contratos:
                if (contrato.simulated == False):
                    if cleaned_data.date_from >= contrato.date_from and cleaned_data.date_from <= contrato.date_to and cleaned_data.id != contrato.id or cleaned_data.date_to <= contrato.date_to and cleaned_data.date_to >= contrato.date_from and cleaned_data.id != contrato.id:
                        if 'date_to' not in errors or not errors['date_to']:
                            errors['date_to'] = []
                        errors['date_to'].append(_(
                            'El rango de fechas del contrato que ha introducido se pisan con otro contrato creado con anterioridad'))

        if cleaned_data.simulated and not cleaned_data.simulated_name:
            if 'simulated_name' not in errors or not errors['simulated_name']:
                errors['simulated_name'] = []
            errors['simulated_name'].append(_('Debe ponerle un nombre al contrato simulado'))

        if cleaned_data.simulated_name:
            supply_point = cleaned_data.supply_point
            contracts = supply_point.get_contracts()
            contracts = contracts.only("id", "simulated_name")
            contracts = contracts.filter(simulated_name=cleaned_data.simulated_name)
            if contracts.count() > 1 or (contracts.count() == 1 and contracts[0].id != cleaned_data.id):
                if 'simulated_name' not in errors or not errors['simulated_name']:
                    errors['simulated_name'] = []
                errors['simulated_name'].append(_('Ya existe otro contrato simulado con el mismo nombre'))

        if errors:
            raise ValidationError(errors)

        return cleaned_data

    def get_contract_type(self):
        return None

    def get_tariff_type(self):
        pass

    def get_num_periods(self):
        pass

    def to_string(self):
        pass

    def __str__(self):
        return self.to_string()

    def __unicode__(self):
        return self.to_string()

    def get_energy_prices(self, date_from, date_to):
        pass

    def get_energy_prices_state(self, date_from, date_to):
        pass

    @property
    def currency_name(self):
        pass

    @staticmethod
    def delete_by_meters(meters):
        pass


class TariffSchedule(Document):
    tariff_name = StringField(max_length=250)
    region = StringField(max_length=250)
    date = DateTimeField(default=datetime.now().replace(hour=0, minute=0, second=0, microsecond=0))
    energy = ListField(ListField(IntField()))
    power = ListField(ListField(IntField()))

    def __str__(self):
        return f'{self.tariff_name} {self.region}'
