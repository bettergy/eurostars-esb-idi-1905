from celery.utils.log import get_task_logger
from django.core.management import call_command
from core.recommendation.models import Recommendation
from efficiency.manager import EfficiencyManager
from escelery.celery import app

logger = get_task_logger(__name__)


@app.task
def register_data_in_blockchain(meter):
    call_command('task_register_meter_consumption', meter=meter.id)
    try:
        pv_recommendation_of_meter = Recommendation.objects.get(
            _cls='Recommendation.PvInstallationRecommendation.PvSelfConsumptionRecommendation',
            supply_point=meter.id)
        if pv_recommendation_of_meter:
            EfficiencyManager.register_recommendation_in_blockchain(meter, pv_recommendation_of_meter)
    except:
        logger.info("PV_Recommendation does not exist")
