import json
import logging
from datetime import datetime, timedelta

import pandas as pd
from dateutil.relativedelta import relativedelta
from mongoengine import MultipleObjectsReturned

from core.analysis.energy_offer.utils import EnergyOfferData, REGIONAL_COEFFICIENT, TAX
from core.internal_services.sips.handlers import CupsInfo, Consumption
from core.meters.exceptions import NotCreatedClientLEAD, NotCreatedBuildingFromLEAD, NotCreatedMeterFromLEAD, \
    NotCreatedContractFromLEAD, NotCreateInvoicesFromLEAD
from core.tariff_system.common.models import TariffGroup
from core.tariff_system.es.factory import DATE_CHANGE_ES_INVOICE_HELPER_FACTORY as DATE_CHANGE
from core.tariff_system.es.common.tariff_converter import TariffConverterEnergyOffer
from core.tariff_system.utils import get_components_of_tariff
from efficiency.power_optimization.utils import MaxiMeter

logger = logging.getLogger(__name__)


class SipsInputData(EnergyOfferData):

    creator_name = "SipsInputData"

    tariff = []
    days_with_data = 100
    days = 0
    contracted_power = []
    power_to_invoice = []
    power_prices = []
    excess_reactive_energy = []
    reactive_prices = []
    tariff_group = None

    def __init__(self, data):
        self.date_from = datetime.strptime(data['date_from'], "%Y-%m-%d")
        self.date_to = datetime.strptime(data['date_to'], "%Y-%m-%d") + timedelta(hours=23, minutes=59)
        self.days = ((self.date_to + timedelta(days=1)) - self.date_from).days
        self.contract = data.get('contract', '')
        self.building = data.get('building', '')
        self.name_building = data.get('name_building', '')
        self.cups = data.get('cups', '')
        self.tariff_name = data.get('tariff_name')
        self.tariff = self.get_tariff(self.tariff_name)
        self.num_periods = data.get('num_periods')
        self.num_periods_power = data.get('num_periods_power')
        self.tariff_group = TariffGroup.objects.get(id=data['tariff_group']) if 'tariff_group' in data and data['tariff_group'] != '' else None
        tariff_group_comp = get_components_of_tariff(self.tariff, self.tariff_group)
        self.power_prices = data.get('power_prices')[:self.num_periods_power] if 'power_prices' in data else tariff_group_comp['power_prices']
        self.energy_prices = self.get_energy_prices(data, self.date_from, self.date_to, self.num_periods)

        self.power_discount = data.get('power_discount', 0)
        self.energy_discount = data.get('energy_discount', 0)

        self.df_power = pd.DataFrame()
        self.contracted_power = data['power_contract'][:self.num_periods_power]
        self.energy_period = data.get('energy_period')[:self.num_periods]
        self.reactive_energy = data.get('reactive_energy')[:self.num_periods]
        self.maximeters = data.get('maximeters')[:self.num_periods_power]
        self.maximeters = MaxiMeter.create(self)

        self.tax = data.get('tax', TAX)
        self.regional_coefficient = data.get('regional_coefficient', REGIONAL_COEFFICIENT)
        self.rent = data.get('rent', 0)
        self.other_concepts = data.get('other_concepts', 0)
        self.iva_percentage = data.get('iva_percentage', 21)


class LEADFormHandler(object):
    sips_data = None
    utility = None
    user = None
    data = None
    lead_origin = None

    def __init__(self, sips_data: CupsInfo, utility, user=None, form_data=None, cnae_code="9820", lead_origin=0):
        self.sips_data = sips_data
        self.utility = utility
        self.user = user
        self.data = form_data
        self.cnae_code = cnae_code
        self.lead_origin = lead_origin

    def generate_client_data(self, lead):
        lead = self._update_client_assessors(lead)
        building = self._create_building(lead)
        meter = self._create_meter(building)
        contract = self._create_contract(meter)
        self._create_invoices_reports(meter)
        return lead, building, meter, contract

    def generate_client_data_with_prices(self):
        from core.utils import get_invoices_by_building_id
        from core.tasks import task_download_sips_by_building
        from core.devices.models import SupplyPoint
        from core.reporting.models import Report
        d = self.data

        if "meter_id" in d and d["meter_id"]:
            meter = SupplyPoint.objects.get(id=d.get('meter_id'))
            building = meter.building
            lead = building.owner
            date_to = datetime.now()
            date_from = date_to - timedelta(days=365*2)
            reports = get_invoices_by_building_id(str(building.id), date_from, date_to, origin=Report.INVOICE_SIPS)
            if len(reports) > 0:
                reports.delete()
        else:
            lead = self._create_client()
            building = self._create_building(lead)
            meter = self._create_meter(building)

        current_invoice = json.loads(d.get('current_invoice'))
        simulated_invoice = json.loads(d.get('simulated_invoice'))
        c_contract = self._create_contract_with_prices(meter, current_invoice, simulated=False)
        s_contract = self._create_contract_with_prices(meter, simulated_invoice)
        task_download_sips_by_building.delay(str(building.id))
        # self._create_invoices_reports(meter)
        return lead, building, meter, c_contract, s_contract

    def generate_lead_with_prices(self, with_data=True):
        if with_data:
            lead, building, meter, c_contract, s_contract = self.generate_client_data_with_prices()
        else:
            lead, building, meter, c_contract, s_contract = None, None, None, None, None
        return lead, building, meter, c_contract, s_contract

    def generate_lead(self, with_data=True):
        lead = self._create_client()
        if with_data:
            lead, building, meter, contract = self.generate_client_data(lead)
        else:
            building, meter, contract = None, None, None
        return lead, building, meter, contract

    def generate_invoice_summary(self, meter, months_ago=24, check_for_exist=False):
        self._update_building(meter.building)
        self._update_meter(meter)
        return self._create_invoices_reports(meter, months_ago=months_ago, check_for_exist=check_for_exist)

    def _create_client(self):
        from core.buildings.models import Client
        try:
            lead = Client()
            lead.name = self.data.get("client_name", "Cliente " + str(self.sips_data.cups)) if self.data else "Cliente " + str(self.sips_data.cups)
            lead.nif = self.data.get("client_nif", None) if self.data else None
            lead.utility = self.utility
            lead.is_lead = self.data.get("is_lead", True) if self.data else True
            lead.creation_origin = self.lead_origin
            lead.assessors = []
            lead = self._update_client_assessors(lead)
            lead.save()
        except Exception as e:
            raise NotCreatedClientLEAD()
        return lead

    def _create_building(self, lead):
        from core.buildings.models import Building, ActivityCode

        try:
            building = Building()
            building.name = self.data.get("building_name", "") if self.data else "Instalación " + str(self.sips_data.cups)
            building.utility = self.utility
            building.owner = lead
            building.address = self.data.get("building_address", "") if self.data else ""
            building.country = "ES"
            building.postal_code = str(self.sips_data.supply_point_info.postal_code)
            building.province = str(self.sips_data.supply_point_info.province)
            building.get_subregion = str(self.sips_data.supply_point_info.province)
            building.city = str(self.sips_data.supply_point_info.town)
            building.get_city = str(self.sips_data.supply_point_info.town)
            building.total_consumption = self.sips_data.annual_consumption
            building.activity_code = ActivityCode()
            if self.data:
                building.surface = self.data.get("building_surface", 0)
                building.surface_efective = self.data.get("building_surface_effective", 0)
                building.occupants = self.data.get("building_occupation", 0)
                building.activity_code.create(self.data.get('building_activity', None), utility=self.utility)
            else:
                building.activity_code.create(self.cnae_code, utility=self.utility)
            building.save()
        except Exception as e:
            raise NotCreatedBuildingFromLEAD()
        return building

    def _update_building(self, building):
        from core.buildings.models import ActivityCode
        try:
            building.utility = self.utility
            building.postal_code = str(self.sips_data.supply_point_info.postal_code)
            building.province = str(self.sips_data.supply_point_info.province)
            building.get_subregion = str(self.sips_data.supply_point_info.province)
            building.city = str(self.sips_data.supply_point_info.town)
            building.get_city = str(self.sips_data.supply_point_info.town)
            building.total_consumption = self.sips_data.annual_consumption
            building.activity_code = ActivityCode()
            building.activity_code.create(self.cnae_code, utility=self.utility)
            building.save()
        except Exception as e:
            logger.info(f'Building {building} could not be updated - {e}')

    def _create_meter(self, building):
        from core.devices.models import SupplyPoint
        try:
            supply_point = SupplyPoint()
            supply_point.name = self.data.get("meter_name", "") if self.data else "Medidor " + str(self.sips_data.cups)
            supply_point.building = building
            supply_point.datalogger_id = self.utility.datalogger_id
            supply_point.cups = str(self.sips_data.cups)
            supply_point.pow_max_BIEW = float(self.sips_data.supply_point_info.max_power_biew)
            supply_point.applies_social_bonus = self.sips_data.owner_info.apply_social_bonus
            supply_point.save()
        except Exception as e:
            raise NotCreatedMeterFromLEAD()
        return supply_point

    def _update_meter(self, supply_point):
        try:
            supply_point.cups = str(self.sips_data.cups)
            supply_point.pow_max_BIEW = float(self.sips_data.supply_point_info.max_power_biew)
            supply_point.applies_social_bonus = self.sips_data.owner_info.apply_social_bonus
            supply_point.save()
        except Exception as e:
            logger.info(f'Supply point {supply_point} could not be updated - {e}')

    def _create_contract_with_prices(self, supply_point, invoice, simulated=True):
        from core.tariff_system.es.common.models import ContractEsEs
        from core.tariff_system.common.models import Tariff, TariffGroup
        from mongoengine import DoesNotExist

        try:
            contract = ContractEsEs.objects.get(supply_point=supply_point, simulated=simulated)
        except DoesNotExist:
            contract = ContractEsEs()
        except MultipleObjectsReturned:
            contract = ContractEsEs.objects.filter(supply_point=supply_point, simulated=simulated).order_by('-date_to').first()

        try:
            contract.name = self.data["meter_name"] if self.data else "Medidor " + str(self.sips_data.cups)
            contract.supply_point = supply_point
            contract.contract_type = 0

            date_from, date_to, tariff_name, contracted_power = self._create_contract_dates(supply_point, self.sips_data)

            try:
                if 'indexed_contract' in invoice:
                    contract.contract_type = 1 if invoice['indexed_contract'] else 0
                if 'tariff_group' in invoice and invoice['tariff_group'] != '':
                    contract.tariff_group = TariffGroup.objects.get(id=invoice['tariff_group'])
                    contract.contract_type = contract.tariff_group.tariff_mode
                else:
                    contract.tariff_group = None
                tariff = Tariff.objects.get(name=tariff_name, contract_type=contract.contract_type)
                contract.tariff = tariff
                num_periods_power = tariff.num_periods_power
                num_periods = tariff.num_periods
                contract.power_prices = [float(i) for i in invoice['power_prices'][:num_periods_power]]
                contract.energy_prices = [float(i) for i in invoice['energy_prices'][:num_periods]]
                contract.commercial_margin = invoice['commercial_margin'] if 'commercial_margin' in invoice else []
            except Exception as e:
                logger.error(f'Error trying to get the tariff for the contract ({e})')

            contract.power_contract = contracted_power
            contract.date_from = date_from
            contract.date_to = date_to
            contract.tax = 4.864
            contract.vat = 21.0
            contract.regional_coefficient = 1.05113
            contract.indexed_type = 1
            contract.meter_leasing = float(invoice['rent'])
            contract.other_concepts = float(invoice['other_concepts'])
            contract.discount_variable_term = float(invoice['energy_discount'])
            contract.discount_fixed_term = float(invoice['power_discount'])

            # contract.term_fixed_energy = [5.0] * num_periods
            # contract.coefficient_OMIE_price = [1.0] * num_periods

            if simulated:
                contract.simulated = simulated
                contract.simulated_name = contract.tariff_group.name if contract.tariff_group else "Contrato Simulado"

            contract.retailer = self.utility
            contract.save()
        except Exception as e:
            raise NotCreatedContractFromLEAD()
        return contract

    def _get_energy_prices_by_sips_contract_type(self, sips_contract_type, tariff_name):
        contract_energy_prices = self.utility.sips_contract_energy_prices.get(str(sips_contract_type), {})
        tariff_name = tariff_name.replace('.', '')
        return contract_energy_prices.get(tariff_name, [])

    def _get_sips_contract_type_from_utility(self):
        return self.utility.sips_contract_type

    def _create_contract(self, supply_point):
        from core.tariff_system.es.common.models import ContractEsEs
        from core.tariff_system.common.models import Tariff, TariffPrices
        from core.export_import.invoice_manager import search_contract_type

        try:
            contract = ContractEsEs()
            contract.name = self.data["meter_name"] if self.data else "Medidor " + str(self.sips_data.cups)
            contract.supply_point = supply_point
            contract.contract_type = self._get_sips_contract_type_from_utility()
            if 'contract_type' in self.data:
                contract.contract_type = search_contract_type(contract, self.data['contract_type'])

            date_from, date_to, tariff_name, contracted_power = self._create_contract_dates(supply_point, self.sips_data)

            num_periods = 6
            try:
                tariff = Tariff.objects.get(name=tariff_name, contract_type=contract.contract_type)
                num_periods_power = tariff.num_periods_power
                num_periods = tariff.num_periods

                power_prices = [self.data['power_prices_' + str(i)] if 'power_prices_' + str(i) in self.data else 0 for
                                i in range(1, 7)][:num_periods_power]
                energy_prices = [self.data['energy_prices_' + str(i)] if 'energy_prices_' + str(i) in self.data else 0
                                 for i in range(1, 7)][:num_periods]
                tariff_f = Tariff.objects.get(name=tariff_name, contract_type=0)
                tariff_prices = TariffPrices.objects.filter(tariff=tariff_f).limit(1)

                if not sum(power_prices) and tariff_prices:
                    power_prices = tariff_prices.first().component["power_prices"]
                contract.tariff = tariff

                if not sum(energy_prices):
                    energy_prices = self._get_energy_prices_by_sips_contract_type(contract.contract_type, tariff.name)

                contract.power_prices = power_prices
                contract.energy_prices = energy_prices

            except Exception as e:
                logger.error(f'Error trying to get the tariff for the contract ({e})')

            contract.power_contract = contracted_power
            contract.date_from = date_from
            contract.date_to = date_to
            contract.tax = 4.864
            contract.vat = 21.0
            contract.regional_coefficient = 1.05113
            contract.meter_leasing = 0.0
            contract.discount_variable_term = 25.0 if supply_point.applies_social_bonus else 0.0  # sobre energia
            contract.discount_fixed_term = 25.0 if supply_point.applies_social_bonus else 0.0  # sobre potencia

            contract.term_fixed_energy = [5.0] * num_periods
            contract.coefficient_OMIE_price = [1.0] * num_periods

            if contract.contract_type == self.utility.SIPS_INDEXED_CONTRACT:
                contract.indexed_type = 1

            contract.retailer = self.utility
            contract.save()
        except Exception as e:
            raise NotCreatedContractFromLEAD()
        return contract

    @staticmethod
    def _create_contract_dates(supply_point, sips_data):
        from core.tariff_system.es.factory import ESInvoiceHelperFactory
        date_change = ESInvoiceHelperFactory.get_date_change_tariff()
        tariff_name = sips_data.current_tariff
        contracted_power = sips_data.contracted_power
        if datetime.now() < date_change:
            date_from = datetime.now() - relativedelta(months=15)
            date_to = date_change - relativedelta(days=1)
        else:
            date_from = date_change
            date_to = date_from + relativedelta(years=1)
            handler = ESInvoiceHelperFactory.get_invoice_helper(date_to)
            conv = handler(supply_point).get_tariff_converter(tariff_name=sips_data.current_tariff)
            tariff_name = conv.get_equivalent
            contracted_power = conv.convert_power_contract(contracted_power)
        return date_from, date_to, tariff_name, contracted_power

    @staticmethod
    def _get_invoice_helper(consumption,  meter):
        from core.tariff_system.factory import InvoiceHelperFactory
        date_from = datetime(year=consumption.date_from.year, month=consumption.date_from.month,
                             day=consumption.date_from.day, hour=0, minute=0, second=0)
        date_to = datetime(year=consumption.date_to.year, month=consumption.date_to.month,
                           day=consumption.date_to.day, hour=23, minute=59, second=59)
        return InvoiceHelperFactory().get_handler(meter, date_from, date_to)

    def _get_data_inputs(self, consumption, meter, ih):
        consumption.energy_period = consumption.active_energy[:consumption.num_periods]
        data = consumption.__dict__
        data['power_contract'] = ih.get_power_contract()
        data['energy_prices'] = ih.get_energy_prices()
        data['power_prices'] = ih.get_power_prices()
        data['contract'] = str(ih.contract.id)
        data['building'] = str(meter.building.id)
        data['name_building'] = meter.building.name
        data['cups'] = meter.cups
        data['utility'] = self.utility
        data['date_from'] = str(consumption.date_from).split(' ')[0]
        data['date_to'] = str(consumption.date_to).split(' ')[0]
        data['energy_period'] = consumption.active_energy[:consumption.num_periods]
        return data

    @staticmethod
    def prepare_input_json(data, ih):
        sips_input_data = SipsInputData(data)
        json_data = sips_input_data.to_json()
        return ih.invoice_generator.compute_sections(json_data)

    @staticmethod
    def data_converter(data):
        if data.get('tariff_name') in TariffConverterEnergyOffer.EQUIVALENT and datetime.strptime(data.get('date_to'), "%Y-%m-%d") > DATE_CHANGE:
            tariff_converter = TariffConverterEnergyOffer(data)
            data.update(tariff_converter.convert_data())
        return data

    def _create_invoice_from_consumption(self, consumption: Consumption, meter):
        from core.reporting.models import Invoice
        ih = self._get_invoice_helper(consumption, meter)
        data = self._get_data_inputs(consumption, meter, ih)
        data = LEADFormHandler.data_converter(data)
        details = self.prepare_input_json(data, ih)
        return Invoice().from_json(details)

    def _create_invoices_reports(self, meter, months_ago=24, check_for_exist=False):
        from core.reporting.models import Report
        from core.reporting.utils import get_name_invoice_report
        from core.utils import get_invoices_by_building_supply_point_two_dates

        download = 0
        saved = 0
        existing = 0

        date_now = datetime.now()
        ref_date = date_now - relativedelta(months=months_ago)

        try:
            for c_data in self.sips_data.consumption_data:
                if c_data.date_from.date() >= ref_date.date():

                    if c_data.date_to < DATE_CHANGE and c_data.tariff_name not in TariffConverterEnergyOffer.EQUIVALENT:
                        logger.info(f'consumption between {c_data.date_from}-{c_data.date_to} with tariff'
                                    f'{c_data.tariff_name} register in old tariff system.')
                        continue

                    if c_data.date_from < DATE_CHANGE and c_data.date_to >= DATE_CHANGE:
                        logger.info(f'consumption of {c_data.date_from}-{c_data.date_to} between two tariff systems.')
                        continue

                    download += 1
                    if check_for_exist:
                        start = datetime(year=c_data.date_from.year, month=c_data.date_from.month,
                                         day=c_data.date_from.day, hour=0, minute=0, second=0)
                        end = datetime(year=c_data.date_to.year, month=c_data.date_to.month,
                                       day=c_data.date_to.day, hour=23, minute=59, second=59)

                        df_invoices = get_invoices_by_building_supply_point_two_dates(meter, start, end,
                                                                                      [Report.INVOICE_SIPS,
                                                                                       Report.SIPS])
                        if not df_invoices.empty:
                            existing += 1
                            continue

                    report = Report()
                    report.category = 1
                    report.type_code = 0
                    report.description = get_name_invoice_report(meter, c_data.date_to) + " SIPS"
                    report.schedule = 1
                    report.origin = Report.SIPS
                    report.building_id = meter.building
                    report.name_building = meter.building.name
                    report.client_id = meter.building.owner
                    report.creation_date = c_data.date_from + relativedelta(months=+1, hour=23, minute=59)
                    contract = meter.get_current_contracts(c_data.date_from, c_data.date_to).first()
                    if contract:
                        report.origin = Report.INVOICE_SIPS
                        report.description = report.description.replace("Factura", "Consumo")

                    # report.details = c_data.calculate_invoice(meter)
                    report.details = self._create_invoice_from_consumption(c_data, meter)
                    try:
                        report.save()
                        saved += 1
                        existing += 1
                        logger.info(f'generating invoice {c_data.date_from}-{c_data.date_to} successfully {report.id}')
                    except Exception as e:
                        logger.error(f'generating invoice {c_data.date_from}-{c_data.date_to} error --> {e}')
            return download, saved, existing
        except Exception as e:
            raise NotCreateInvoicesFromLEAD()

    def _update_client_assessors(self, client):
        from authz.models import UserProfile
        from authz.utils import get_user_profile
        if client is None or client.assessors is None:
            return client
        assessors = client.assessors
        try:
            user_profile = UserProfile.objects.get(user=self.user)
            if user_profile.is_assessor:
                assessors.append(user_profile)
        except Exception as ex:
            logger.info(f'{ex}')

        username = self.data.get('username', None) if self.data else None
        user, user_profile = get_user_profile(utility=client.utility, username=username)
        if user_profile is not None and user_profile.is_assessor:
            assessors.append(user_profile)
        client.assessors = assessors
        return client
