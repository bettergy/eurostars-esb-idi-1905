import json
import pandas as pd
import os

from django.contrib import messages
from django.core.management import call_command
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import resolve
from pathlib import *

from django_mongoengine.views import UpdateView

from authz.utils import user_is_role
from core.buildings.forms import ParametersForm, UtilityForm
from core.mailing.service import NewMeterSender
from core.meters.tasks import register_data_in_blockchain
from core.tariff_system.common.models import Tariff, TariffPrices, Utility, TariffGroup
from core.alerts.models import Alert
from core.devices.models import EnergySource
from core.meters.forms import SupplyPointForm, SubmeterForm
from core.cleanup.tasks import task_process_and_delete_meter
from core.buildings.models import Building
from core.tariff_system.utils import get_components_of_tariff
from core.utils import *
from core.tasks import compute_invoice_one_building, download_sips_by_contract
from django.utils.translation import gettext as _
from django.contrib.auth.mixins import LoginRequiredMixin, AccessMixin
from django.views.generic import TemplateView, DeleteView
from core.internal_services.sips.exceptions import InvalidCUPS, NotFoundCUPS, SIPSSystemError, InvalidSIPSCredentials, \
    MissingSIPSCredentials, NoDataReceived
from django.shortcuts import redirect, reverse
from django.urls import reverse_lazy
from mongoengine import MultipleObjectsReturned
from core.meters.forms import DocumentFormUtils, RegisterLEADForm
from app.catalog.models import ReportTemplate
from eslogging.models import RecommendationTrace
from core.meters.exceptions import *
from config.settings.common import DATE_CHANGE_ES_INVOICE_HELPER_FACTORY
from config.settings.common import DEFAULT_RECOS

import logging

from storage.storage import handle_upload_file_to_cloud_bucket

logger = logging.getLogger(__name__)


def meter_view(request, building_id, meter_id=None):
    user_profile = UserProfile.objects.get(user=request.user)
    final_client = user_profile.is_employee
    is_supply = True
    building = get_building_or_403(request.user, building_id)
    total_alerts = Alert.objects.filter(building=building).count()
    new_alerts = Alert.objects.filter(building=building, unread=True).count()
    sources = EnergySource.objects.all()
    selected_meter = None
    is_meter_synchronized_with_esb = False

    # Check if SIPS synchronization is enabled
    # sips_authenticated = SIPSAPIHandler.is_sips_authenticated(utility=user_profile.utility)
    sige_authenticated = user_profile.utility.is_sige_authenticated

    if meter_id:
        selected_meter = Meter.objects.get(pk=meter_id)
    elif resolve(request.path).url_name == "detail_meter":
        selected_meter = get_default_meter(building)

    if selected_meter:
        old_phone = selected_meter.comm_params
        is_supply = selected_meter.is_supply()
        meter_form = SupplyPointForm(instance=selected_meter)
        submeter_form = SubmeterForm(building_id=building_id, instance=selected_meter)
        is_meter_synchronized_with_esb = selected_meter.esb_sync
    else:
        meter_form = SupplyPointForm()
        submeter_form = SubmeterForm(building_id=building_id)

    # Check the slider.
    if request.method == "POST":
        copy = request.POST.copy()
        post = clean_meter_form(copy)
        if request.POST.get('is_supply_point') == "on":
            meter_form = SupplyPointForm(post, instance=selected_meter)
        else:
            meter_form = SubmeterForm(post, instance=selected_meter)

        if meter_form.is_valid():
            selected_meter = meter_form.save(commit=False)
            selected_meter.building = building
            if selected_meter.esb_meter_id and selected_meter.esb_meter_contract_id:
                if not selected_meter.esb_sync:
                    selected_meter.esb_sync = True
                    selected_meter.save()
                    register_data_in_blockchain(selected_meter)
            else:
                selected_meter.esb_sync = False
            selected_meter.save()

            # TODO Review the email sending process
            # if selected_meter.comm_params != old_phone and selected_meter.comm_params is not None\
            #         and selected_meter.comm_params != '':
            #     ms = NewMeterSender()
            #     ms.send_mail(selected_meter)
            messages.success(request, _('Se ha guardado correctamente el medidor.'))
            return redirect('meters_edit', building_id=building.id, meter_id=selected_meter.id)
        else:
            utils = DocumentFormUtils()
            meter_form._errors = utils.assign_all_errors_to_fields(meter_form.errors)
            messages.error(request, _('Formulario no válido'))
            print(meter_form.errors.as_data())

            # return redirect('meters_edit', building_id=building.id, meter_id=selected_meter.id)

    meters = get_all_meters(building, selected_meter)
    # building.status = building.get_status_of_building()
    energy_contracts = get_all_contracts(selected_meter)
    energy_contracts_simulated = get_all_contracts(selected_meter, True)

    try:
        datalogger_id_value = user_profile.utility.datalogger_id
        if datalogger_id_value:
            meter_form.fields['datalogger_id'].initial = datalogger_id_value
    except Exception as e:
        print(e)

    if selected_meter:
        # reports of ReportTemplate.METER
        models = [ReportTemplate.METER, ReportTemplate.SUPPLY_POINT]
        reports = get_reports_by_user(request.user)
        reports = reports.filter(model__in=models)
    else:
        reports = None

    return render(request,
                  'core/meter_form_layout.html',
                  {
                      'is_meter_synchronized_with_esb': is_meter_synchronized_with_esb,
                      'meter_form': meter_form,
                      'submeter_form': submeter_form,
                      'is_supply': is_supply,
                      'meters': meters,
                      'sources': sources,
                      'energyContracts': energy_contracts,
                      'energy_contracts_simulated': energy_contracts_simulated,
                      'final_client': final_client,
                      'selected_meter': selected_meter,
                      'alertasTotales': total_alerts,
                      'alertasNuevas': new_alerts,
                      'building': building,
                      'meter_reports': reports,
                      'sips_authenticated': user_profile.utility.sips_authenticated,
                      'sige_authenticated': sige_authenticated,
                  })


class MeterDeleteView(LoginRequiredMixin, DeleteView):
    model = Meter
    template_name = ''
    success_url = ''
    object = None

    def get_object(self, queryset=None):
        try:
            self.object = self.model.objects.get(id=self.kwargs['meter_id'])
        except (Building.DoesNotExist, DoesNotExist):
            raise Http404(_('The meter does not exist'))
        return self.object

    def post(self, request, *args, **kwargs):
        if kwargs.get("meter_id", None):
            meter = self.get_object()
            task_process_and_delete_meter(str(meter.id))
        return HttpResponse(json.dumps({}), content_type='application/json')


class GetSIPSInvoiceSummaryView(LoginRequiredMixin, TemplateView):
    model = Report
    template_name = ''
    success_url = ''
    object = None
    message_response = ''

    def post(self, request, *args, **kwargs):
        from core.devices.models import Meter
        if kwargs["meter_id"]:
            meter = Meter.objects.get(id=kwargs["meter_id"])

        run_recos = self.request.POST["run_recos"] if "run_recos" in self.request.POST else False
        if run_recos:
            reco_based_on = int(self.request.POST["reco_based_on"]) if "reco_based_on" in self.request.POST else 0
            from efficiency.manager import BASED_ON
            reco_based_on_name = BASED_ON.get(reco_based_on)
            try:
                from core.tasks import task_compute_recommendations_for_building
                from celery_once import AlreadyQueued
                from django.contrib.messages import constants
                task_compute_recommendations_for_building.delay(building_id=str(meter.building.id),
                                                                user_id=str(request.user.id),
                                                                based_on=reco_based_on)

                message = _('Se ha comenzado a recalcular las recomendaciones basadas en "%s" para el edificio "%s".'
                            '') % (reco_based_on_name, meter.building.name)
                messages.success(request, message)
            except AlreadyQueued as e:
                message = _(
                    'No se ha comenzado a recalcular las recomendaciones basadas en "%s" para el edificio "%s" porque se está '
                    'ejecutando un cálculo anterior. Inténtelo mas tarde.') % (reco_based_on_name, meter.building.name)
                messages.error(request, message)
            except Exception as e:
                message = _(
                    'No se ha comenzado a recalcular las recomendaciones basadas en "%s" para el edificio "%s" '
                    'porque ha ocurrido un error inesperado.') % (reco_based_on_name, meter.building.name)
                messages.error(request, message)
            data = {'ok': 1}

        sips_data = self.request.POST["sips_data"] if "sips_data" in self.request.POST else False
        if sips_data:
            try:
                from core.internal_services.sips.utils import SIPSHandler
                downloaded, saved, existing = SIPSHandler().download_invoice_summary(meter)
            except Exception as e:
                logger.error(e)
                downloaded, saved, existing = 0, 0, 0
            data = {"invoice_summary_download": downloaded, "invoice_summary_saved": saved,
                    "invoice_existing": existing}
        return HttpResponse(json.dumps(data), content_type='application/json')

    @staticmethod
    def get_succesfull_message(invoice_summary_download, invoice_summary_saved):
        return "<p style='text-align:justify;'>" + _(
            "Se ha realizado la descarga de datos de consumos de la base de datos SIPS de los últimos 24 meses") + \
               _(", puede acceder al listado de sus facturas y comprobar que han sido descargadas. ") + \
               "<br/> " + _("Consumos disponibles en SIPS") + ": " + "<strong>" + str(
            invoice_summary_download) + "</strong>" + \
               "<br/>" + _("Consumos descargados") + ": " + "<strong>" + str(
            invoice_summary_saved) + "</strong>" + "<br/>" + \
               _(
                   "Se procede a recalcular para este contrato las medidas de eficiencia energética basadas en facturas") + \
               _(", recibirá un aviso una vez estén disponibles.") + "</p>"


class ContractView(LoginRequiredMixin, TemplateView):
    model = ContractBase
    template_name = "contract/common/contract_form_layout.html"
    object = None
    form = None
    contract = None
    date_from = None
    date_to = None
    selected_meter = None

    def get_object(self, queryset=None):
        self.object = self.model()
        return self.object

    def get_context_data(self, **kwargs):
        context = super(ContractView, self).get_context_data(**kwargs)

        # General info
        user = self.request.user
        user_profile = UserProfile.objects.get(user=user)
        final_client = user_profile.is_employee
        building = get_building_or_403(self.request.user, kwargs["building_id"])
        alertasTotales = Alert.objects.filter(building=building).count()
        alertasNuevas = Alert.objects.filter(building=building, unread=True).count()

        self.selected_meter = Meter.objects.get(pk=kwargs["meter_id"])
        meters = get_all_meters(building, self.selected_meter)
        energy_contracts = get_all_contracts(self.selected_meter)
        energy_contracts_simulated = get_all_contracts(self.selected_meter, True)

        # current contract info
        is_supply = self.selected_meter.is_supply()
        if kwargs.get("contract_id", None):
            self.contract = ContractBase.objects.get(id=kwargs["contract_id"])
            self.nb_periods = self.contract.get_num_periods()
            self.date_from = self.contract.date_from
            self.date_to = self.contract.date_to

        context['is_supply'] = is_supply
        context['meters'] = meters
        context['contract'] = self.contract
        context['energyContracts'] = energy_contracts
        context['energy_contracts_simulated'] = energy_contracts_simulated
        context['final_client'] = final_client
        context['selected_meter'] = self.selected_meter
        context['alertasTotales'] = alertasTotales
        context['alertasNuevas'] = alertasNuevas
        context['building'] = building

        invoice_helper = InvoiceHelperFactory().get_handler_by_contract(self.selected_meter, self.date_from,
                                                                        self.date_to)
        # invoice_helper = None
        if invoice_helper:
            contract_types = list(dict(invoice_helper.contract_model.CONTRACT_TYPE).keys())

            if self.contract:
                contract_type = self.contract.contract_type
            elif contract_types:
                contract_type = int(contract_types[0]) if is_supply else 13  # TODO 0 review
            else:
                contract_type = None

            tariff = str(self.contract.tariff.id) if self.contract and self.contract.tariff else None

            self.form = invoice_helper.get_contract_form_with_type(contract_type, self.contract, user_profile.utility,
                                                                   self.selected_meter.id, None, tariff)
            context["form"] = self.form
            context["template"] = invoice_helper.get_template_by_contract_type(contract_type)
            context["contract_type"] = contract_type
            context["has_form"] = bool(self.form)
            # context['country'] = country
            context['contracts_types'] = invoice_helper.contract_model.CONTRACT_TYPE
            context["has_invoice_helper"] = True
            context["date_tariff_change_es"] = DATE_CHANGE_ES_INVOICE_HELPER_FACTORY
        else:
            context["has_invoice_helper"] = False
        return context

    def post(self, request, *args, **kwargs):
        from efficiency.manager import BASED_ON
        context = self.get_context_data(**kwargs)
        invoice_helper = InvoiceHelperFactory().get_handler(self.selected_meter)
        recalculate_measures = (
            "recalculate_measures" in self.request.POST and self.request.POST["recalculate_measures"] == "1")
        recalculate_measures_based_on = int(self.request.POST[
                                                "recalculate_measures_based_on"]) if "recalculate_measures_based_on" in self.request.POST else 0
        reco_based_on_name = BASED_ON.get(recalculate_measures_based_on)
        copy = request.POST.copy()
        post = invoice_helper.clean_ContractForm(copy)
        self.form = invoice_helper.get_contract_form_with_post(self.contract, post, request.POST.get('contract_type'))
        date_from = datetime.strptime(request.POST.get('date_from'), '%d/%m/%Y')
        date_to = datetime.strptime(request.POST.get('date_to'), '%d/%m/%Y')

        if self.form.is_valid():
            self.contract, msg = self.form.save()

            if msg is "success":
                messages.success(request, _('Se ha guardado correctamente el contrato.'))

                if not self.contract.simulated:
                    try:
                        compute_invoice_one_building.delay(str(self.contract.id))
                        download_sips_by_contract.delay(str(self.contract.id), str(request.user.id))
                        messages.success(request, _('Se ha comenzado a generar las facturas para el contrato y a '
                                                    'descargar los consumos de SIPS.'))
                    except Exception as e:
                        messages.warning(request, _('Se ha producido algún error generando facturas para el contrato'))

                    from config.settings.common import CALC_FROM_CONTRACT_PROFILE
                    if recalculate_measures and CALC_FROM_CONTRACT_PROFILE:
                        from core.tasks import task_compute_recommendations_for_supply_point
                        task_compute_recommendations_for_supply_point.delay(
                            supply_point_id=str(self.selected_meter.id),
                            user_id=str(request.user.id),
                            based_on=recalculate_measures_based_on)

                        message = _('Se ha comenzado a recalcular las recomendaciones basadas en "%s" para el cups '
                                    '"%s" del edificio "%s". Esta operación terminará en unos minutos' %
                                    (reco_based_on_name, self.selected_meter.cups, self.selected_meter.building.name))
                        messages.info(request, message)
            else:
                messages.error(request, msg)
        else:
            messages.error(request, _('Formulario no válido'))

            # print(self.form.errors.as_data())

            utils = DocumentFormUtils()
            self.form._errors = utils.assign_all_errors_to_fields(self.form.errors)

            context["form"] = self.form
            context["template"] = invoice_helper.get_template_by_contract_type(request.POST.get('contract_type'))
            context['contract'] = self.contract

            return render(request, self.template_name, context)

        if self.contract:
            return redirect(reverse('contract_edit', kwargs={'building_id': self.selected_meter.building.id,
                                                             'meter_id': self.selected_meter.id,
                                                             'contract_id': self.contract.id}))


class GetPowerPricesOfTariff(LoginRequiredMixin, TemplateView):
    template_name = ""

    def post(self, request, *args, **kwargs):
        tariff_group = None

        try:
            utility_id = get_utility(self.request.user)
            utility = Utility.objects.get(id=utility_id)
            tariff_selected = Tariff.objects.get(id=request.POST.get('tariff'))
            if request.POST.get('tariff_group', None):
                tariff_group = TariffGroup.objects.get(id=request.POST.get('tariff_group'))
            json_output = get_components_of_tariff(tariff_selected, tariff_group, utility)
        except (MultipleObjectsReturned, DoesNotExist) as e:
            json_output = {'power_prices': [], 'energy_prices': [], 'commercial_margin': []}
            logger.error(e)

        return JsonResponse(json_output, safe=False)


class ContractDeleteView(LoginRequiredMixin, DeleteView):
    model = ContractBase
    template_name = ''
    success_url = ''
    object = None

    def post(self, request, *args, **kwargs):
        if kwargs["contract_id"]:
            contract = self.model.objects.get(id=kwargs["contract_id"])
            reports = Report.objects(__raw__={'details.contract': contract.id}).delete()
            contract.delete()
            data = {'reports': str(reports)}
        return HttpResponse(json.dumps(data), content_type='application/json')


class GetContractFormView(LoginRequiredMixin, TemplateView):
    model = ContractBase

    def get_context_data(self, **kwargs):
        return super(GetContractFormView, self).get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if request.POST['get_current_form']:
            user = request.user
            user_profile = UserProfile.objects.get(user=user)
            contract_id = request.POST.get('contract_id', None)
            retailer_id = request.POST.get('retailer_id', None)
            date_to = request.POST.get('date_to', None)
            date_from = request.POST.get('date_from', None)
            contract_type = request.POST.get('contract_type', None)
            tariff_id = request.POST.get('tariff_id', None)
            triggered_by_date = request.POST.get('triggered_by_date', None)
            selected_meter = request.POST.get('selected_meter', None)
            selected_meter = Meter.objects.get(
                id=selected_meter)  # TODO 1 verify SupplyPoint.objects.get(id=selected_meter)

            contract = None
            if contract_id:
                contract = ContractBase.objects.get(id=contract_id)
            invoice_helper = InvoiceHelperFactory().get_handler(selected_meter, None, datetime.strptime(date_to,
                                                                                                        '%d/%m/%Y'))  # , supply_point, date_from=None, date_to=None, energy=None, flag=True, contract=None
            form = invoice_helper.get_contract_form_with_type(contract_type, contract, user_profile.utility,
                                                              selected_meter, retailer_id, tariff_id=tariff_id,
                                                              initial_data={"date_from": date_from,
                                                                            "date_to": date_to})
            template = invoice_helper.get_template_by_contract_type(contract_type)

            context["form"] = form
            context["contract_type"] = int(contract_type)
            context['date_to'] = date_to
            context['date_from'] = date_from
            context['triggered_by_date'] = triggered_by_date
            context['date_tariff_change_es'] = DATE_CHANGE_ES_INVOICE_HELPER_FACTORY

            return render(request, template, context)


class ComputeRecommendationsView(LoginRequiredMixin, TemplateView):
    model = SupplyPoint
    template_name = ''
    success_url = ''
    object = None
    message_response = ''

    def post(self, request, *args, **kwargs):
        from celery_once import AlreadyQueued
        message = None
        code_message = 1
        data = json.loads(request.POST.get('data'))

        meter = None
        building = None
        if "meter_id" in kwargs and kwargs["meter_id"]:
            meter = SupplyPoint.objects.get(id=kwargs["meter_id"])

        if "building_id" in kwargs and kwargs["building_id"]:
            building = Building.objects.get(id=kwargs["building_id"])

        recos = list(map(int, data['recos']))
        begin = datetime.strptime(data['date_range'][:10], '%Y-%m-%d')
        end = datetime.strptime(data['date_range'][-10:], '%Y-%m-%d')

        logger.info(f'Date range for reco calculation: {begin} - {end}')

        if isinstance(begin, str) and isinstance(end, str):
            logger.info(f'Date range string format')
        elif isinstance(begin, datetime) and isinstance(end, datetime):
            logger.info(f'Date range datetime format')

        by_building = "by_building" in self.request.POST
        reco_based_on = int(data["reco_based_on"]) if "reco_based_on" in data else 0
        from efficiency.manager import BASED_ON
        reco_based_on_name = BASED_ON.get(reco_based_on)
        try:
            from core.tasks import task_compute_recommendations_for_building, \
                task_compute_recommendations_for_supply_point
            from django.contrib.messages import constants
            from eslogging.manager import get_session_key_with_trigger_origin
            from eslogging.models import RecommendationTrace
            from config.settings.common import CALC_FROM_METER_PROFILE
            if not by_building and meter and CALC_FROM_METER_PROFILE:
                session_key = get_session_key_with_trigger_origin(request.user.username, RecommendationTrace.METER)
                task_compute_recommendations_for_supply_point.delay(supply_point_id=str(meter.id),
                                                                    user_id=str(request.user.id),
                                                                    based_on=reco_based_on, recos=recos, begin=begin,
                                                                    end=end, keep_dates=True, session_key=session_key)
                message = _(
                    'Se ha comenzado a recalcular las recomendaciones basadas en "%s" '
                    'para el cups "%s" del edificio "%s". '
                    % (reco_based_on_name, meter.cups, meter.building.name))
            else:
                task_compute_recommendations_for_building.delay(building_id=str(building.id),
                                                                user_id=str(request.user.id),
                                                                based_on=reco_based_on)
                message = _('Se ha comenzado a recalcular las recomendaciones basadas en "%s" para el edificio "%s".'
                            '') % (reco_based_on_name, building.name)

            code_message = 0
            # messages.info(request, message)
        except AlreadyQueued as e:
            message = _(
                'No se ha comenzado a recalcular las recomendaciones basadas en "%s" para el edificio "%s" porque se está '
                'ejecutando un cálculo anterior. Inténtelo mas tarde.') % (reco_based_on_name, meter.building.name)
            # messages.error(request, message)
        except Exception as e:
            message = _(
                'No se ha comenzado a recalcular las recomendaciones basadas en "%s" para el edificio "%s" '
                'porque ha ocurrido un error inesperado.') % (reco_based_on_name, meter.building.name)
            # messages.error(request, message)
        data = {'ok': 1, 'message': message, 'code_message': code_message}
        return HttpResponse(json.dumps(data), content_type='application/json')


class GetRecommendations(LoginRequiredMixin, TemplateView):
    template_name = ''

    def post(self, request, *args, **kwargs):
        response = {}
        reco_types = {'Instalación Autoconsumo Solar PV': 1,
                      'Optimización de la potencia': 4,
                      'Compensación de energía reactiva': 5,
                      'Compensación de energía reactiva con condensador fijo': 9,
                      'Instalación Autoconsumo Solar PV (RD 244/2019)': 10,
                      'Optimización de contrato': 11}
        data = json.loads(request.POST['data'])
        if 'meter' in data.keys():
            meter_id = data['meter']
            meter = SupplyPoint.objects.get(id=meter_id)
            aux = meter.building
        else:
            client_id = data['client']
            aux = Client.objects.get(id=client_id)

        response['reco'] = {}
        if aux.utility.parameters['rec_types']:
            for rec in aux.utility.parameters['rec_types']:
                response['reco'][rec.name] = reco_types[rec.name]
        else:
            for rec in DEFAULT_RECOS:
                response['reco'][list(reco_types.keys())[list(reco_types.values()).index(rec)]] = rec
        return HttpResponse(json.dumps(response), content_type='application/json')


class UpdateSigeConsumptionView(LoginRequiredMixin, TemplateView):
    model = SupplyPoint
    template_name = ''
    success_url = ''
    object = None
    message_response = ''

    def post(self, request, *args, **kwargs):
        from core.tasks import task_update_consumption
        from celery_once import AlreadyQueued
        code_message = 1
        meter = None

        if "meter_id" in kwargs and kwargs["meter_id"]:
            meter = SupplyPoint.objects.get(id=kwargs["meter_id"])
        try:
            task_update_consumption.delay(str(meter.id), str(request.user.id))
            message = _(f'Se ha comenzado a sincronizar los perfiles de carga para CUPS {meter.cups}')
            code_message = 0
        except AlreadyQueued as e:
            message = _(f'No se ha comenzado a sincronizar los perfiles de carga para CUPS {meter.cups}')
        except Exception as e:
            message = _(f'No se ha comenzado a sincronizar los perfiles de carga para medidor {meter.name} '
                        f'porque ha ocurrido un error inesperado.')
        data = {'ok': 1, 'message': message, 'code_message': code_message}
        return HttpResponse(json.dumps(data), content_type='application/json')


class DuplicateContractView(LoginRequiredMixin, TemplateView):
    model = ContractBase
    template_name = "contract/common/contract_form_layout.html"
    object = None
    form = None
    contract = None
    selected_meter = None

    @staticmethod
    def get_random_string(size=7):
        import random
        import string
        chars = string.ascii_lowercase + string.digits
        return ''.join(random.choice(chars) for _ in range(size))

    def _duplicate(self):
        try:
            from copy import deepcopy
            copy_contract = deepcopy(self.contract)
            copy_contract.id = None
            copy_contract.simulated = True
            base_name = copy_contract.to_string() if not self.contract.simulated else self.contract.simulated_name
            simulated_name = "%s_%s" % (base_name, self.get_random_string())

            copy_contract.simulated_name = simulated_name
            copy_contract.save()
        except:
            copy_contract = None

        return copy_contract

    def get_context_data(self, **kwargs):
        return super(DuplicateContractView, self).get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        self.selected_meter = Meter.objects.get(pk=kwargs["meter_id"])
        self.contract = ContractBase.objects.get(id=kwargs["contract_id"])

        copy_contract = self._duplicate()

        if copy_contract:
            messages.success(self.request, _('Se ha duplicado correctamente el contrato.'))
            url = reverse('contract_edit', kwargs={'building_id': self.selected_meter.building.id,
                                                   'meter_id': self.selected_meter.id,
                                                   'contract_id': copy_contract.id})
        else:
            messages.error(self.request, _('Ha ocurrido un error al duplicar el contrato.'))
            url = reverse('contract_edit', kwargs={'building_id': self.selected_meter.building.id,
                                                   'meter_id': self.selected_meter.id,
                                                   'contract_id': self.contract.id})
        return HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):
        self.selected_meter = Meter.objects.get(pk=kwargs["meter_id"])
        self.contract = ContractBase.objects.get(id=kwargs["contract_id"])

        copy_contract = self._duplicate()

        if copy_contract:
            messages.success(request, _('Se ha duplicado correctamente el contrato.'))
            url = reverse('contract_edit', kwargs={'building_id': self.selected_meter.building.id,
                                                   'meter_id': self.selected_meter.id,
                                                   'contract_id': copy_contract.id})
        else:
            messages.error(request, _('Ha ocurrido un error al duplicar el contrato.'))
            url = reverse('contract_edit', kwargs={'building_id': self.selected_meter.building.id,
                                                   'meter_id': self.selected_meter.id,
                                                   'contract_id': self.contract.id})

        return HttpResponse(json.dumps({"url": url}), content_type='application/json')


class SIPSSynchronizationView(LoginRequiredMixin, TemplateView):
    template_name = "sips/sips_synchronization_index.html"
    model = SupplyPoint

    cups = ''
    utility = None
    sips_data = None
    exist_cups = False

    def get_object(self, cups):
        user = self.request.user
        user_profile = UserProfile.objects.get(user=user)
        return get_meter_with_cups(cups, user_profile.utility, user)

    def get_context_data(self, **kwargs):
        context = super(SIPSSynchronizationView, self).get_context_data(**kwargs)
        from core.utils import get_locale_by_user
        context["locale"] = get_locale_by_user(self.request.user)
        return context

    def get_sips_info(self, context):
        from core.internal_services.sips.consumer import SIPSAPIHandler
        from core.internal_services.sips.schemas import SipsResultSchema
        try:
            sh = SIPSAPIHandler(self.utility, self.cups)
            self.sips_data = sh.cups_info
        except SIPSSystemError as e:
            context["message_error"] = str(e)
            context["error"] = 1
        except (NoDataReceived, InvalidCUPS, NotFoundCUPS, SIPSSystemError, InvalidSIPSCredentials,
                MissingSIPSCredentials) as e:
            context["message_error"] = str(e)
            context["error"] = 0
        except Exception as e:
            msg = "El sistema SIPS esta fuera de servicio."
            msg = msg + str(e) if self.request.user.is_staff else msg
            context["message_error"] = msg
            context["error"] = 1

        try:
            from core.common.cups_utils import CupsUtils
            self.exist_cups = self.get_object(CupsUtils.get_possible_cups(self.cups))
        except:
            self.exist_cups = None
        # try:
        #     sh = SIPSAPIHandler(self.utility)
        #     # possible_cups = sh.get_possible_cups(self.cups)
        #     from core.common.cups_utils import CupsUtils
        #     possible_cups = CupsUtils.get_possible_cups(self.cups)
        #     self.exist_cups = self.get_object(possible_cups)
        #
        #     try:
        #         df_ps, df_data = sh.get_SIPS_data(self.cups)
        #         self.sips_data = SIPSData(df_ps, df_data, context["locale"])
        #     except SIPSSystemError as e:
        #         context["message_error"] = str(e)
        #         context["error"] = 1
        #     except (NoDataReceived, InvalidCUPS, NotFoundCUPS, SIPSSystemError, InvalidSIPSCredentials,
        #             MissingSIPSCredentials) as e:
        #         context["message_error"] = str(e)
        #         context["error"] = 0
        #     except Exception as e:
        #         msg = "El sistema SIPS esta fuera de servico."
        #         msg = msg + str(e) if self.request.user.is_staff else msg
        #         context["message_error"] = msg
        #         context["error"] = 1
        # except Exception as e:
        #     msg = "El sistema SIPS esta fuera de servico."
        #     msg = msg + str(e) if self.request.user.is_staff else msg
        #     context["message_error"] = msg
        #     context["error"] = 1

        context["sips_data"] = self.sips_data
        # context["sips_data_json"] = self.sips_data.to_json() if self.sips_data else ""
        context["sips_data_json"] = SipsResultSchema().dumps(self.sips_data) if self.sips_data else ""
        context["exist_cups"] = 1 if self.exist_cups else 0

        if self.exist_cups:
            context["cups_edit_url"] = reverse('meters_edit', kwargs={'building_id': self.exist_cups.building.id,
                                                                      'meter_id': self.exist_cups.id})
            context["meter_id"] = str(self.exist_cups.id)
            context["cups"] = self.exist_cups.cups
        else:
            context["cups_edit_url"] = ""
            context["meter_id"] = ""
            context["cups"] = self.sips_data.cups if self.sips_data else self.cups
        return context

    def get_data(self, request):
        self.cups = request.POST.get('cups', '').upper()
        utility_id = get_utility(self.request.user)
        self.utility = Utility.objects.get(id=utility_id)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        self.get_data(request)
        context = self.get_sips_info(context)
        return render(request, "sips/cups_details_partial.html", context)


class LeadFormView(LoginRequiredMixin, TemplateView):
    template_name = "sips/register_lead_form_modal.html"
    template_info_name = "sips/register_lead_info_modal.html"
    form_class = RegisterLEADForm
    url_to_redirect = 'aev'
    trigger_origin = RecommendationTrace.FIND_CUPS

    client, building, supply_point, contract, rec = None, None, None, None, None

    def get_context_data(self, **kwargs):
        context = super(LeadFormView, self).get_context_data(**kwargs)
        from core.utils import get_locale_by_user
        context["locale"] = get_locale_by_user(self.request.user)
        return context

    def send_task_calculate_recommendations(self, building, reco_based_on):
        from core.tasks import task_compute_recommendations_for_building
        from eslogging.manager import get_session_key_with_trigger_origin

        session_key = get_session_key_with_trigger_origin(self.request.user.username, self.trigger_origin)
        task_compute_recommendations_for_building.delay(building_id=str(building.id), session_key=session_key,
                                                        user_id=str(self.request.user.id), based_on=reco_based_on)

    def get_url_to_redirect(self):
        return reverse(self.url_to_redirect,
                       kwargs={'building_id': self.building.id}) if self.code_message_form == 0 else ""

    def calculate_recommendations(self, building):
        from efficiency.manager import BASED_ON
        from celery_once import AlreadyQueued

        # ####### Calculate the recommendations based on SIPS data ########
        reco_based_on = 1  # SIPS
        reco_based_on_name = BASED_ON.get(reco_based_on)
        try:
            self.send_task_calculate_recommendations(building, reco_based_on)
            message_reco = _(
                'Se ha comenzado a recalcular las recomendaciones basadas en {0} esto puede tomar '
                'unos minutos.'.format(reco_based_on_name))

            code_message_reco = 0
        except AlreadyQueued as e:
            message_reco = _(
                'No se ha comenzado a recalcular las recomendaciones basadas en "%s" porque se está '
                'ejecutando un cálculo anterior. Inténtelo mas tarde.') % (reco_based_on_name)
            code_message_reco = 1
        except Exception as e:
            message_reco = _(
                'No se ha comenzado a recalcular las recomendaciones basadas en "%s" '
                'porque ha ocurrido un error inesperado.') % (reco_based_on_name)
            code_message_reco = 1
        return message_reco, code_message_reco

    def post(self, request, *args, **kwargs):
        from core.internal_services.sips.schemas import SipsResultSchema
        context = self.get_context_data(**kwargs)
        cups = request.POST.get('cups', '')
        save = request.POST.get('save', None)

        form = self.form_class(request.POST) if save else self.form_class(cups=cups)
        sips_data_json = request.POST.get('sips_data_json', None)
        message_reco = None
        code_message_reco = None
        sips_contract_type_name = ""
        if sips_data_json:
            sips_data = SipsResultSchema().loads(sips_data_json)
            if form.is_valid():
                try:
                    self.client, self.building, self.supply_point, self.contract, self.rec = form.save(sips_data,
                                                                                                       self.request.user,
                                                                                                       self.request.path)
                    message_form = _('Se ha guardado el cliente PROSPECTO {0}.'.format(self.client.name))
                    self.code_message_form = 0
                    sips_contract_type_name = self.contract.retailer.get_sips_contract_type_name(
                        self.contract.contract_type)
                    message_reco, code_message_reco = self.calculate_recommendations(self.building)
                    if not self.rec:
                        message_reco += " Aunque no se ha podido generar la medida para cambio de comercializadora."
                        code_message_reco = 1

                except (NotCreatedClientLEAD, NotCreatedBuildingFromLEAD, NotCreatedMeterFromLEAD,
                        NotCreatedContractFromLEAD, NotCreateInvoicesFromLEAD) as e:
                    message_form = str(e)
                    self.code_message_form = 1
                except Exception as e:
                    message_form = _('No se ha creado el cliente PRSPECTO porque ha ocurrido un error inesperado.')
                    self.code_message_form = 1

                context["message_form"] = message_form
                context["code_message_form"] = self.code_message_form
                context["sips_contract_type_name"] = sips_contract_type_name
                context["message_reco"] = message_reco
                context["code_message_reco"] = code_message_reco
                context["modal_id"] = "register_lead_info_modal"
                context["url_to_redirect"] = self.get_url_to_redirect()
                return render(request, self.template_info_name, context)

        context["form"] = form
        context["modal_id"] = "register_lead_modal"
        return render(request, self.template_name, context)


class ImportLoadCurveView(LoginRequiredMixin, TemplateView):
    template_name = "sips/sips_synchronization_index.html"

    def get_context_data(self, **kwargs):
        return super(ImportLoadCurveView, self).get_context_data(**kwargs)

    @staticmethod
    def download_load_curve_manual(request):
        from core.export_import.base import export_template
        return export_template('Manual de subida de curva de carga.pdf')

    @staticmethod
    def download_load_curve_template(request, meter_id, type):
        from metering.data_read import get_template_of_meter
        from core.export_import.utils import Exporter
        if meter_id == "new":
            headers = ['date', 'EAct imp', 'ERInd imp']
            template = pd.DataFrame(columns=headers)
            filename = "curva_plantilla_datos"
        else:
            meter = Meter.objects.get(id=meter_id)
            template = get_template_of_meter(meter)
            filename = meter.name + "_plantilla_datos"
        exporter = Exporter()
        return exporter.data_frame_as_response(filename, template, None, e_type=type)

    @staticmethod
    def download_load_curve_data(request, meter_id, type):
        from metering.data_read import get_data_all_2
        from core.export_import.utils import Exporter
        meter = Meter.objects.get(id=meter_id)
        now = datetime.now()
        date_to = datetime(day=now.day, month=now.month, year=now.year, hour=0, minute=0, second=0)
        date_from = now - timedelta(days=365)
        data = get_data_all_2(meter, date_from, date_to)

        exporter = Exporter()
        filename = meter.name + "_" + date_to.strftime("%d%m%Y_%H:%M:%S")
        return exporter.data_frame_as_response(filename, data, None, e_type=type)

    def post(self, request, *args, **kwargs):
        from core.tasks import task_import_datalog
        from config.settings.common import MEDIA_IMPORTS_ROOT

        file = request.FILES['loadCurve']
        meter_id = kwargs.get("meter_id")
        meter = Meter.objects.get(id=meter_id)
        user_profile = UserProfile.objects.get(user=request.user)

        code_message = 1

        if user_profile in UserProfile.objects.filter(is_demo=True):
            message = _('¡Eres un usuario demo! Si quieres dejar de ser demo contacta con dept.support@bettergy.es')

        elif meter.comm_device_id is None or meter.comm_device_id == '':
            message = _('¡El medidor no tiene id de dispositivo! Por favor guarde el medidor con un id de dispositivo')

        else:
            # the file is going to be an instance of UploadedFile
            filepath = Path(os.path.join(MEDIA_IMPORTS_ROOT, file.name))
            with open(filepath, 'wb+') as f:
                for chunk in file.chunks():
                    f.write(chunk)
            handle_upload_file_to_cloud_bucket(filepath)
            from celery_once import AlreadyQueued
            try:
                task_import_datalog.delay(str(filepath), meter_id, str(request.user.id))
                code_message = 0
                message = _('Los datos están siendo procesados, puede seguir navegando')
                messages.success(request, message)
            except AlreadyQueued as e:
                message = _(
                    'No se ha comenzado a procesar los datos porque se está éjecutando una '
                    'carga anterior. Inténtelo mas tarde.')
                messages.error(request, message)
            except Exception as e:
                message = _(
                    'No se ha comenzado a procesar los datos porque porque ha ocurrido un error inesperado.')
                messages.error(request, message)

        return HttpResponse(json.dumps({"message": message, "code_message": code_message}),
                            content_type='application/json')
