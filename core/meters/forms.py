# from mongoengine import *
from django.forms import *
from django.forms import forms
from django.forms import fields as d_fields
from core.tariff_system.common.models import *
from core.devices.models import Meter, SupplyPoint, SubMeter, UseCategory, EnergySource
from mongodbforms import DocumentForm
from django.utils.translation import ugettext_lazy as _
# from datetime import datetime
from django.forms.utils import ErrorList
from eslogging.manager import UserTraceManager
import logging
logger = logging.getLogger(__name__)
# from dateutil.relativedelta import relativedelta


class DocumentFormUtils(object):

    def assign_all_errors_to_fields(self, errors):
        if "__all__" in errors:
            for error_list in errors["__all__"].data:
                for field, error in error_list.message.items():
                    if field not in errors:
                        errors[field] = ErrorList()
                    errors[field].extend(error)

        return errors


class MeterForm(DocumentForm):
    categorias = UseCategory.objects.all().order_by('name')
    sources = EnergySource.objects.all()
    speeds = Meter.SPEED
    parities = Meter.PARITY

    name = CharField(widget=TextInput(attrs={'class': 'form-control'}))
    usage = ListField(required=True, queryset=categorias,
                      widget=SelectMultiple(attrs={'class': 'input-md form-control input-s-sm inline', 'size': 10}))
    energy_sources = ReferenceField(document_type=EnergySource, queryset=sources,
                                    widget=RadioSelect(attrs={'class': 'input-md form-control input-s-sm inline'}))
    comm_devname = CharField(required=False, widget=TextInput(
        attrs={'class': 'form-control'}))  # , 'readonly': 'readonly'
    comm_device_id = CharField(required=False, widget=TextInput(attrs={'class': 'form-control'}))
    datalogger_id = CharField(required=False, widget=TextInput(attrs={'class': 'form-control'}))
    comm_params = CharField(required=False,
                            widget=TextInput(attrs={'class': 'form-control'}))
    reading_key = FloatField(required=False, localize=True, widget=NumberInput(
            attrs={'class': 'form-control'}))
    speed = ListField(queryset=speeds, empty_label=None, widget=Select(attrs={'class': 'form-control'}))
    parity = ListField(queryset=parities, empty_label=None, widget=Select(attrs={'class': 'form-control'}))
    gate_link = FloatField(required=False, widget=NumberInput(
            attrs={'class': 'form-control'}))
    address_measure_point = FloatField(required=False, localize=True, widget=NumberInput(
            attrs={'class': 'form-control'}))
    observations = CharField(required=False, widget=Textarea(attrs={'class': 'form-control', 'rows': 3, 'cols': 40}))
    is_supply_point = NullBooleanField(required=False, widget=CheckboxInput(attrs={'class': 'form-control js-switch', 'onchange': "showContent()"}))  # , initial=True)

    # meter_type_choices = [(f[0], str(f[1])) for f in Meter.METER_TYPE]
    # meter_type = ChoiceField(choices=Meter.METER_TYPE, required=True, widget=Select(attrs={'class': 'selectpicker input-md form-control input-s-sm inline'}))

    has_remote_conection = BooleanField(required=False)
    esb_sync = BooleanField(required=False, default=False)
    esb_meter_id = CharField(required=False, widget=TextInput(attrs={'class': 'form-control', 'pattern': '[^\s]*', 'title': 'Espacios en blanco no estan permitidos'}))
    esb_meter_contract_id = CharField(required=False, widget=TextInput(attrs={'class': 'form-control', 'pattern': '[^\s]*', 'title': 'Espacios en blanco no estan permitidos'}))

    def get_datalogger_id(self):
        switcher = {
            "Solelec": "S0L3L3C"
        }

    class Meta:
        model = Meter
        fields = ['name', 'is_supply_point', 'usage', 'energy_sources', 'comm_devname', 'comm_params', 'reading_key',
                  'speed', 'parity', 'gate_link', 'address_measure_point', 'cups', 'company', 'comm_device_id',
                  'datalogger_id', 'observations', 'has_remote_conection', 'esb_sync', 'esb_meter_id',
                  'esb_meter_contract_id']  # , 'device_model', 'meter_type'

    def __init__(self, *args, **kwargs):
        super(MeterForm, self).__init__(*args, **kwargs)


class SupplyPointForm(MeterForm):
    utilities = Utility.objects.all()

    cups = CharField(widget=TextInput(attrs={'class': 'form-control', 'placeholder': 'ESXXXXXXXXXXXXXXXXXXX'}),
                     required=True, min_length=20, max_length=22, label=_('CUPS'))
    company = ListField(required=False, queryset=utilities, empty_label=None,
                        widget=Select(attrs={'class': 'form-control'}))

    class Meta:
        model = SupplyPoint
        fields = ['name', 'is_supply_point', 'usage', 'energy_sources', 'comm_devname', 'comm_params', 'reading_key',
                  'speed', 'parity', 'gate_link', 'address_measure_point', 'cups', 'company', 'comm_device_id',
                  'datalogger_id', 'observations', 'has_remote_conection', 'esb_sync', 'esb_meter_id',
                  'esb_meter_contract_id']  # , 'device_model', 'meter_type'


class SubmeterForm(MeterForm):
    PARENTS = None

    def __init__(self, *args, **kwargs):
        try:
            self.building_id = kwargs.pop("building_id")
            super(SubmeterForm, self).__init__(*args, **kwargs)
            self.PARENTS = SupplyPoint.objects.filter(building=self.building_id)
            self.fields['parent'].queryset = self.PARENTS
        except Exception:
            super(SubmeterForm, self).__init__(*args, **kwargs)

    parent = ReferenceField(required=False, document_type=Meter, queryset=PARENTS,
                            widget=Select(attrs={'class': 'input-md form-control input-s-sm inline'}))

    class Meta:
        model = SubMeter
        fields = ['name', 'cups', 'company', 'is_supply_point', 'usage', 'energy_sources', 'energy_sources',
                  'comm_devname', 'comm_params', 'reading_key', 'speed', 'parity', 'gate_link',
                  'address_measure_point', 'parent', 'comm_device_id', 'datalogger_id', 'observations',
                  'has_remote_conection' , 'esb_sync', 'esb_meter_id',
                  'esb_meter_contract_id']


class RegisterLEADForm(forms.Form):
    client_name = d_fields.CharField(label=_("Nombre de cliente"), max_length=250, required=True,
                                     widget=TextInput(attrs={'class': 'form-control'}))  # is_lead and assessors
    building_name = d_fields.CharField(label=_("Nombre de instalación"), max_length=250, required=True,
                                       widget=TextInput(attrs={'class': 'form-control'})) # country
    # building, datalogger_id,cups and has_remote_connection and company = ReferenceField(Utility)
    meter_name = d_fields.CharField(label=_("Nombre de medidor"), max_length=250, required=True,
                                    widget=TextInput(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        cups = kwargs.pop("cups") if "cups" in kwargs else None
        super(RegisterLEADForm, self).__init__(*args, **kwargs)
        if cups:
            self.fields["client_name"].initial = f'{_("Cliente")} {cups}'
            self.fields["building_name"].initial = f'{_("Instalación")} {cups}'
            self.fields["meter_name"].initial = f'{_("Medidor")} {cups}'

    def save(self, sips_data, user, path=""):
        from core.utils import get_utility
        from core.tariff_system.common.models import Utility
        from core.meters.utils import LEADFormHandler
        from core.buildings.models import Client

        utility_id = get_utility(user)
        utility = Utility.objects.get(id=utility_id)

        form_handler = LEADFormHandler(sips_data, utility, user, self.data, lead_origin=Client.SIPS_SYNC)
        lead, building, meter, contract = form_handler.generate_lead()
        generate_reco = True
        return lead, building, meter, contract, generate_reco


class EnergyOfferRegisterLEADForm(RegisterLEADForm):

    def __init__(self, *args, **kwargs):
        super(EnergyOfferRegisterLEADForm, self).__init__(*args, **kwargs)

    def save(self, sips_data, user, path=""):
        from core.utils import get_utility
        from core.tariff_system.common.models import Utility
        from efficiency.engines.retailer_change import RetailerChangeRecommendationEngine
        from efficiency.manager import EfficiencyManager
        from core.meters.utils import LEADFormHandler
        from core.buildings.models import Client
        from core.reporting.models import Report, Invoice
        import json

        rec = None
        utility_id = get_utility(user)
        utility = Utility.objects.get(id=utility_id)

        form_handler = LEADFormHandler(sips_data, utility, user, self.data, lead_origin=Client.SIPS_SYNC)
        lead, building, meter, c_contract, s_contract = form_handler.generate_lead_with_prices()
        UserTraceManager().register(user, path, 'cups_saved', value=sips_data.cups)

        current_invoice = json.loads(self.data['current_invoice'])
        simulated_invoice = json.loads(self.data['simulated_invoice'])
        invoices = json.loads(self.data['invoices'])
        current_costs = invoices['current']
        simulated_costs = invoices['simulated']

        try:
            engine = RetailerChangeRecommendationEngine()
            engine.based_on = Report.INVOICE_SIPS
            rec = engine.evaluate(meter, current_invoice, simulated_invoice, current_costs, simulated_costs, c_contract, s_contract)
            created, accepted = EfficiencyManager.save_recommendation(rec)
        except Exception as ex:
            logger.error("[EnergyOfferRegisterLEADForm] Error evaluate RetailerChangeRecommendation for meter "
                          + meter.name + " with id " + str(meter.id))

        return lead, building, meter, c_contract, rec


class EditEnergyOfferRegisterLEADForm(EnergyOfferRegisterLEADForm):

    def save(self, sips_data, user):
        from core.utils import get_utility
        from core.tariff_system.common.models import Utility
        from efficiency.engines.retailer_change import RetailerChangeRecommendationEngine
        from efficiency.manager import EfficiencyManager
        from core.meters.utils import LEADFormHandler
        from core.buildings.models import Client
        from core.reporting.models import Report, Invoice
        import json

        utility_id = get_utility(user)
        utility = Utility.objects.get(id=utility_id)
        generate_reco = True

        current_invoice = json.loads(self.data['current_invoice'])
        simulated_invoice = json.loads(self.data['simulated_invoice'])
        invoices = json.loads(self.data['invoices'])
        current_costs = invoices['current']
        simulated_costs = invoices['simulated']

        form_handler = LEADFormHandler(sips_data, utility, user, self.data, lead_origin=Client.SIPS_SYNC)
        lead, building, meter, c_contract, s_contract = form_handler.generate_lead_with_prices()

        try:
            engine = RetailerChangeRecommendationEngine()
            engine.based_on = Report.INVOICE_SIPS
            rec = engine.evaluate(meter, current_invoice, simulated_invoice, current_costs, simulated_costs, c_contract, s_contract)
            created, accepted = EfficiencyManager.save_recommendation(rec)
        except Exception as ex:
            logger.error("[EnergyOfferRegisterLEADForm] Error evaluate RetailerChangeRecommendation for meter "
                          + meter.name + " with id " + str(meter.id))
            generate_reco = False

        return lead, building, meter, c_contract, generate_reco
