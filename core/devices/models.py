from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import FloatField, IntField, EmbeddedDocumentField, \
    ReferenceField, StringField, ListField, DateTimeField, EmbeddedDocumentListField, BooleanField
# from mongoengine import CASCADE
from mongoengine.errors import ValidationError
from core.tariff_system.common.models import ContractBase, Utility
from datetime import datetime, timedelta
from core.tariff_system.common.models import Market
import numpy as np
from django.utils.translation import ugettext_lazy as _


class UseCategory(Document):
    name = StringField(max_length=250, null=True, blank=True)

    def __str__(self):
        return '%s' % self.name


class EnergySource(Document):
    SOURCES = (
        (1, _('Electricidad')),
        (2, _('Agua')),
        (3, _('Gas'))
    )

    name = IntField(choices=SOURCES, required=False, default=1)

    def __str__(self):
        return '%s' % self.name


class DeviceModel(Document):
    name = StringField(max_length=250, null=True, blank=True)
    energy_source = ReferenceField(EnergySource)

    def __str__(self):
        return '%s' % self.name


class InvoicingCycle(Document):
    code = IntField(min_value=1, max_value=100)
    description = StringField(null=True, blank=True)

    def __str__(self):
        return '%s' % self.description


class InvoicingCycleEmbedded(EmbeddedDocument):
    code = IntField(min_value=1, max_value=100)
    description = StringField(null=True, blank=True)
    reference = ReferenceField('InvoicingCycle', null=True, blank=True)  # Dejar las comillas porque sino no funciona

    def __str__(self):
        return '%s' % self.description

    def create(self, code, **kwargs):
        self.code = code
        if self.code:
            try:
                self.reference = InvoicingCycle.objects.get(code=self.code)
                self.description = self.reference.description
            except Exception as e:
                print(e)


class MarketCode(EmbeddedDocument):
    code = StringField()
    operator = StringField()
    std = ReferenceField('Market', null=True, blank=True)  # Dejar las comillas porque sino no funciona

    def __str__(self):
        return self.operator

    def create(self, code, **kwargs):
        self.code = code
        if self.code:
            try:
                # from core.tariff_system.co.models import Market
                market = Market.objects.get(code=int(self.code))
                self.operator = market.operator
                self.std = market
            except Exception as e:
                print(e)


class TypicalPattern(EmbeddedDocument):
    DAILY = 0
    WEEKLY = 1
    MONTHLY = 2
    SEASONAL = 3
    YEARLY = 4
    PERIOD = (
        (DAILY, _('Daily')),
        (WEEKLY, _('Weekly')),
        (MONTHLY, _('Monthly')),
        (SEASONAL, _('Seasonal')),
        (YEARLY, _('Yearly')),
    )

    WINTER = 1
    SUMMER = 2
    SPRING = 3
    AUTUMN = 4
    ALL = 5
    SEASON = (
        (WINTER, _('Winter')),
        (SPRING, _('Spring')),
        (SUMMER, _('Summer')),
        (AUTUMN, _('Autumn')),
        (ALL, _('All seasons')),
    )

    NON_WORKING_DAY = 0
    WORKING_DAY = 1
    WORKING = (
        (NON_WORKING_DAY, _('Dia no laboral')),
        (WORKING_DAY, _('Dia laboral')),
    )

    calculated = DateTimeField(null=True, blank=True)
    centroids = ListField(null=False, blank=False)
    labels = ListField(null=False, blank=False)
    measure = IntField(null=True, default=0)
    period = IntField(null=True, default=0, choices=PERIOD)
    season = IntField(null=True, default=0, choices=SEASON)
    name = StringField(max_length=250, null=True, blank=True)
    working = IntField(null=True, default=0, choices=WORKING)
    frequency = IntField(null=True, default=900)
    base_load = FloatField(null=False, blank=False)
    peak_load = FloatField(null=False, blank=False)
    duty_cycle = FloatField(null=False, blank=False)
    duty_cycle_start = DateTimeField(null=True, blank=True)


class Meter(Document):
    SPEED = (
        (1, 1200),
        (2, 2400),
        (3, 3600),
        (4, 4800),
        (5, 6000),
        (6, 7200),
        (7, 8400),
        (8, 9600)
    )

    PARITY = (
        (1, 'P'),
        (2, 'I'),
        (3, 'N'),
    )

    ACTIVE = 1
    INACTIVE = 2

    STATUS = (
        (ACTIVE, _('Activo')),
        (INACTIVE, _('Inactivo')),
    )

    METER_TYPE = (
        (0, _('Punto de Suministro')),
        (1, _('Sub Medidor')),
        # (2, _('Feature Meter')), #TODO: Add this option when this type of meter has been enabled
        (3, _('Consumo')),
        (4, _('Producción autoconsumo')),
    )

    building = ReferenceField('Building')
    building_name = StringField(max_length=250, null=True, blank=True)
    name = StringField(max_length=250, null=True, blank=True)

    description = StringField(max_length=250, null=True, blank=True)
    device_model = ReferenceField(DeviceModel, null=True, blank=True)
    market = StringField(max_length=250, null=True, blank=True)
    market_code = EmbeddedDocumentField(MarketCode, null=True, blank=True)

    tags = StringField(max_length=250, null=True, blank=True)  # Location
    usage = ListField(ReferenceField(UseCategory))
    energy_sources = ReferenceField(EnergySource)

    comm_devname = StringField(max_length=250, null=True, blank=True)
    comm_device_id = StringField(max_length=250, null=True, blank=True)
    comm_params = StringField(max_length=250, null=True, blank=True)  # phone
    reading_key = IntField(null=True, blank=True, min_value=1)  # max_value=65535
    speed = IntField(choices=SPEED, required=False)
    parity = IntField(choices=PARITY, required=False)
    gate_link = IntField(null=True, blank=True, min_value=1)  # max_value=65535
    address_measure_point = IntField(null=True, blank=True, min_value=1, default=1)  # max_value=65535

    voltage_level = StringField(max_length=15, null=True, blank=True)
    active_owner = StringField(max_length=250, null=True, blank=True)
    # TODO delete after migration
    invoicing_cycle = ReferenceField(InvoicingCycle, null=True, blank=True)
    invoice_cycle = EmbeddedDocumentField(InvoicingCycleEmbedded, null=True, blank=True)
    installed_power = FloatField(null=True, blank=True, min_value=1)

    datalogger_id = StringField(max_length=250, null=True, blank=True)
    status = IntField(null=True, default=2, choices=STATUS)

    observations = StringField(max_length=250, null=True, blank=True)
    phone = StringField(max_length=250, null=True, blank=True, required=False)

    tariff_name = StringField(max_length=250, null=True, blank=True)
    typical_pattern = EmbeddedDocumentListField(TypicalPattern, default=[])
    contract_power = ListField(FloatField(null=True, blank=True))
    power_prices = ListField(FloatField(null=True, blank=True))
    energy_prices = ListField(FloatField(null=True, blank=True))

    # TODO delete after migration
    cups = StringField(max_length=22, null=True, blank=True, required=False)
    # TODO delete after migration
    contract_list = ListField(ReferenceField(ContractBase))
    # TODO delete after migration
    company = ReferenceField(Utility)
    # TODO delete after migration
    parent = ReferenceField('Meter')

    has_remote_conection = BooleanField(default=False)

    pow_max_BIEW = FloatField(null=True, blank=True, default=0.0)
    applies_social_bonus = BooleanField(default=False)

    total_consumption = FloatField(default=0.0)
    base_consumption = FloatField(default=0.0)
    last_year_cost = FloatField(default=0.0)
    saving_potential = FloatField(default=0.0)
    energy_saving_potential = FloatField(default=0.0)

    meter_type = IntField(null=True, default=3, choices=METER_TYPE)

    # ESB variables
    esb_sync = BooleanField(default=False, required=False)
    esb_meter_id = StringField(max_length=250, required=False)
    esb_register_id = StringField(required=False)
    esb_provider_id = StringField(required=False)
    esb_meter_contract_id = StringField(max_length=250, required=False)

    meta = {'collection': 'meter', 'strict': False, 'allow_inheritance': True}

    def save(self, *args, **kwargs):
        super(Meter, self).save(*args, **kwargs)
        self.update_building_name()
        self.update_tariff_name()
        self.update_meter_type()
        if not self.comm_device_id or self.comm_device_id == "":
            self.set_default_com_device_id()
        super(Meter, self).save(*args, **kwargs)

    def set_default_com_device_id(self):
        from app.random_utils import generate_secure_random_string
        r_str = generate_secure_random_string()
        self.comm_device_id = r_str

    def update_building_name(self):
        self.building_name = self.building.name

    def update_tariff_name(self):
        contract = self.get_recent_contract()
        self.tariff_name = contract.tariff.name if contract and contract.tariff else ''
        # TODO 0 Not all tariff support tariff name, for example Colombia

    def update_meter_type(self):
        # TODO Implement logic depending on usages type selection, for now all meter types are consumption
        pass

    def is_supply(self):
        pass

    def get_last_update(self, yesterday):
        from datasources.datalogger.models import DataLog
        data_log = DataLog._get_collection()
        a = data_log.aggregate([
            {'$match': {'datalogger_id':self.datalogger_id, 'device_id': self.comm_device_id, 'date': {"$gte": yesterday}}},
            {'$group': {'_id': '$device_id', 'date': {'$max': '$date'}}}
        ])
        try:
            last_date = a.next()['date']
        except:
            return None
        return last_date

    def get_status_of_meter(self):
        yesterday = (datetime.now() - timedelta(days=2, hours=1))
        last_data = self.get_last_update(yesterday)
        if last_data and last_data > yesterday:
            self.status = self.ACTIVE
            # self.save() #TODO THIS IS VERY SLOW FOR LOAD METER FORM PAGE, THE CASE OF EPCOS HAVE 30 METERS!!!
        else:
            self.status = self.INACTIVE
            # self.save()
        return self.status

    def get_recent_contract(self):
        pass

    def get_current_contract(self, date_from=None, date_to=None):
        pass

    def get_contracts(self):
        return ContractBase.objects(supply_point=self)

    def get_current_tariff_name(self):
        pass

    def client_name(self):
        return self.get_client_name()

    def building_photo(self):
        return self.building.photo

    def client_logo(self):
        return self.building.utility_logo

    def get_tariff_name(self):
        try:
            contract = self.get_recent_contract()
            tariff_name = contract.tariff.name if contract and contract.tariff else None
        except:
            tariff_name = None
        return tariff_name

    def get_client_name(self):
        if self.building.owner_name:
            return self.building.owner_name
        try:
            client_name = self.building.owner.name if self.building.owner else None
        except:
            client_name = None
        return client_name

    def __str__(self):
        return self.name


class SupplyPoint(Meter):
    cups = StringField(null=True, blank=True, required=False)
    company = ReferenceField(Utility)

    # AUDINFOR REFERENCES
    client_id = IntField(null=True, blank=True)
    contract_list = ListField(ReferenceField(ContractBase))

    def is_supply(self):
        return True

    def get_recent_contract(self):
        return ContractBase.objects.filter(supply_point=self.id, simulated=False).order_by("-date_to").first()

    def get_current_contract(self, date_from=None, date_to=None):
        contract = self.get_current_contracts(date_from=date_from, date_to=date_to).first()
        if not contract:
            contract = self.get_recent_contract()
        return contract

    def get_current_contracts(self, date_from=None, date_to=None, simulated=False):
        if not date_to:
            date_to = datetime.now()
        if not date_from:
            date_from = date_to - timedelta(days=365)
        return ContractBase.objects.filter(supply_point=self, date_from__lte=date_to, date_to__gte=date_from,
                                           simulated=simulated).order_by("-date_to")

    def get_current_contracts_simulated(self, date_from=None, date_to=None):
        return self.get_current_contracts(date_from=date_from, date_to=date_to, simulated=True)

    def get_current_tariff_name(self):
        try:
            tariff_name = self.get_current_contract().tariff.name
        except Exception as ex:
            tariff_name = ''
        return tariff_name

    def get_power_prices(self):
        contract = self.get_current_contract()
        return np.round(contract.power_prices, 5).tolist()

    def get_energy_prices(self):
        contract = self.get_current_contract()
        return np.round(contract.energy_prices, 5).tolist()

    def clean(self):
        cleaned_data = self
        errors = {}

        # Clean data
        if not cleaned_data.cups:
            cleaned_data.cups = "ESXXXXXXXXXXXXXXXXXX"
            # errors['cups'] = [_('Debe insertar un CUPS')]
        elif len(cleaned_data.cups) < 20 or len(cleaned_data.cups) > 22:
            if 'cups' not in errors or not errors['cups']:
                errors['cups'] = []
            errors['cups'].append(_('La longitud del CUPS debe ser entre 20 y 22 dígitos'))

        if errors:
            raise ValidationError(errors)

        return cleaned_data

    def get_pv_submeter(self):
        from core.utils import get_use_category_pv
        use_cat_pv = get_use_category_pv()
        submeters_pv_qs = SubMeter.objects(parent=self, usage__contains=use_cat_pv)
        return submeters_pv_qs.first() if submeters_pv_qs else None

    meta = {'strict': False}

    def __str__(self):
        return '%s' % self.name


class SubMeter(Meter):
    parent = ReferenceField('Meter')

    def is_supply(self):
        return False

    def get_recent_contract(self):
        if not self.parent:
            return None
        return ContractBase.objects.filter(supply_point=self.parent.id, simulated=False).order_by("-date_to").first()

    def get_current_contract(self, date_from=None, date_to=None):
        if not date_to:
            date_to = datetime.now()
        if not date_from:
            date_from = date_to - timedelta(days=365)
        rate = ContractBase.objects.filter(supply_point=self.parent, date_from__lte=date_to,
                                           date_to__gte=date_from, simulated=False).order_by("-date_to").first()
        if not rate:
            rate = self.get_recent_contract()
        return rate

    meta = {'strict': False}

    def __str__(self):
        return '%s' % self.name


class FeatureMeter(Meter):
    parent = ReferenceField('Meter')
    # feature_name = StringField(max_length=100, null=True, blank=True, required=False)

    def is_supply(self):
        return False

    def get_recent_contract(self):
        if not self.parent:
            return None
        return ContractBase.objects.filter(supply_point=self.parent.id, simulated=False).order_by("-date_to").first()

    def get_current_contract(self, date_from=None, date_to=None):
        if not date_to:
            date_to = datetime.now()
        if not date_from:
            date_from = date_to - timedelta(days=365)
        rate = ContractBase.objects.filter(supply_point=self.parent, date_from__lte=date_to,
                                           date_to__gte=date_from, simulated=False).order_by("-date_to").first()
        if not rate:
            rate = self.get_recent_contract()
        return rate

    meta = {'strict': False}

    def __str__(self):
        return '%s' % self.name
