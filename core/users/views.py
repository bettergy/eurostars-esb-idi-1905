import requests
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.translation import gettext as _, ugettext_lazy as _u, get_language
from django_mongoengine.mongo_auth.managers import get_user_document
from django_mongoengine.mongo_auth.models import MongoUser
from django_mongoengine.views import DeleteView
from mongoengine import DoesNotExist, NotUniqueError, OperationError, ValidationError

from authz.models import UserProfile
from eslogging.models import UserTrace
from core.buildings.models import Client, Building
from core.monitorization.models import UserPreferences
from core.portfolio.profile.mixins import ProfileMixin
from core.tariff_system.common.models import Utility
from core.portfolio.profile.forms import UserProfileForm
from core.users.permissions import UserPermissions, Permission
from core.users.utils import ManageDemoUser
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, Http404
from django.urls import reverse_lazy, reverse
from django.views.generic import UpdateView, CreateView
from .mixins import UserRequiredAccessMixin
import logging

from ..cleanup.classes import DatabaseCleanup

logger = logging.getLogger(__name__)

User = get_user_document()


class UserDetailView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'users/profile.html'
    object = None

    def get_object(self, queryset=None):
        try:
            self.object = MongoUser.objects.get(id=self.kwargs['pk'])
        except (User.DoesNotExist, DoesNotExist):
            raise Http404(_('The user does not exist'))
        return self.object

    def get_success_url(self):
        return reverse('core.users:client_profile', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(UserDetailView, self).get_context_data(**kwargs)
        user_profile = UserProfile.objects.get(user=self.object)
        context['user_profile'] = user_profile
        context['user_id'] = str(self.object.id)
        return context



class UserCreateView(LoginRequiredMixin, ProfileMixin, CreateView):
    model = UserProfile
    template_name = ''
    form_class = UserProfileForm
    object = None

    def get_object(self, queryset=None):
        self.object = self.model()
        return self.object

    def post(self, request, *args, **kwargs):
        success = False
        utility = None
        form_errors = []
        message = None

        if self.profile is None:
            self.profile = self.get_profile(**{'profile_id': self.kwargs.get('profile_id')})

        form = UserProfileForm(request.POST, request.FILES, instance=self.get_object(), **{'profile': self.profile})
        # form = UserProfileForm(request.POST, request.FILES, instance=None, **{'profile': self.profile})

        if form.is_valid():
            # try:
            #     user = User.objects.get(username=form.cleaned_data['username'])
            # except(User.DoesNotExist,
            # ):
            #     user = False
            #
            # if user:
            #     form_errors.append({"field": 'username',
            #                         "errors": [_u('El usuario "{0}" ya existe en plataforma'.format(user.username))]})
            # else:
            if isinstance(self.profile, Client):
                utility = self.profile.utility
            elif isinstance(self.profile, Utility):
                utility = self.profile

            user = User()
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.username = form.cleaned_data['username']
            user.email = form.cleaned_data['email']
            user.is_active = form.cleaned_data['is_active']
            user.is_staff = False
            user.is_superuser = False

            password = MongoUser.objects.make_random_password()
            if request.POST.get('password'):
                password = request.POST.get('password')

            user.set_password(password)

            self.object = form.save(commit=False)
            self.object.utility = utility
            if isinstance(self.profile, Client):
                self.object.client = self.profile
            self.object.is_demo = utility.is_demo if utility.is_demo else False

            permissions = UserPermissions.get_permissions_of_user(user_profile=self.object)

            if permissions is Exception:
                raise (ValidationError(permissions))

            permissions = list(permissions)
            if utility.country != 'CO' and not self.object.is_demo:
                permissions.append(Permission.objects.get(codename='access_power_optimization'))

            if utility.name == 'Solelec':
                permissions.append(Permission.objects.get(codename='not_access_analysis'))

            user.user_permissions = []
            for permission in permissions:
                user.user_permissions.append(permission)

            user.save()

            self.object.user = user
            if len(form.files) > 0:
                self.object.image = form.files['image']

            self.object.save()

            if self.object.is_demo:
                try:
                    from core.demo.tasks import task_create_demo_user_data
                    # verificar si es necesario crear los datos demos
                    create_demo_data = True
                    if isinstance(self.object.client, Client):
                        if self.object.client.buildings.count():
                            create_demo_data = False
                    else:
                        if Building.objects(utility=utility.id).count():
                            create_demo_data = False
                    if create_demo_data:
                        # TODO: [CELERY] comment the next line to use asynchronous task
                        task_create_demo_user_data(user_id=str(user.id), user_profile_id=str(self.object.id),
                                                   utility_id=str(utility.id))
                        # TODO: [CELERY] uncomment the next line to use asynchronous task
                        # task_create_demo_user_data.delay(user_id=str(user.id), user_profile_id=str(self.object.id), utility_id=str(utility.id))
                except Exception as e:
                    print('Error al generar los datos de usuario demo. {0}'.format(e))
            else:
                permissions = UserPermissions.get_permissions_of_user(user_profile=self.object)

                if permissions is Exception:
                    raise (ValidationError("This user not have valid role"))

                permissions = list(permissions)
                if utility.country != 'CO':
                    permissions.append(Permission.objects.get(codename='access_power_optimization'))

                if utility.name == 'Solelec':
                    permissions.append(Permission.objects.get(codename='not_access_analysis'))

                user.user_permissions = []
                for permission in permissions:
                    user.user_permissions.append(permission)
                user.save()

            if self.object.client and int(self.object.role) == UserProfile.ASSESSOR and not self.object.is_demo:
                try:
                    from core.tasks import task_update_client_assessors
                    # TODO: [CELERY] comment the next line to use asynchronous task
                    task_update_client_assessors(str(self.object.id))
                    # TODO: [CELERY] uncomment the next line to use asynchronous task
                    # task_update_client_assessors.delay(str(self.object.id))
                except Exception as e:
                    print('[ERROR] Asociacion de usuario con rol asesor (id: {0}) con su '
                          'cliente. {1}'.format(self.object.id, e))

            send_pass = request.POST.get('send_pass')
            if send_pass:
                from authz.views import createToken, TokenUserMailSender
                token = createToken(user)
                service_password = TokenUserMailSender()
                service_password.send_mail(user.first_name, user.username,
                                           [user.email], token, get_language())
                print('Email enviado al usuario "{0}"'.format(user.username))

            success = True
            message = _('El usuario "{0}" ha sido creado satisfactoriamente'.format(user.username))

        for field in form.errors:
            form_errors.append({"field": field, "errors": form.errors[field]})

        if len(form_errors):
            message = _('El formulario contiene errores de validación, por favor compruebe')

        return JsonResponse({'success': success, 'errors': form_errors, 'message': message})

    def get_success_url(self):
        return reverse_lazy('utility_users', kwargs={'utility_id': self.kwargs['utility_id']})


class UserEditView(LoginRequiredMixin, UserRequiredAccessMixin, UpdateView):
    model = UserProfile
    template_name = ''
    form_class = UserProfileForm
    object = None

    def get_object(self, queryset=None):
        try:
            self.object = UserProfile.objects.get(id=self.kwargs['pk'])
        except (UserProfile.DoesNotExist, DoesNotExist):
            raise Http404(_('The user profile does not exist'))
        return self.object

    def post(self, request, *args, **kwargs):
        success = False
        form_errors = []
        message = None
        self.get_object()

        if self.object.utility and self.object.client:
            profile = self.object.client
        else:
            profile = self.object.utility
        form = UserProfileForm(request.POST, request.FILES, instance=self.object, **{'profile': profile})
        if form.is_valid():
            self.object = form.save(commit=False)
            # try:
            #     user = User.objects.get(username=form.cleaned_data['username'])
            # except DoesNotExist:
            #     user = False
            #
            # if user and self.object and user.id != self.object.user.id:
            #     form_errors.append({"field": 'username',
            #                         "errors": [_u('El usuario "{0}" ya existe en plataforma'.format(user.username))]})
            # else:

            user = self.object.user
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.username = form.cleaned_data['username']
            user.email = form.cleaned_data['email']
            user.is_active = form.cleaned_data['is_active']

            if request.POST.get('password'):
                password = request.POST.get('password')
                user.set_password(password)

            permissions = UserPermissions.get_permissions_of_user(user_profile=self.object)

            permissions = list(permissions)
            if self.object.utility.country != 'CO':
                permissions.append(Permission.objects.get(codename='access_power_optimization'))

            if self.object.utility.name == 'Solelec':
                permissions.append(Permission.objects.get(codename='not_access_analysis'))

            user.user_permissions = []
            for permission in permissions:
                user.user_permissions.append(permission)

            user.save()

            if len(form.files) > 0:
                self.object.image = form.files['image']
            self.object.save()

            if self.object.client and int(self.object.role) == UserProfile.ASSESSOR and not self.object.is_demo:
                try:
                    from core.tasks import task_update_client_assessors
                    task_update_client_assessors.delay(str(self.object.id))
                except Exception as e:
                    print('[ERROR] Asociacion de usuario con rol asesor (id: {0}) con su '
                          'cliente. {1}'.format(self.object.id, e))

            send_pass = request.POST.get('send_pass')
            if send_pass:
                from authz.views import createToken, TokenUserMailSender
                token = createToken(user)
                service_password = TokenUserMailSender()
                service_password.send_mail(user.first_name, user.username,
                                           [user.email], token, get_language())
                print('Email enviado al usuario "{0}"'.format(user.username))

            success = True
            message = _('El usuario "{0}" ha sido editado satisfactoriamente'.format(user.username))

        for field in form.errors:
            form_errors.append({"field": field, "errors": form.errors[field]})

        if len(form_errors):
            message = _('El formulario contiene errores de validación, por favor compruebe')

        return JsonResponse({'success': success, 'errors': form_errors, 'message': message})

    def get_success_url(self):
        return reverse_lazy('utility_users', kwargs={'utility_id': self.kwargs['utility_id']})
        # return reverse_lazy('utility_edit', kwargs={'utility_id': self.kwargs['utility_id']})


class UserDeleteView(LoginRequiredMixin, UserRequiredAccessMixin, DeleteView):
    model = User
    template_name = ''
    success_url = ''
    object = None

    def get_object(self, queryset=None):
        try:
            self.object = MongoUser.objects.get(id=self.kwargs['pk'])
        except (User.DoesNotExist, DoesNotExist):
            raise Http404(_('The user does not exist'))
        return self.object

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        user_profile = UserProfile.objects.get(user=self.object.id)
        user_profile_id = user_profile.id

        utility = user_profile.utility
        client = user_profile.client
        profile_id = utility.id
        if isinstance(utility, Utility) and isinstance(client, Client):
            profile_id = client.id

        DatabaseCleanup.delete_user_by_profile(user_profile_id)

        message = _('El usuario "{0}" ha sido eliminado satisfactoriamente'.format(self.object.username))
        messages.success(request=request, message=message)

        return redirect(reverse('core.portfolio.profile:users', kwargs={'profile_id': profile_id}))
