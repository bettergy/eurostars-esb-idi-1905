import dateparser
import math

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
from django.urls import reverse
from django.utils import formats
from django.utils.translation import gettext as _
from django.views.generic import TemplateView
from django.shortcuts import render

from core.locale import LANGUAGE_NAME_SESSION_KEY
from authz.models import UserProfile
from authz.utils import user_has_perm
from core.analysis.pv.cashflow import PvCashflowParams, PvFinancialModelSolver
from core.analysis.pv.models import *
from core.analysis.pv.utils import format_data_series
from core.base_datatable_view import BaseDatatableView
from core.buildings.models import Building
from core.common.mixins import LoadBuildingsOptionsMixin


def _format_number(number, nb_digits=1):
    number = round(number, nb_digits)
    return formats.localize(number, use_l10n=True)


class QueryParamsMixin(object):

    def get_param(self, name, default=None):
        try:
            val = self.request.POST[name]
            return val
        except:
            try:
                val = self.request.GET[name]
                return val
            except:
                if not default:
                    raise
        return default

    def get_date_param(self, param):
        val = self.get_param(param)
        date_1 = dateparser.parse(val, settings={'DATE_ORDER': 'DMY',
                                                 # 'DATE_ORDER': 'DMY',
                                                 'PREFER_LANGUAGE_DATE_ORDER': True,
                                                 # 'TO_TIMEZONE': 'UTC'
                                                 })
        # tz = _localize(self.get_meter())
        # date_1 = date_1.astimezone(tz)
        return date_1

    def get_date_range(self, s_name='s', e_name='e'):
        start = self.get_date_param(s_name)
        end = self.get_date_param(e_name)
        return start, end + timedelta(minutes=(23 * 60 + 59))

    def get_percentage_param(self, name, default=0.3):
        val = float(self.get_param(name, default))
        if val < 0 or val > 100:
            raise ValueError('Invalid value {0} for ratio query parameter {1}'.format(name, val))
        return val / 100

    def get_meter(self):
        pv_meter_id = self.get_param('m')
        return get_pv_meter(pv_meter_id)

    def _get_timezone(self):
        meter = self.get_meter()
        local_tz = meter.building.get_local_tz()
        return pytz.timezone(local_tz)

    def get_ppa_contract(self):
        ppa_contract = None
        ppa_contract_id = self.get_param('ppa_contract_id', None)
        if ppa_contract_id:
            ppa_contract = PPAContract.objects.get(id=ppa_contract_id)

        # building_id = self.kwargs.get("building_id", None)
        # if building_id:
        #     ppa_contract = get_ppa_by_building(building_id)
        # else:
        #     pv_meter_id = self.get_param('m', None)
        #     if pv_meter_id:
        #         pv_meter = get_pv_meter(pv_meter_id)
        #         ppa_contract = get_ppa_contract(pv_meter)
        return LocalizedContract(ppa_contract)

    def _get_date_now(self):
        # tz = self._get_timezone()
        now = datetime.utcnow()
        today = datetime(now.year, now.month, now.day)
        return today  # today.astimezone(tz)

    def _get_dates_range(self, years=1):
        end = self._get_date_now()
        start = end - relativedelta(years=years)
        return start, end

    def get_relative_dates(self, **kwargs):
        end = self._get_date_now() + relativedelta(day=1)
        start = end + relativedelta(**kwargs)
        return start, end - relativedelta(seconds=1)


class PVPermissionRequiredMixin(PermissionRequiredMixin, QueryParamsMixin):
    building_id = None
    permission_required = 'access_pv_analysis'

    def get_user_profile(self):
        user = self.request.user
        profile = UserProfile.objects.get(user=user)
        return user, profile

    def get_ppa_by_access(self):
        building_id = self.kwargs.get("building_id")
        objs = PPAContract.objects(building=building_id) \
            if building_id else PPAContract.objects
        user, profile = self.get_user_profile()

        if user.is_staff:
            ppa = objs.first()
            return LocalizedContract(ppa) if ppa else None
        if profile.is_investor:
            for x in objs.filter(investor=profile):
                return LocalizedContract(x)
        elif profile.client:
            buildings = Building.objects(owner=profile.client).only('id')
            for x in objs.filter(building__in=buildings):
                return LocalizedContract(x)
        elif profile.is_management_or_is_admin:
            for x in objs.filter(retailer=profile.utility):
                return LocalizedContract(x)
        return None

    def has_permission(self):
        user, profile = self.get_user_profile()
        perm = user_has_perm(user, self.permission_required)
        if perm:
            ppa = self.get_ppa_by_access()
            if ppa:
                if user.is_staff:
                    return True
                if profile.is_investor:
                    return ppa.investor == profile
                if profile.client:
                    return ppa.building.owner == profile.client
                if profile.is_management_or_is_admin:
                    return ppa.retailer == profile.utility
        return perm


class PVIndexView(PVPermissionRequiredMixin, TemplateView):
    template_name = 'pv/index.html'

    def get_views_access(self):
        access = dict()
        # if ppa:
        user, profile = self.get_user_profile()
        is_utility_manager = user.is_staff or (not profile.client \
                                               # and profile.utility == ppa.retailer \
                                               and profile.is_management_or_is_admin)
        access['client_view'] = int(not profile.is_investor or is_utility_manager)
        access['investor_view'] = int(profile.is_investor or is_utility_manager)
        return access

    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)

            # ppa = self.get_ppa_by_access()
            # context['ppa'] = ppa
            # context['buildings'] = [ppa.building]
            building = self.request.GET.get("building", None)
            data_dict = {}
            if building:
                context['building_id'] = building

            context['access'] = self.get_views_access()
            return context
        except Exception as e:
            raise e

    def render_to_response(self, context, **response_kwargs):
        return super(PVIndexView, self).render_to_response(context, **response_kwargs)
        # if context['ppa']:
        #     return super(PVIndexView, self).render_to_response(context, **response_kwargs)
        # else:
        #     # no ppa found, return an empty page
        #     response_kwargs.setdefault('content_type', self.content_type)
        #     return self.response_class(
        #         request=self.request,
        #         template=['pv/no_ppa_found.html'],
        #         context=context,
        #         using=self.template_engine,
        #         **response_kwargs
        #     )


class PVGenerationSummaryView(PVIndexView):
    template_name = ''

    def post(self, request, *args, **kwargs):
        pv_meter = self.get_meter()
        data = get_generation_summary(pv_meter)
        output = {i: {
            'year': entry['_id'],
            'dailyAvg': entry['dailyAvg'],  # energy_with_units(entry['dailyAvg']),
            'yearlySum': entry['yearlySum']  # energy_with_units(entry['yearlySum'])

        } for i, entry in enumerate(data)}

        return JsonResponse(output, safe=False)


class PVCashFlowView(PVIndexView):
    MIN_PROJECTION_LENGTH = 15  # in years

    template_name = ''

    def _cashflow_params(self):
        ppa = self.get_ppa_contract()
        pv_meter = ppa.supply_point

        df = get_client_payment(pv_meter, ppa, ppa.date_from, None)
        df = df.filter(['ppa_cost', 'feeding_cost'])
        df = df.groupby(df.index.year).sum()
        return PvCashflowParams(df, ppa)

    # TODO 1 @staticmethod or promote to querypaarmeter mixin
    def _format_result(self, value):
        return value if not math.isnan(value) else "NaN"

    def post(self, request, *args, **kwargs):
        params = self._cashflow_params()
        result = PvFinancialModelSolver(None, params.projection_yrs).solve(params)
        df = result.df_financial_model
        output = {
            "start": params.pt_start,
            "model": {i: df[col].tolist() for i, col in enumerate(df.filter(df.columns[0:5]))},
            "cashflow": df["CashFlowWithoutUpdateConcept"].tolist(),
            "vanflow": df["VanNetUpdated"].tolist(),
            "irr": self._format_result(result.get_irr()),
            "van": self._format_result(result.get_van()),
            "pp": result.get_payback(),
            "yrs": params.elapsed_yrs
        }
        return JsonResponse(output, safe=False)


class PVMonthlyIncomeView(PVIndexView):  # TODO 1 move to charts?

    template_name = ''

    def _get_data(self):
        ppa = self.get_ppa_contract()

        df = get_feeding_data(ppa.supply_point, ppa.date_from)
        df_pmd = get_feeding_price(df.index[0], df.index[-1])
        df = pd.merge(df, df_pmd, right_index=True, left_index=True)
        df.columns = ['Production', 'Replacement', 'Feeding', 'Price']
        df.eval('Income = Replacement*{0} + Feeding*Price'.format(ppa.energy_price / 1000), inplace=True)
        df = df.drop(columns=df.columns[0:4])  # TODO 1 return all and enhance tooltip
        return df

    def post(self, request, *args, **kwargs):
        df = self._get_data()
        if int(self.get_param('type')) == 1:
            df = df.resample("M").sum()
        output = format_data_series(df['Income'])  # , point_start=start
        return JsonResponse(output, safe=False)


class PVClientSummaryView(PVIndexView):
    template_name = 'pv/partials/tpl_ppa_summary.html'

    def post(self, request, *args, **kwargs):
        ppa = self.get_ppa_contract()
        # from core.tariff_system.es.models import PPAContract
        # ppa_contract_id = request.POST.get('ppa_contract_id')
        # ppa = PPAContract.objects.get(id=ppa_contract_id)
        pv_meter = ppa.supply_point

        start, end = self.get_relative_dates(months=-3)
        start = max(ppa.date_from, start) if start else ppa.date_from
        try:
            df = get_client_payment(pv_meter, ppa, start, end)
            df = df.filter(['ppa_cost', 'saved_cost', 'feeding_cost'])
            df = df.groupby(df.index.month).sum()
        except:
            df = pd.DataFrame(columns=['ppa_cost', 'saved_cost', 'feeding_cost'])
        return JsonResponse(df.to_dict(), safe=True)


class PVAlertsListJsonView(PVPermissionRequiredMixin, BaseDatatableView):
    model = Alert
    template_name = None
    columns = ['id', 'level', 'date_created', 'description', 'building_name', 'details']  # , 'trust'
    order_columns = ['', 'level', 'date_created', 'description', 'building_name', '']  # , 'trust'

    def __init__(self):
        super(PVAlertsListJsonView, self).__init__()
        self.level_label = {Alert.LOW: 'info', Alert.NORMAL: 'warning', Alert.HIGH: 'danger', Alert.CRITICAL: 'danger'}
        self.level_text = dict(Alert.LEVEL_CHOICES)

    def get_initial_queryset(self):
        meter_id = self.kwargs.get("meter_id", None)
        alerts = Alert.objects.none()
        if meter_id:
            alerts = Alert.objects(src_meter=meter_id)
        return alerts

    def render_column(self, row, column):
        result = None
        date_created_format = str(_('%d-%m-%Y a las %H:%M'))
        date_created_formated = datetime.strftime(row.date_created,
                                                  date_created_format) if row.date_created else ''
        if column == 'id':
            result = str(row.pk)
        elif column == 'trust':
            result = str(row.pk)
        elif column == 'level':
            result = {'sort': row.level,
                      'display': '<span class="label label-{0}">{1}</span>'.format(self.level_label.get(row.level),
                                                                                   self.level_text.get(row.level))}
        elif column == 'date_created':
            result = {'sort': row.date_created.timestamp() if row.date_created else -1,
                      'display': date_created_formated}
        elif column == 'description':
            desc = row.get_translation('description', language=self.request.session[LANGUAGE_NAME_SESSION_KEY])
            result = {'sort': str.lower(desc),
                      'display': '<strong>{0}</strong>'.format(desc)}
        elif column == 'building_name':
            building_name = row.building_name if row.building_name else row.building.name if row.building else -1
            result = {'sort': str.lower(building_name),
                      'display': '<a href="{0}">{1}</a>'.format(reverse('building_edit',
                                                                        kwargs={'building_id': row.building.pk}),
                                                                building_name)}
        elif column == 'details':
            building_name = row.building_name if row.building_name else row.building.name if row.building else ''
            desc = row.get_translation('description', language=self.request.session[
                LANGUAGE_NAME_SESSION_KEY]) if row.description else '-'
            subject = row.get_translation('subject', language=self.request.session[LANGUAGE_NAME_SESSION_KEY])
            result = {'level': self.level_text.get(row.level),
                      'subject': subject,
                      'building': building_name,
                      'date_created': date_created_formated,
                      'measure': row.src_measure if row.src_measure else '-',
                      'meter': row.src_meter.name if row.src_meter else False,
                      'description': desc}
        return result


class LoadBuildingsOptionsByPVView(LoadBuildingsOptionsMixin, LoginRequiredMixin, TemplateView):
    template_name = None

    def post(self, request, *args, **kwargs):
        from core.utils import get_submeters_pv_by_user
        submeters_pv_qs = get_submeters_pv_by_user(self.request.user)

        buildings_qs = []
        for sub_meter_pv in submeters_pv_qs:
            if (sub_meter_pv.building.id, sub_meter_pv.building.name) not in buildings_qs:
                buildings_qs.append((sub_meter_pv.building.id, sub_meter_pv.building.name));
        # buildings_qs = []
        return self.get_options(buildings_qs=buildings_qs)


class LoadSubmetersPVOptionsView(LoginRequiredMixin, TemplateView):
    template_name = ''

    def post(self, request, *args, **kwargs):
        building = request.POST.get('building')
        from core.utils import get_submeters_pv_by_user
        submeters_pv_qs = get_submeters_pv_by_user(self.request.user, building_id=building)

        meters_qs = submeters_pv_qs.values_list('id', 'name')
        meters = []
        for m in meters_qs:
            meter = list(m)
            meters.append({'value': str(m[0]), 'name': meter[1]})

        return JsonResponse({'options': meters})


class LoadContractsPPAOptionsView(LoginRequiredMixin, TemplateView):
    template_name = ''

    def post(self, request, *args, **kwargs):
        sub_meter_pv = request.POST.get('sub_meter_pv')
        from core.devices.models import SubMeter
        from core.tariff_system.es.common.models import PPAContract
        sub_meter = SubMeter.objects.get(id=sub_meter_pv)
        ppa_contracts = PPAContract.objects(supply_point=sub_meter)

        contracts = []
        for c in ppa_contracts:
            contracts.append({'value': str(c.id), 'name': str(c)})

        return JsonResponse({'options': contracts})


class PVPPAContractDetailsView(QueryParamsMixin, LoginRequiredMixin, TemplateView):
    template_name = ''

    def post(self, request, *args, **kwargs):
        ppa_contract = self.get_ppa_contract()

        context = {}
        context["ppa"] = ppa_contract
        return render(request, 'pv/partials/tpl_selection_details.html', context)
