from datetime import datetime
from django.utils.translation import gettext as _
from mongoengine import DoesNotExist, MultipleObjectsReturned
from core.recommendation.models import Recommendation, RecommendationContext
from core.buildings.models import Building, Client, Utility
from core.devices.models import SupplyPoint
from datasources.esb.api_managers.meter_api_manager.models.public.recommendation_data import RecommendationData, FinancialModelData
from datasources.esb.api_managers.meter_api_manager.register_recommendation.models.public.register_recommendation_request import \
    RegisterRecommendationRequest
from datasources.esb.api_managers.meter_api_manager.register_recommendation.models.public.register_recommendation_response import \
    RegisterRecommendationSuccess, RegisterRecommendationFailure
from datasources.esb.api_managers.meter_api_manager.register_recommendation.register_recommendation_service import RegisterRecommendationService
from eslogging.manager import RecommendationTraceManager
from efficiency.engines.baseload_reduction import BaseloadReductionRecommendationEngine
from efficiency.engines.contract_optimization import ContractOptimizationRecommendationEngine
from efficiency.engines.free_cooling import FreeCoolingRecommendationEngine
from efficiency.engines.hvac_schedule_optimization import HvacScheduleOptimizationRecommendationEngine
from efficiency.engines.load_shifting import LoadShiftingRecommendationEngine
from efficiency.power_optimization.power_optimization import PowerOptimizationRecommendationEngine
from efficiency.engines.power_peak import PowerPeakRecommendationEngine
from efficiency.reactive_compensation.reactive_energy_reduction import ReactiveEnergyRecommendationEngine
from efficiency.photovoltaic.pv_self_consumption import SelfConsumptionRecommendationEngine
from efficiency.engines.temp_setpoint_adjustment import TempSetPointAdjustmentRecommendationEngine
from efficiency.reactive_compensation.reactive_energy_compensation import ReactiveEnergyCompensationEngine
from efficiency.photovoltaic.pv_self_consumption_rd_244_2019 import PvSelfConsumptionRecommendationEngine
from efficiency.engines.retailer_change import RetailerChangeRecommendationEngine
from efficiency.collective_self_consumption.pv_collective_self_consumption import CollectiveSelfConsumptionRecommendationEngine

import logging
logger = logging.getLogger(__name__)

REAL_INVOICE = 0
SIPS_INVOICE = 1
LOAD_CURVE = 2

BASED_ON = {
    REAL_INVOICE: _("Factura real"),
    SIPS_INVOICE: _("Factura SIPS"),
    LOAD_CURVE: _("Curva de carga"),
}


class EfficiencyManager(object):

    RECOMMENDATION_ENGINES = [
        PowerPeakRecommendationEngine(),  # 0
        SelfConsumptionRecommendationEngine(),  # 1
        LoadShiftingRecommendationEngine(),  # 2
        BaseloadReductionRecommendationEngine(),  # 3
        PowerOptimizationRecommendationEngine(),  # 4
        ReactiveEnergyRecommendationEngine(),  # 5
        TempSetPointAdjustmentRecommendationEngine(),  # 6
        FreeCoolingRecommendationEngine(),  # 7
        HvacScheduleOptimizationRecommendationEngine(),  # 8
        ReactiveEnergyCompensationEngine(),  # 9 - Recommendation for Solelec
        PvSelfConsumptionRecommendationEngine(),  # 10
        ContractOptimizationRecommendationEngine(),  # 11
        RetailerChangeRecommendationEngine(),  # 12 - Recommendation NOT automatic
        CollectiveSelfConsumptionRecommendationEngine(),  # 13  - Recommendation NOT automatic
        # ProductionSchedulingRecommendationEngine(),
        # LEDChangeRecommendationEngine(),

    ]

    def __init__(self):
        pass

    def get_engine_by_type_id(self, id):
        for idx, value in enumerate(self.RECOMMENDATION_ENGINES):
            r_type = value.get_type()
            if r_type and str(r_type.id) == id:
                return r_type

    # Return a recommendation engines list from a recommendation ID list
    def get_engines_by_type_ids(self, id_list):
        return [self.get_engine_by_type_id(rec_id) for rec_id in id_list]

    def get_engine_by_index(self, index):
        for idx, value in enumerate(self.RECOMMENDATION_ENGINES):
            if idx == index:
                return value

    def get_engine_index(self, engines):
        engines_ids = []
        for engine in engines:
            for idx, value in enumerate(self.RECOMMENDATION_ENGINES):
                if isinstance(value, engine):
                    engines_ids.append(idx)
                    break
        return engines_ids

    def get_data_measures(self):
        measures = []
        for engine in self.RECOMMENDATION_ENGINES:
            measures = list(set(measures + engine.get_data_measures()))
        return measures

    @staticmethod
    def delete_recommendation_by_building(building, rec_type):
        try:
            ex_rec = Recommendation.objects.filter(building=building, type=rec_type, action=Recommendation.NEW).first()
            print('\nNo exist NEW recommendation for building "%s"' % building.id)
            if ex_rec:
                if ex_rec.context:
                    ex_rec.context.delete()
                ex_rec.delete()
                print('\nSuccessfully DELETE recommendation for building "%s"' % building.id)
        except Exception as e:
            print("ERROR in delete_recommendation" + str(e))

    @staticmethod
    def delete_recommendation_by_supply_point(supply_point, rec_type):
        try:
            ex_rec = Recommendation.objects.filter(supply_point=supply_point, type=rec_type, action=Recommendation.NEW).first()
            print('\nNo exist NEW recommendation for supply_point "%s"' % supply_point.id)
            if ex_rec:
                if ex_rec.context:
                    ex_rec.context.delete()
                ex_rec.delete()
                print('\nSuccessfully DELETE recommendation for supply_point "%s"' % supply_point.id)
        except Exception as e:
            print("ERROR in delete_recommendation" + str(e))

    @staticmethod
    def save_recommendation(rec):
        today = datetime.now()
        ex_rec = None
        ex_context = None

        try:
            try:
                ex_rec = Recommendation.objects.get(building=rec.building, type=rec.type, supply_point=rec.supply_point)
            except DoesNotExist:
                logger.info('Recommendation does not exist!')

            if ex_rec:
                try:
                    ex_context = RecommendationContext.objects.get(recommendation=ex_rec)
                except DoesNotExist:
                    logger.info('Recommendation context does not exist!')
                except MultipleObjectsReturned:
                    logger.info('MultipleObjectsReturned for Recommendation Context!')
                    ex_context = RecommendationContext.objects.filter(recommendation=ex_rec)[0]

                if ex_rec.action in [Recommendation.ACCEPTED, Recommendation.APPLIED]:
                    logger.info(f'\nCannot update the recommendation for building/supply point {ex_rec.building.id} / '
                                f'{ex_rec.supply_point.id} because it is ACCEPTED or APPLIED')
                    return False, True
                ex_rec_energy_saving = ex_rec.energy_saving or 0
                ex_rec_cost_savings = ex_rec.cost_savings or 0
                rec_energy_saving = rec.energy_saving or 0
                rec_cost_savings = rec.cost_savings or 0
                ex_rec.building.energy_saving_potential = (ex_rec.building.energy_saving_potential or 0) - ex_rec_energy_saving + rec_energy_saving
                ex_rec.building.saving_potential = (ex_rec.building.saving_potential or 0) - ex_rec_cost_savings + rec_cost_savings
                rec.id = ex_rec.id

                try:
                    if ex_context:
                        rec.context.id = ex_context.id
                    rec.context.recommendation = ex_rec.id
                    rec.context.save()
                except Exception as ex:
                    logger.info(f'[ERROR] Cannot saving recommendation context! - {ex}')

                ex_rec = rec
                ex_rec.save()
                ex_rec.building.save()
                ex_rec.building.owner.save()

                logger.info(f'Successfully UPDATED recommendation for building {rec.building.id}')
            else:
                rec.building.energy_saving_potential = (rec.building.energy_saving_potential or 0) + rec.energy_saving
                rec.building.saving_potential = rec.cost_savings + (rec.building.saving_potential or 0)
                rec.action = Recommendation.NEW
                rec.id = None
                rec.date_new = today
                rec.date_action = today

                rec.context.save()  # Save the reference document
                rec.save()
                rec.context.recommendation = rec.id  # Update and save the reco id in the context
                rec.context.save()
                rec.building.save()
                rec.building.owner.save()

                logger.info(f'Successfully CREATED recommendation for building {rec.building.id}')
            logger.info(f'Building: {rec.building}')
            logger.info(f'RecommendationType: {rec.type}')
            logger.info(f'date_new: {rec.date_new}')
            logger.info(f'Cost savings: {rec.cost_savings}')
            logger.info(f'Energy saving: {rec.energy_saving}')
            return True, False
        except MultipleObjectsReturned:
            raise ValueError("Found a building CUPS with more than one recommendation of the same type!")
        except Exception as ex:
            logger.error(f'EfficiencyManager.save_recommendation - {ex}')

    @staticmethod
    def compute_savings_for_utility(utility_id, rec_types=None, based_on=2, begin=None, end=None):
        try:
            utility = Utility.objects.get(id=utility_id)
        except Exception as ex:
            utility = None
            logger.error(f'compute_savings_for_utility - utility {utility_id} does not exist! - {ex}')

        if utility:
            buildings = Building.objects.filter(utility=utility).values_list('id').order_by('name')
            b_ids = []
            [b_ids.append(str(b)) for b in buildings]
            for b_id in b_ids:
                try:
                    b = Building.objects.get(id=b_id)
                    EfficiencyManager.compute_saving_for_building(b, rec_types, based_on=based_on, begin=begin, end=end)
                except Exception as e:
                    logger.error(f'compute_savings_for_utility - error for building {b_id} - {e}')

    @staticmethod
    def compute_saving_for_client(client_id, rec_types=None, based_on=2, begin=None, end=None, order=False,
                                  keep_dates=False, session_key=None):
        try:
            client = Client.objects.get(id=client_id)
        except Exception as ex:
            client = None
            logger.error(f'compute_saving_for_client - client {client_id} does not exist! - {ex}')

        if client:
            buildings = Building.objects.filter(owner=client).values_list('id').order_by('name')
            b_ids = []
            [b_ids.append(str(b)) for b in buildings]
            for b_id in b_ids:
                try:
                    b = Building.objects.get(id=b_id)
                    EfficiencyManager.compute_saving_for_building(b, rec_types, based_on=based_on, begin=begin, end=end, order=order, keep_dates=keep_dates, session_key=session_key)
                except Exception as e:
                    logger.error(f'compute_saving_for_client - error for building {b_id} - {e}')

    @staticmethod
    def compute_saving_for_building(building, rec_types=None, based_on=2, actions=[0, 6, 7, 98], tariffs=None, begin=None, end=None, order=False, keep_dates=False, session_key=None):
        from core.utils import update_statistics_building_and_client
        try:
            logger.info('Evaluate Building - "%s"' % building)
            for sp in SupplyPoint.objects.filter(building=building):
                if tariffs:
                    contract = sp.get_current_contract()
                    if contract and contract.tariff.name not in tariffs:
                        continue
                EfficiencyManager.compute_saving_for_supply_point(sp, rec_types, based_on, calc_statistics=False,
                                                                  actions=actions, begin=begin, end=end, order=order,
                                                                  keep_dates=keep_dates, session_key=None)
        except Exception as ex:
            logger.error('\nError occurred for building "%s". Error: "%s"' % (building.id, ex))
        update_statistics_building_and_client(building)

    @staticmethod
    def compute_saving_for_supply_point(supply_point, rec_types=None, based_on=2, calc_statistics=True,
                                        actions=[0, 6, 7, 98], begin=None, end=None, order=False, keep_dates=False, session_key=None):
        from core.utils import update_statistics_building_and_client

        for rec_type in rec_types:
            try:

                based_on = EfficiencyManager.get_best_based_on_option(supply_point, based_on)

                try:
                    engine = EfficiencyManager.RECOMMENDATION_ENGINES[rec_type]
                    engine.based_on = based_on
                except KeyError:
                    raise Exception('Recommendation index "%s" is not correct' % rec_type)
                logger.info('Evaluate supply point - "%s"' % supply_point)

                initial_rec = EfficiencyManager.init_recommendation(supply_point, engine, actions)
                if initial_rec:
                    rec = engine.evaluate_supply_point(supply_point, initial_rec=initial_rec, begin=begin, end=end, order=order, keep_dates=keep_dates)
                    if not rec:
                        logger.info('No recommendation result for supply point "%s"' % supply_point)
                        EfficiencyManager.delete_recommendation_by_supply_point(supply_point, engine.get_type())
                    else:
                        if session_key:
                            RecommendationTraceManager().register_from_session(session_key, rec)
                        EfficiencyManager.save_recommendation(rec)

                        EfficiencyManager.register_recommendation_in_blockchain(supply_point, rec)
            except Exception as ex:
                logger.info('Error occurred for supply_point "%s". Error: "%s"' % (supply_point.id, ex))

        if calc_statistics:
            update_statistics_building_and_client(supply_point.building, only_save=False)

    @staticmethod
    def register_recommendation_in_blockchain(meter: SupplyPoint, new_recommendation: Recommendation):
        if not meter.esb_meter_id or not new_recommendation.type == EfficiencyManager.RECOMMENDATION_ENGINES[10].get_type():
            return

        EfficiencyManager.register_recommendation(meter, new_recommendation)

    @staticmethod
    def register_recommendation(meter, new_recommendation):
        financial_data = FinancialModelData.fill_data(new_recommendation.financial_model)
        recommendation_data = RecommendationData.fill_data(new_recommendation, financial_data)

        request = RegisterRecommendationRequest(meter.esb_meter_id, recommendation_data)
        response = RegisterRecommendationService().register_recommendation(request)

        if isinstance(response, RegisterRecommendationSuccess):
            logger.info(f'Recommendation saving for meter {meter.name} finished succesfully')
        elif isinstance(response, RegisterRecommendationFailure):
            print(
                f'########## Error during Recommendation saving for meter {meter.name}. Status code: {response.status_code}'
                f', Reason: {response.status_str}, Additional info: {response.additional_info} #########')

    @staticmethod
    def exist_recommendation(supply_point, reco_type, action=None):
        action = action or [Recommendation.NEW, Recommendation.ACCEPTED]
        try:
            ex_rec = Recommendation.objects.get(supply_point=supply_point, type=reco_type, action__in=action)
        except DoesNotExist as ex:
            logger.info('Recommendation does not exist!')
            ex_rec = None
        return ex_rec

    @staticmethod
    def init_recommendation(supply_point, engine, actions=[0, 6, 7, 98]):
        logger.info(f'Initialize recommendation for supply point {supply_point}')

        try:
            rec = Recommendation.objects.get(supply_point=supply_point, type=engine.get_type())
            act = Recommendation.RECALCULATED if rec.action == Recommendation.NEW and rec.recalculated else rec.action
            if act in actions:
                rec.action = Recommendation.CALCULATING
                rec.save()
                logger.info(f'Supply point {supply_point} has existing recommendation!')
            else:
                rec = None
        except DoesNotExist as ex:
            logger.info(f'Recommendation for supply point {supply_point} does not exist yet! '
                        f'Create NEW initial recommendation in CALCULATING status.')
            rec = engine.create_initial_recommendation(supply_point)
            rec.save()
        return rec

    @staticmethod
    def init_recommendation_for_supply_point(supply_point, rec_types=[]):
        recos = []
        logger.info(f'Initialize recommendation for supply point {supply_point}')
        for rec_type in rec_types:
            try:
                try:
                    engine = EfficiencyManager.RECOMMENDATION_ENGINES[rec_type]
                except KeyError:
                    raise Exception('Recommendation index "%s" is not correct' % rec_type)
                recos.append(EfficiencyManager.init_recommendation(supply_point, engine))
            except Exception as ex:
                logger.info(f'[ERROR] Created initial recommendation for supply point {supply_point} '
                            f'for recommendation type {rec_type} - {ex}')
        logger.info(f'Created initial recommendation for supply point {supply_point}')
        return recos

    @staticmethod
    def get_best_based_on_option(supply_point, based_on):
        from core.utils import get_datalogger_days_by_meter, get_invoices_months_by_meter
        from datetime import datetime
        from dateutil.relativedelta import relativedelta

        if based_on not in [99, 98]:
            return based_on

        try:
            if based_on == 99:
                date_to = datetime.now()
                date_from = date_to - relativedelta(years=2)

                datalog_days = get_datalogger_days_by_meter(supply_point, date_from, date_to)
                invoices_sips_months, invoices_real_months = get_invoices_months_by_meter(supply_point, date_from, date_to)

                if datalog_days >= 3:
                    based_on = 2
                elif invoices_sips_months > 0 and invoices_sips_months >= invoices_real_months:
                    based_on = 1
                elif invoices_real_months >= invoices_sips_months and invoices_real_months > 0:
                    based_on = 0
                else:
                    based_on = 2
            elif based_on == 98:
                contract = supply_point.get_current_contract()
                if contract:
                    num_periods = contract.tariff.num_periods_power
                    based_on = 1 if num_periods == 3 else 2
                return based_on
            else:
                return based_on
        except Exception as ex:
            based_on = 2  # Load Curve
        return based_on
