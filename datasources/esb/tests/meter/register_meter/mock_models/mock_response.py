from unittest import mock
import requests
from datasources.esb.helper.esb_status_code import ESBStatusCode


class IMockResponse(requests.Response):

    def update_zip_region_city(self, zip: int, region: str, city: str):
        pass


class MockResponse(IMockResponse):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{"meter_id": "e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973fa4b6", ' \
                             '"registrar_id": "fd58653bf5bc4e68bd621183796253e2",' \
                             '"provider_id": "8202c8b1194ea9998a7be345dead13e5ae2440b15c2f12c1668bacabd8a64ca8",' \
                             '"zip": 50667,' \
                             '"region": "NRW",' \
                             '"city": "Cologne",' \
                             '"enabled": true} '
        type(self).text = mock.PropertyMock(return_value=self._content)

    def update_zip_region_city(self, zip: int, region: str, city: str):
        self._content: str = '{"meter_id": "e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973fa4b6",  ' \
                        '"registrar_id": "fd58653bf5bc4e68bd621183796253e2",' \
                        '"provider_id": "8202c8b1194ea9998a7be345dead13e5ae2440b15c2f12c1668bacabd8a64ca8",' \
                        '"zip": ' + str(zip) + ',' \
                        '"region": "' + region + '",' \
                        '"city": "' + city + '",' \
                        '"enabled": true} '


class MockBadRequestResponse(IMockResponse):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.BadRequest.name
        self.status_code: int = ESBStatusCode.BadRequest.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithEmptyTextProperty(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithEmptyMeterId(IMockResponse):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{"meter_id": "",  ' \
                             '"registrar_id": "fd58653bf5bc4e68bd621183796253e2",' \
                             '"provider_id": "8202c8b1194ea9998a7be345dead13e5ae2440b15c2f12c1668bacabd8a64ca8",' \
                             '"zip": 50667,' \
                             '"region": "NRW",' \
                             '"city": "Cologne",' \
                             '"enabled": true} '
        type(self).text = mock.PropertyMock(return_value=self._content)

    def update_zip_region_city(self, zip: int, region: str, city: str):
        self._content: str = '{"meter_id": "",  ' \
                        '"registrar_id": "fd58653bf5bc4e68bd621183796253e2",' \
                        '"provider_id": "8202c8b1194ea9998a7be345dead13e5ae2440b15c2f12c1668bacabd8a64ca8",' \
                        '"zip": ' + str(zip) + ',' \
                        '"region": "' + region + '",' \
                        '"city": "' + city + '",' \
                        '"enabled": true} '


class MockResponseWithoutMeterId(IMockResponse):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{' \
                             '"registrar_id": "fd58653bf5bc4e68bd621183796253e2",' \
                             '"provider_id": "8202c8b1194ea9998a7be345dead13e5ae2440b15c2f12c1668bacabd8a64ca8",' \
                             '"zip": 50667,' \
                             '"region": "NRW",' \
                             '"city": "Cologne",' \
                             '"enabled": true} '
        type(self).text = mock.PropertyMock(return_value=self._content)

    def update_zip_region_city(self, zip: int, region: str, city: str):
        self._content: str = '{' \
                        '"registrar_id": "fd58653bf5bc4e68bd621183796253e2",' \
                        '"provider_id": "8202c8b1194ea9998a7be345dead13e5ae2440b15c2f12c1668bacabd8a64ca8",' \
                        '"zip": ' + str(zip) + ',' \
                        '"region": "' + region + '",' \
                        '"city": "' + city + '",' \
                        '"enabled": true} '


class MockResponseWithForbiddenStatus(IMockResponse):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Forbidden.name
        self.status_code: int = ESBStatusCode.Forbidden.value
        self._content: str = '{}'
        type(self).text = mock.PropertyMock(return_value=self._content)
