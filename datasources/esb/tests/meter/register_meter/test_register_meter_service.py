import json
from unittest import TestCase
from unittest.mock import patch
from nose_parameterized import parameterized
from datasources.esb.helper.esb_status_code import ESBStatusCode
from datasources.esb.api_managers.meter_api_manager.models.public.registered_meter_data import RegisteredMeterData
from datasources.esb.api_managers.meter_api_manager.register_meter.models.public.register_meter_request import RegisterMeterRequest
from datasources.esb.api_managers.meter_api_manager.register_meter.models.public.register_meter_response import RegisterMeterSuccess
from datasources.esb.api_managers.meter_api_manager.register_meter.register_meter_data_service import RegisterMeterDataService
from datasources.esb.api_managers.meter_api_manager.register_meter.register_meter_service import RegisterMeterService
from datasources.esb.models.public.EsbUserCredentials import EsbUserCredentials
from datasources.esb.tests.meter.register_meter.mock_models.mock_response import MockResponse, \
    MockResponseWithEmptyTextProperty, MockBadRequestResponse, MockResponseWithEmptyMeterId,\
    MockResponseWithoutMeterId, MockResponseWithForbiddenStatus


class TestRegisterMeterService(TestCase):

    @parameterized.expand([
        ("MockResponse_201", MockResponse(), 123, 'test_region', 'test_city',
         EsbUserCredentials('user_key', 'password')),
        ("MockBadRequestResponse_400", MockBadRequestResponse(), None, None, None, None),
        ("MockResponseWithForbiddenStatus_403", MockResponseWithForbiddenStatus(), None,
         'region', 'city', EsbUserCredentials('user_key', 'password')),
    ])
    @patch.object(RegisterMeterDataService, 'register_meter')
    def test_should_return_expected_response(self, name, mock_response, zip,
                                             region, city, esb_user_credentials, mock_register_meter):
        # Arrange
        mock_response.update_zip_region_city(zip, region, city)
        mock_register_meter.return_value = mock_response
        register_meter_service = RegisterMeterService()
        request = RegisterMeterRequest(zip=zip, region=region, city=city, esb_user_credentials=esb_user_credentials)

        # Act
        response = register_meter_service.register_meter(request)

        # Assert
        self.assertEqual(response.status_code, mock_response.status_code)
        self.assertEqual(response.status_str, mock_response.reason)
        if isinstance(response, RegisterMeterSuccess):
            registered_meter_data = self.__extract_registered_meter_data(mock_response)
            self.assertEqual(response.registered_meter_data.zip, registered_meter_data.zip)


    @parameterized.expand([
        ("MockResponseWithEmptyTextProperty_500", MockResponseWithEmptyTextProperty(),
         None, 'region', 'city', EsbUserCredentials('user_key', 'password'),
         ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
        ("MockResponseWithEmptyMeterId_500", MockResponseWithEmptyMeterId(),
         None, 'region', 'city', EsbUserCredentials('user_key', 'password'),
         ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
        ("MockResponseWithoutMeterId_500", MockResponseWithoutMeterId(),
         None, 'region', 'city', EsbUserCredentials('user_key', 'password'),
         ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
    ])
    @patch.object(RegisterMeterDataService, 'register_meter')
    def test_should_return_status_500_if_response_does_not_have_some_data_of_created_meter(self, name,
                                                                                           mock_response,
                                                                                           zip, region, city,
                                                                                           esb_user_credentials,
                                                                                           expected_status_code,
                                                                                           expected_status_str,
                                                                                           mock_register_meter):
        # Arrange
        mock_response.update_zip_region_city(zip, region, city)
        mock_register_meter.return_value = mock_response
        register_meter_service = RegisterMeterService()
        request = RegisterMeterRequest(zip=zip, region=region, city=city, esb_user_credentials=esb_user_credentials)

        # Act
        response = register_meter_service.register_meter(request)

        # Assert
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.status_str, expected_status_str)





    @staticmethod
    def __extract_registered_meter_data(response):
        meter_data = json.loads(response.text)
        esb_meter_id = meter_data.get('meter_id', None)
        esb_register_id = meter_data.get('registrar_id', None)
        esb_provider_id = meter_data.get('provider_id', None)
        zip = meter_data.get('zip', None)
        region = meter_data.get('region', None)
        city = meter_data.get('city', None)
        enabled = meter_data.get('enabled', None)
        registered_meter_data = RegisteredMeterData(id=esb_meter_id, registrar_id=esb_register_id,
                                                    provider_id=esb_provider_id, zip=zip, region=region,
                                                    city=city, enabled=enabled)
        return registered_meter_data
