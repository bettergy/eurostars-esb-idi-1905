import requests
from unittest import mock
from datasources.esb.helper.esb_status_code import ESBStatusCode


class IMockResponse(requests.Response):

    def update_meter_id(self, meter_id: str):
        """Update of parameters"""


class MockResponse(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{  "meter_id": "string",' \
                                 '"timestamp": 0,' \
                                 '"user": "string",' \
                                 '"recommendation": {"energy_saving": 0, ' \
                                        '"cost_savings": 0,' \
                                        '"investment": 0,' \
                                        '"payback": 0,' \
                                        '"iir_recommended": 0,' \
                                        '"iir_optimum": 0,' \
                                        '"irr_minimum": 0,' \
                                        '"minimum_peak_power": 0,' \
                                        '"maximum_peak_power": 0,' \
                                        '"peak_power_recommended": 0,' \
                                        '"peak_power_optimum": 0,' \
                                        '"financial": {' \
                                            '"num_years": 0,' \
                                            '"irr": 0,' \
                                            '"van": 0,' \
                                            '"payback": 0,' \
                                            '"irr_7": 0,' \
                                            '"van_7": 0,' \
                                            '"payback_7": 0,' \
                                            '"irr_10": 0,' \
                                            '"van_10": 0,' \
                                            '"payback_10": 0,' \
                                            '"om": "string",' \
                                            '"rent-ing": "string",' \
                                            '"insur-ance": "string",' \
                                            '"self_generation": "string",' \
                                            '"fee_licence": "string",' \
                                            '"construction_tax": "string",' \
                                            '"investment_installation": "string",' \
                                            '"equip-ment_replacement": "string",' \
                                            '"subven-tion": "string",' \
                                            '"ener-gy_saving": "string",' \
                                            '"ener-gy_sales": "string",' \
                                            '"pow-er_optimization": "string",' \
                                            '"cashflow": "string",' \
                                            '"cashflow_without_update": "string",' \
                                            '"van_net_updated": "string",' \
                                            '"cost_10_years_financing": "string",' \
                                            '"saving_10_years_financing": "string",' \
                                            '"cost_7_years_financing": "string",' \
                                            '"van_7_years_financing": "string",' \
                                            '"saving_7_years_financing": "string",' \
                                            '"cost_10_years_with_subvention": "string"' \
                                        '}}' \
                                     '}'
        type(self).text = mock.PropertyMock(return_value=self._content)

    def update_meter_id(self, meter_id: str):
        self._content: str = '{ "meter_id": "' + meter_id + '",' \
                                 '"timestamp": 0,' \
                                 '"user": "string",' \
                                 '"recommendation": {"energy_saving": 0, ' \
                                        '"cost_savings": 0,' \
                                        '"investment": 0,' \
                                        '"payback": 0,' \
                                        '"iir_recommended": 0,' \
                                        '"iir_optimum": 0,' \
                                        '"irr_minimum": 0,' \
                                        '"minimum_peak_power": 0,' \
                                        '"maximum_peak_power": 0,' \
                                        '"peak_power_recommended": 0,' \
                                        '"peak_power_optimum": 0,' \
                                        '"financial": {' \
                                            '"num_years": 0,' \
                                            '"irr": 0,' \
                                            '"van": 0,' \
                                            '"payback": 0,' \
                                            '"irr_7": 0,' \
                                            '"van_7": 0,' \
                                            '"payback_7": 0,' \
                                            '"irr_10": 0,' \
                                            '"van_10": 0,' \
                                            '"payback_10": 0,' \
                                            '"om": "string",' \
                                            '"rent-ing": "string",' \
                                            '"insur-ance": "string",' \
                                            '"self_generation": "string",' \
                                            '"fee_licence": "string",' \
                                            '"construction_tax": "string",' \
                                            '"investment_installation": "string",' \
                                            '"equip-ment_replacement": "string",' \
                                            '"subven-tion": "string",' \
                                            '"ener-gy_saving": "string",' \
                                            '"ener-gy_sales": "string",' \
                                            '"pow-er_optimization": "string",' \
                                            '"cashflow": "string",' \
                                            '"cashflow_without_update": "string",' \
                                            '"van_net_updated": "string",' \
                                            '"cost_10_years_financing": "string",' \
                                            '"saving_10_years_financing": "string",' \
                                            '"cost_7_years_financing": "string",' \
                                            '"van_7_years_financing": "string",' \
                                            '"saving_7_years_financing": "string",' \
                                            '"cost_10_years_with_subvention": "string"' \
                                        '}}' \
                                     '}'
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockBadRequestResponse(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.BadRequest.name
        self.status_code: int = ESBStatusCode.BadRequest.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithForbiddenStatus(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Forbidden.name
        self.status_code: int = ESBStatusCode.Forbidden.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithEmptyTextProperty(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithEmptyMeterId(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{  "meter_id": "",' \
                                 '"timestamp": 0,' \
                                 '"user": "string",' \
                                 '"recommendation": {"energy_saving": 0, ' \
                                        '"cost_savings": 0,' \
                                        '"investment": 0,' \
                                        '"payback": 0,' \
                                        '"iir_recommended": 0,' \
                                        '"iir_optimum": 0,' \
                                        '"irr_minimum": 0,' \
                                        '"minimum_peak_power": 0,' \
                                        '"maximum_peak_power": 0,' \
                                        '"peak_power_recommended": 0,' \
                                        '"peak_power_optimum": 0,' \
                                        '"financial": {' \
                                            '"num_years": 0,' \
                                            '"irr": 0,' \
                                            '"van": 0,' \
                                            '"payback": 0,' \
                                            '"irr_7": 0,' \
                                            '"van_7": 0,' \
                                            '"payback_7": 0,' \
                                            '"irr_10": 0,' \
                                            '"van_10": 0,' \
                                            '"payback_10": 0,' \
                                            '"om": "string",' \
                                            '"rent-ing": "string",' \
                                            '"insur-ance": "string",' \
                                            '"self_generation": "string",' \
                                            '"fee_licence": "string",' \
                                            '"construction_tax": "string",' \
                                            '"investment_installation": "string",' \
                                            '"equip-ment_replacement": "string",' \
                                            '"subven-tion": "string",' \
                                            '"ener-gy_saving": "string",' \
                                            '"ener-gy_sales": "string",' \
                                            '"pow-er_optimization": "string",' \
                                            '"cashflow": "string",' \
                                            '"cashflow_without_update": "string",' \
                                            '"van_net_updated": "string",' \
                                            '"cost_10_years_financing": "string",' \
                                            '"saving_10_years_financing": "string",' \
                                            '"cost_7_years_financing": "string",' \
                                            '"van_7_years_financing": "string",' \
                                            '"saving_7_years_financing": "string",' \
                                            '"cost_10_years_with_subvention": "string"' \
                                        '}}' \
                                     '}'
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithoutMeterId(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{ "timestamp": 0,' \
                                 '"user": "string",' \
                                 '"recommendation": {"energy_saving": 0, ' \
                                        '"cost_savings": 0,' \
                                        '"investment": 0,' \
                                        '"payback": 0,' \
                                        '"iir_recommended": 0,' \
                                        '"iir_optimum": 0,' \
                                        '"irr_minimum": 0,' \
                                        '"minimum_peak_power": 0,' \
                                        '"maximum_peak_power": 0,' \
                                        '"peak_power_recommended": 0,' \
                                        '"peak_power_optimum": 0,' \
                                        '"financial": {' \
                                            '"num_years": 0,' \
                                            '"irr": 0,' \
                                            '"van": 0,' \
                                            '"payback": 0,' \
                                            '"irr_7": 0,' \
                                            '"van_7": 0,' \
                                            '"payback_7": 0,' \
                                            '"irr_10": 0,' \
                                            '"van_10": 0,' \
                                            '"payback_10": 0,' \
                                            '"om": "string",' \
                                            '"rent-ing": "string",' \
                                            '"insur-ance": "string",' \
                                            '"self_generation": "string",' \
                                            '"fee_licence": "string",' \
                                            '"construction_tax": "string",' \
                                            '"investment_installation": "string",' \
                                            '"equip-ment_replacement": "string",' \
                                            '"subven-tion": "string",' \
                                            '"ener-gy_saving": "string",' \
                                            '"ener-gy_sales": "string",' \
                                            '"pow-er_optimization": "string",' \
                                            '"cashflow": "string",' \
                                            '"cashflow_without_update": "string",' \
                                            '"van_net_updated": "string",' \
                                            '"cost_10_years_financing": "string",' \
                                            '"saving_10_years_financing": "string",' \
                                            '"cost_7_years_financing": "string",' \
                                            '"van_7_years_financing": "string",' \
                                            '"saving_7_years_financing": "string",' \
                                            '"cost_10_years_with_subvention": "string"' \
                                        '}}' \
                                     '}'
        type(self).text = mock.PropertyMock(return_value=self._content)
