import json
from datasources.esb.api_managers.meter_api_manager.models.public.recommendation_data import FinancialModelData, RecommendationData
from datasources.esb.api_managers.meter_api_manager.register_recommendation.models.public.register_recommendation_request import \
    RegisterRecommendationRequest


class IMockRequest(RegisterRecommendationRequest):
    meter_id = ''
    recommendation: RecommendationData = None
    response_str: str = ''


class MockRequest(IMockRequest):
    meter_id = 'e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973asd2'
    response_str: str = '{  "meter_id": "string",' \
                         '"timestamp": 0,' \
                         '"user": "string",' \
                         '"recommendation": {"energy_saving": 0, ' \
                         '"cost_savings": 0,' \
                         '"investment": 0,' \
                         '"payback": 0,' \
                         '"iir_recommended": 0,' \
                         '"iir_optimum": 0,' \
                         '"irr_minimum": 0,' \
                         '"minimum_peak_power": 0,' \
                         '"maximum_peak_power": 0,' \
                         '"peak_power_recommended": 0,' \
                         '"peak_power_optimum": 0,' \
                         '"financial_model": {' \
                         '"num_years": 0,' \
                         '"irr": 0,' \
                         '"van": 0,' \
                         '"payback": 0,' \
                         '"irr_7": 0,' \
                         '"van_7": 0,' \
                         '"payback_7": 0,' \
                         '"irr_10": 0,' \
                         '"van_10": 0,' \
                         '"payback_10": 0,' \
                         '"om": "string",' \
                         '"rent-ing": "string",' \
                         '"insur-ance": "string",' \
                         '"self_generation": "string",' \
                         '"fee_licence": "string",' \
                         '"construction_tax": "string",' \
                         '"investment_installation": "string",' \
                         '"equip-ment_replacement": "string",' \
                         '"subven-tion": "string",' \
                         '"ener-gy_saving": "string",' \
                         '"ener-gy_sales": "string",' \
                         '"pow-er_optimization": "string",' \
                         '"cashflow": "string",' \
                         '"cashflow_without_update": "string",' \
                         '"van_net_updated": "string",' \
                         '"cost_10_years_financing": "string",' \
                         '"saving_10_years_financing": "string",' \
                         '"cost_7_years_financing": "string",' \
                         '"van_7_years_financing": "string",' \
                         '"saving_7_years_financing": "string",' \
                         '"cost_10_years_with_subvention": "string"' \
                         '}}' \
                         '}'

    def __init__(self):
        self.recommendation = extract_recommendation_data(self.response_str)
        super().__init__(self.meter_id, self.recommendation)


class MockBadRequest(IMockRequest):
    response_str: str = '{  "meter_id": "string",' \
                         '"timestamp": 0,' \
                         '"user": "string",' \
                         '"recommendation": {"energy_saving": 0, ' \
                         '"cost_savings": 0,' \
                         '"investment": 0,' \
                         '"payback": 0,' \
                         '"iir_recommended": 0,' \
                         '"iir_optimum": 0,' \
                         '"irr_minimum": 0,' \
                         '"minimum_peak_power": 0,' \
                         '"maximum_peak_power": 0,' \
                         '"peak_power_recommended": 0,' \
                         '"peak_power_optimum": 0,' \
                         '"financial_model": {' \
                         '"num_years": 0,' \
                         '"irr": 0,' \
                         '"van": 0,' \
                         '"payback": 0,' \
                         '"irr_7": 0,' \
                         '"van_7": 0,' \
                         '"payback_7": 0,' \
                         '"irr_10": 0,' \
                         '"van_10": 0,' \
                         '"payback_10": 0,' \
                         '"om": "string",' \
                         '"rent-ing": "string",' \
                         '"insur-ance": "string",' \
                         '"self_generation": "string",' \
                         '"fee_licence": "string",' \
                         '"construction_tax": "string",' \
                         '"investment_installation": "string",' \
                         '"equip-ment_replacement": "string",' \
                         '"subven-tion": "string",' \
                         '"ener-gy_saving": "string",' \
                         '"ener-gy_sales": "string",' \
                         '"pow-er_optimization": "string",' \
                         '"cashflow": "string",' \
                         '"cashflow_without_update": "string",' \
                         '"van_net_updated": "string",' \
                         '"cost_10_years_financing": "string",' \
                         '"saving_10_years_financing": "string",' \
                         '"cost_7_years_financing": "string",' \
                         '"van_7_years_financing": "string",' \
                         '"saving_7_years_financing": "string",' \
                         '"cost_10_years_with_subvention": "string"' \
                         '}}' \
                         '}'

    def __init__(self):
        self.recommendations = extract_recommendation_data(self.response_str)
        super().__init__(self.meter_id, self.recommendation)


class MockBadRecommendationDataRequest(IMockRequest):
    # saving_7_years_financing and cost_10_years_with_subvention are empty
    meter_id = 'e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973asd2'
    response_str: str = '{  "meter_id": "string",' \
                         '"timestamp": 0,' \
                         '"user": "string",' \
                         '"recommendation": {"energy_saving": 0, ' \
                         '"cost_savings": 0,' \
                         '"investment": 0,' \
                         '"payback": 0,' \
                         '"iir_recommended": 0,' \
                         '"iir_optimum": 0,' \
                         '"irr_minimum": 0,' \
                         '"minimum_peak_power": 0,' \
                         '"maximum_peak_power": 0,' \
                         '"peak_power_recommended": 0,' \
                         '"peak_power_optimum": 0,' \
                         '"financial_model": {' \
                         '"num_years": 0,' \
                         '"irr": 0,' \
                         '"van": 0,' \
                         '"payback": 0,' \
                         '"irr_7": 0,' \
                         '"van_7": 0,' \
                         '"payback_7": 0,' \
                         '"irr_10": 0,' \
                         '"van_10": 0,' \
                         '"payback_10": 0,' \
                         '"om": "string",' \
                         '"rent-ing": "string",' \
                         '"insur-ance": "string",' \
                         '"self_generation": "string",' \
                         '"fee_licence": "string",' \
                         '"construction_tax": "string",' \
                         '"investment_installation": "string",' \
                         '"equip-ment_replacement": "string",' \
                         '"subven-tion": "string",' \
                         '"ener-gy_saving": "string",' \
                         '"ener-gy_sales": "string",' \
                         '"pow-er_optimization": "string",' \
                         '"cashflow": "string",' \
                         '"cashflow_without_update": "string",' \
                         '"van_net_updated": "string",' \
                         '"cost_10_years_financing": "string",' \
                         '"saving_10_years_financing": "string",' \
                         '"cost_7_years_financing": "string",' \
                         '"van_7_years_financing": "string",' \
                         '"saving_7_years_financing": "",' \
                         '"cost_10_years_with_subvention": ""' \
                         '}}' \
                         '}'

    def __init__(self):
        self.recommendation = extract_recommendation_data(self.response_str)
        super().__init__(self.meter_id, self.recommendation)


def extract_recommendation_data(response_str) -> RecommendationData:
    response = json.loads(response_str)
    recommendation = response['recommendation']
    energy_saving = recommendation['energy_saving']
    cost_savings = recommendation['cost_savings']
    investment = recommendation['investment']
    payback = recommendation['payback']
    iir_recommended = recommendation['iir_recommended']
    iir_optimum = recommendation['iir_optimum']
    irr_minimum = recommendation['irr_minimum']
    minimum_peak_power = recommendation['minimum_peak_power']
    maximum_peak_power = recommendation['maximum_peak_power']
    peak_power_recommended = recommendation['peak_power_recommended']
    peak_power_optimum = recommendation['peak_power_optimum']
    financial_model = recommendation['financial_model']
    financial_model = extract_financial_model_data(financial_model)

    return RecommendationData(energy_saving, cost_savings, investment, payback, iir_recommended, iir_optimum,
                              irr_minimum, minimum_peak_power, maximum_peak_power, peak_power_recommended,
                              peak_power_optimum, financial_model)


def extract_financial_model_data(financial_model: {}) -> FinancialModelData:
    num_years = financial_model['num_years']
    irr = financial_model['irr']
    van = financial_model['van']
    payback = financial_model['payback']
    irr_7 = financial_model['irr_7']
    van_7 = financial_model['van_7']
    payback_7 = financial_model['payback_7']
    irr_10 = financial_model['irr_10']
    van_10 = financial_model['van_10']
    payback_10 = financial_model['payback_10']
    om = financial_model['om']
    rent_ing = financial_model['rent-ing']
    insur_ance = financial_model['insur-ance']
    self_generation = financial_model['self_generation']
    fee_licence = financial_model['fee_licence']
    construction_tax = financial_model['construction_tax']
    investment_installation = financial_model['investment_installation']
    equip_ment_replacement = financial_model['equip-ment_replacement']
    subven_tion = financial_model['subven-tion']
    ener_gy_saving = financial_model['ener-gy_saving']
    ener_gy_sales = financial_model['ener-gy_sales']
    pow_er_optimization = financial_model['pow-er_optimization']
    cashflow = financial_model['cashflow']
    cashflow_without_update = financial_model['cashflow_without_update']
    van_net_updated = financial_model['van_net_updated']
    cost_10_years_financing = financial_model['cost_10_years_financing']
    saving_10_years_financing = financial_model['saving_10_years_financing']
    cost_7_years_financing = financial_model['cost_7_years_financing']
    van_7_years_financing = financial_model['van_7_years_financing']
    saving_7_years_financing = financial_model['saving_7_years_financing']
    cost_10_years_with_subvention = financial_model['cost_10_years_with_subvention']

    return FinancialModelData(num_years, irr, van, payback, irr_7, van_7,
                              payback_7, irr_10, van_10, payback_10, om, rent_ing,
                              insur_ance, self_generation, fee_licence, construction_tax,
                              investment_installation, equip_ment_replacement, subven_tion,
                              ener_gy_saving, ener_gy_sales, pow_er_optimization, cashflow,
                              cashflow_without_update, van_net_updated, cost_10_years_financing,
                              saving_10_years_financing, cost_7_years_financing, van_7_years_financing,
                              saving_7_years_financing, cost_10_years_with_subvention)
