from unittest import TestCase
from unittest.mock import patch
from parameterized import parameterized
from datasources.esb.helper.esb_status_code import ESBStatusCode
from datasources.esb.api_managers.meter_api_manager.register_recommendation.models.public.register_recommendation_request import \
    RegisterRecommendationRequest
from datasources.esb.api_managers.meter_api_manager.register_recommendation.models.public.register_recommendation_response import \
    RegisterRecommendationSuccess
from datasources.esb.api_managers.meter_api_manager.register_recommendation.register_recommendation_data_service import \
    RegisterRecommendationDataService
from datasources.esb.api_managers.meter_api_manager.register_recommendation.register_recommendation_service import RegisterRecommendationService
from datasources.esb.tests.meter.register_recommendation.mock_models.mock_request import MockRequest, MockBadRequest,\
    MockBadRecommendationDataRequest, IMockRequest
from datasources.esb.tests.meter.register_recommendation.mock_models.mock_response import MockResponse,\
    MockBadRequestResponse, MockResponseWithForbiddenStatus, MockResponseWithEmptyMeterId, \
    MockResponseWithEmptyTextProperty, MockResponseWithoutMeterId, IMockResponse


class TestRegisterRecommendationService(TestCase):

    @parameterized.expand([
        ("MockResponse_201", MockResponse(), MockRequest()),
        ("MockBadRequestResponse_400", MockBadRequestResponse(), MockBadRequest()),
        ("MockBadRecommendationDataRequest_400", MockBadRequestResponse(), MockBadRecommendationDataRequest()),
        ("MockResponseWithForbiddenStatus_403", MockResponseWithForbiddenStatus(), MockRequest()),
    ])
    @patch.object(RegisterRecommendationDataService, 'register_recommendation')
    def test_should_return_expected_response(self, name, mock_response: IMockResponse,
                                             mock_request: IMockRequest,
                                             mock_register_recommendation):
        # Arrange
        mock_response.update_meter_id(mock_request.meter_id)
        mock_response.update_meter_id(mock_request.meter_id)
        mock_register_recommendation.return_value = mock_response
        register_recommendation_service = RegisterRecommendationService()
        request = RegisterRecommendationRequest(mock_request.meter_id, mock_request.recommendation)

        # Act
        response = register_recommendation_service.register_recommendation(request)

        # Assert
        self.assertEqual(response.status_code, mock_response.status_code)
        self.assertEqual(response.status_str, mock_response.reason)
        if isinstance(response, RegisterRecommendationSuccess):
            self.assertEqual(response.registered_consumption_data.meter_id, mock_request.meter_id)

    @parameterized.expand([
        ("MockResponseWithEmptyTextProperty_500", MockResponseWithEmptyTextProperty(), MockRequest(),
          ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
        ("MockResponseWithEmptyMeterId_500", MockResponseWithEmptyMeterId(), MockRequest(),
          ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
        ("MockResponseWithoutMeterId_500", MockResponseWithoutMeterId(), MockRequest(),
          ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
    ])
    @patch.object(RegisterRecommendationDataService, 'register_recommendation')
    def test_should_return_status_500_if_response_does_not_have_some_data_of_created_recommendation(self,
                                                                                                    name,
                                                                                         mock_response: IMockResponse,
                                                                                         mock_request: MockRequest,
                                                                                         expected_status_code,
                                                                                         expected_status_str,
                                                                                         mock_register_recommendation):
        # Arrange
        mock_response.update_meter_id(mock_request.meter_id)
        mock_response.update_meter_id(mock_request.meter_id)
        mock_register_recommendation.return_value = mock_response
        register_recommendation_service = RegisterRecommendationService()
        request = RegisterRecommendationRequest(mock_request.meter_id, mock_request.recommendation)

        # Act
        response = register_recommendation_service.register_recommendation(request)

        # Assert
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.status_str, expected_status_str)
