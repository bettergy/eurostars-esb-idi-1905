import json
from unittest import TestCase
from unittest.mock import patch
from nose_parameterized import parameterized

from datasources.esb.helper.esb_status_code import ESBStatusCode
from datasources.esb.api_managers.meter_api_manager.get_all_meters.get_all_meters_data_service import GetAllMetersDataService
from datasources.esb.api_managers.meter_api_manager.get_all_meters.get_all_meters_service import GetAllMetersService
from datasources.esb.api_managers.meter_api_manager.get_all_meters.models.public.get_all_meters_response import GetAllMetersSuccess
from datasources.esb.api_managers.meter_api_manager.models.public.registered_meter_data import RegisteredMeterData
from datasources.esb.tests.meter.get_all_meters.mock_models.mock_response import MockResponse, \
    MockResponseWithEmptyMetersArray, MockResponseWithForbiddenStatus, MockResponseWithoutMeterId, \
    MockResponseWithEmptyTextProperty


class TestGetAllMetersService(TestCase):

    @parameterized.expand([
        ("MockResponse_200", MockResponse()),
        ("MockResponseWithEmptyMetersArray_200", MockResponseWithEmptyMetersArray()),
        ("MockResponseWithForbiddenStatus_403", MockResponseWithForbiddenStatus()),
    ])
    @patch.object(GetAllMetersDataService, 'get_all_meters')
    def test_should_return_expected_response(self, name, mock_response, mock_get_all_meters):
        # Arrange
        mock_get_all_meters.return_value = mock_response
        get_all_meters_service = GetAllMetersService()

        # Act
        response = get_all_meters_service.get_all_meters()
        meters = self.get_registered_meter_data_from_str(mock_response)

        # Assert
        self.assertEqual(response.status_code, mock_response.status_code)
        self.assertEqual(response.status_str, mock_response.reason)
        if isinstance(response, GetAllMetersSuccess):
            self.assertEqual(len(response.meters), len(meters))


    @parameterized.expand([
        ("MockResponseWithoutMeterId_500", MockResponseWithoutMeterId(), ESBStatusCode.InternalServerError.value,
         ESBStatusCode.InternalServerError.name),
        ("MockResponseWithEmptyTextProperty_500", MockResponseWithEmptyTextProperty(), ESBStatusCode.InternalServerError.value,
         ESBStatusCode.InternalServerError.name)
    ])
    @patch.object(GetAllMetersDataService, 'get_all_meters')
    def test_should_return_expected_response(self, name, mock_response, expected_status_code,
                                             expected_status_str,mock_get_all_meters):
        # Arrange
        mock_get_all_meters.return_value = mock_response
        get_all_meters_service = GetAllMetersService()

        # Act
        response = get_all_meters_service.get_all_meters()

        # Assert
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.status_str, expected_status_str)

    @staticmethod
    def get_registered_meter_data_from_str(mock_response):
        meters = []
        meters_data = json.loads(mock_response.text)
        for i in meters_data:
            esb_meter_id = i['meter_id']
            esb_register_id = i['registrar_id']
            esb_provider_id = i.get('provider_id', None)
            zip = i.get('zip', None)
            region = i.get('region', None)
            city = i.get('city', None)
            enabled = i.get('enabled', None)

            registered_meter_data = RegisteredMeterData(id=esb_meter_id, registrar_id=esb_register_id,
                                                        provider_id=esb_provider_id, zip=zip, region=region,
                                                        city=city, enabled=enabled)

            meters.append(registered_meter_data)
        return meters
