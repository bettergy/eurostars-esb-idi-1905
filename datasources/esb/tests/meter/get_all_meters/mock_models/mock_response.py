from unittest import mock
import requests
from datasources.esb.helper.esb_status_code import ESBStatusCode


class MockResponse(requests.Response):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Ok.name
        self.status_code: int = ESBStatusCode.Ok.value
        self._content: str = '[{"meter_id":"574acfa365549d3e6fcfc998c40b3d55c2e4e0a11553ff068e6ea37fd8acf5f9",' \
                '"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"None",' \
                '"region":"VitReg","city":"Lviv","enabled":true},' \
                '{"meter_id":"34f16537cd7713658da67d6467018c66694d4e910ac74e2b55bc41a3fa64a4a7",' \
                '"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"36992",' \
                '"region":"VitReg","city":"Lviv","enabled":true},' \
                '{"meter_id":"6f44e089ea457d32a9b1820685c525c2cb036307e200c6ba51385bcdb6c7fd52",' \
                '"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"50667",' \
                '"region":"NRW","city":"Cologne","enabled":true},' \
                '{"meter_id":"35d9c38e9a512fd4177caafbf643fc0267283321498795333f4053757bf52f85",' \
                '"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"50667",' \
                '"region":"NRW","city":"Cologne","enabled":true}] '
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithEmptyMetersArray(requests.Response):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Ok.name
        self.status_code: int = ESBStatusCode.Ok.value
        self._content: str = '[]'
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithForbiddenStatus(requests.Response):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Forbidden.name
        self.status_code: int = ESBStatusCode.Forbidden.value
        self._content: str = '[]'
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithEmptyTextProperty(requests.Response):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Ok.name
        self.status_code: int = ESBStatusCode.Ok.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithoutMeterId(requests.Response):

    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Ok.name
        self.status_code: int = ESBStatusCode.Ok.value
        self._content: str = '[{registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"None",' \
                '"region":"VitReg","city":"Lviv","enabled":true},' \
                '{"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"36992",' \
                '"region":"VitReg","city":"Lviv","enabled":true},' \
                '{"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"50667",' \
                '"region":"NRW","city":"Cologne","enabled":true},' \
                '{"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"50667",' \
                '"region":"NRW","city":"Cologne","enabled":true},' \
                '{"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"123",' \
                '"region":"ES","city":"4","enabled":true},' \
                '{"registrar_id":"afbbb6bc7b93b0be1a8e349939dac94025340ae2a72babd02235c7b859fe86b0","zip":"123",' \
                '"region":"ES","city":"Málaga","enabled":true}] '
        type(self).text = mock.PropertyMock(return_value=self._content)
