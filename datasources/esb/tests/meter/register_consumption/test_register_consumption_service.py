from unittest import TestCase
from unittest.mock import patch
from parameterized import parameterized
from datasources.esb.helper.esb_status_code import ESBStatusCode
from datasources.esb.api_managers.meter_api_manager.register_consumption.models.public.register_consumption_request import \
    RegisterConsumptionRequest
from datasources.esb.api_managers.meter_api_manager.register_consumption.models.public.register_consumption_response import \
    RegisterConsumptionSuccess
from datasources.esb.api_managers.meter_api_manager.register_consumption.register_consumption_data_service import RegisterConsumptionDataService
from datasources.esb.api_managers.meter_api_manager.register_consumption.register_consumption_service import RegisterConsumptionService
from datasources.esb.tests.meter.register_consumption.mock_models.mock_request import MockRequest, MockBadRequest, \
    MockBadConsumptionRequest, IMockRequest
from datasources.esb.tests.meter.register_consumption.mock_models.mock_response import MockResponse, IMockResponse,\
    MockBadRequestResponse, MockResponseWithForbiddenStatus, MockResponseWithEmptyPowerConsumptionBlockId,\
    MockResponseWithoutPowerConsumptionBlockId, MockResponseWithEmptyTextProperty


class TestRegisterConsumptionService(TestCase):

    @parameterized.expand([
        ("MockResponse_201", MockResponse(), MockRequest()),
        ("MockBadRequestResponse_400", MockBadRequestResponse(), MockBadRequest()),
        ("MockBadConsumptionRequest_400", MockBadRequestResponse(), MockBadConsumptionRequest()),
        ("MockResponseWithForbiddenStatus_403", MockResponseWithForbiddenStatus(), MockRequest()),
    ])
    @patch.object(RegisterConsumptionDataService, 'register_consumption')
    def test_should_return_expected_response(self, name,
                                             mock_response: IMockResponse,
                                             mock_request: IMockRequest,
                                             mock_register_consumption):
        # Arrange
        mock_response.update_meter_id_and_power_consumption(mock_request.meter_id, mock_request.consumption_str)
        mock_register_consumption.return_value = mock_response
        register_consumption_service = RegisterConsumptionService()
        request = RegisterConsumptionRequest(mock_request.consumptions, mock_request.meter_id, mock_request.meter_contract_id)

        # Act
        response = register_consumption_service.register_consumption(request)

        # Assert
        self.assertEqual(response.status_code, mock_response.status_code)
        self.assertEqual(response.status_str, mock_response.reason)
        if isinstance(response, RegisterConsumptionSuccess):
            self.assertEqual(len(response.registered_consumption_data.power_consumption), len(mock_request.consumptions))

    @parameterized.expand([
        ("MockResponseWithEmptyTextProperty_500", MockResponseWithEmptyTextProperty(), MockRequest(),
          ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
        ("MockResponseWithEmptyPowerConsumptionBlockId_500", MockResponseWithEmptyPowerConsumptionBlockId(),
         MockRequest(), ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
        ("MockResponseWithoutPowerConsumptionBlockId_500", MockResponseWithoutPowerConsumptionBlockId(),
         MockRequest(), ESBStatusCode.InternalServerError.value, ESBStatusCode.InternalServerError.name),
    ])
    @patch.object(RegisterConsumptionDataService, 'register_consumption')
    def test_should_return_status_500_if_response_does_not_have_some_data_of_created_consumption(self, name,
                                                                                         mock_response: IMockResponse,
                                                                                         mock_request: MockRequest,
                                                                                         expected_status_code,
                                                                                         expected_status_str,
                                                                                         mock_register_consumption):
        # Arrange
        mock_response.update_meter_id_and_power_consumption(mock_request.meter_id, mock_request.consumption_str)
        mock_register_consumption.return_value = mock_response
        register_consumption_service = RegisterConsumptionService()
        request = RegisterConsumptionRequest(mock_request.consumptions, mock_request.meter_id, mock_request.meter_contract_id)

        # Act
        response = register_consumption_service.register_consumption(request)

        # Assert
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.status_str, expected_status_str)
