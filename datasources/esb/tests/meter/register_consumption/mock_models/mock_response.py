import requests
from unittest import mock
from datasources.esb.helper.esb_status_code import ESBStatusCode


class IMockResponse(requests.Response):

    def update_meter_id_and_power_consumption(self, meter_id: str, power_consumption: str):
        """Update of parameters"""


class MockResponse(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{"date": {},' \
                             '"meter_id": "e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973fa4b6",' \
                             '"power_consumption_block_id": "e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973asd2",' \
                             '"power_consumption": [ ' \
                                        '{"start_time": 1612787629,' \
                                          '"end_time": 1632787629,' \
                                          '"consumption": 3.42},' \
                                        '{"start_time": 1642787629,' \
                                          '"end_time": 1652787629,' \
                                          '"consumption": 3.11},' \
                                        '{"start_time": 1662787629,' \
                                          '"end_time": 1672787629,' \
                                          '"consumption": 2.22}]}'
        type(self).text = mock.PropertyMock(return_value=self._content)

    def update_meter_id_and_power_consumption(self, meter_id: str, power_consumption: str):
        self._content: str = '{"date": {},' \
                         '"meter_id": "' + meter_id + '",' \
                         '"power_consumption_block_id": "e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973asd2",' \
                         '"power_consumption": ' + power_consumption + ' }'
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockBadRequestResponse(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.BadRequest.name
        self.status_code: int = ESBStatusCode.BadRequest.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithForbiddenStatus(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Forbidden.name
        self.status_code: int = ESBStatusCode.Forbidden.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithEmptyTextProperty(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = ''
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithEmptyPowerConsumptionBlockId(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{"date": {},' \
                             '"meter_id": "e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973fa4b6",' \
                             '"power_consumption_block_id": "",' \
                             '"power_consumption": [ ' \
                                        '{"start_time": 1612787629,' \
                                          '"end_time": 1632787629,' \
                                          '"consumption": 3.42},' \
                                        '{"start_time": 1642787629,' \
                                          '"end_time": 1652787629,' \
                                          '"consumption": 3.11},' \
                                        '{"start_time": 1662787629,' \
                                          '"end_time": 1672787629,' \
                                          '"consumption": 2.22}]}'
        type(self).text = mock.PropertyMock(return_value=self._content)

    def update_meter_id_and_power_consumption(self, meter_id: str, power_consumption: str):
        self._content: str = '{"date": {},' \
                         '"meter_id": "' + meter_id + '",' \
                         '"power_consumption_block_id": "",' \
                         '"power_consumption": ' + power_consumption + ' }'
        type(self).text = mock.PropertyMock(return_value=self._content)


class MockResponseWithoutPowerConsumptionBlockId(IMockResponse):
    def __init__(self):
        super().__init__()
        self.reason: str = ESBStatusCode.Created.name
        self.status_code: int = ESBStatusCode.Created.value
        self._content: str = '{"date": {},' \
                             '"meter_id": "e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973fa4b6",' \
                             '"power_consumption": [ ' \
                                        '{"start_time": 1612787629,' \
                                          '"end_time": 1632787629,' \
                                          '"consumption": 3.42},' \
                                        '{"start_time": 1642787629,' \
                                          '"end_time": 1652787629,' \
                                          '"consumption": 3.11},' \
                                        '{"start_time": 1662787629,' \
                                          '"end_time": 1672787629,' \
                                          '"consumption": 2.22}]}'
        type(self).text = mock.PropertyMock(return_value=self._content)

    def update_meter_id_and_power_consumption(self, meter_id: str, power_consumption: str):
        self._content: str = '{"date": {},' \
                         '"meter_id": "' + meter_id + '",' \
                         '"power_consumption": ' + power_consumption + ' }'
        type(self).text = mock.PropertyMock(return_value=self._content)


