from datasources.esb.api_managers.meter_api_manager.models.public.consumption_data import ConsumptionData
from datasources.esb.api_managers.meter_api_manager.register_consumption.models.public.register_consumption_request import \
    RegisterConsumptionRequest


class IMockRequest(RegisterConsumptionRequest):
    consumption_str = ''
    consumptions_dict = []
    consumptions = []
    meter_id = ''
    meter_contract_id = ''


class MockRequest(IMockRequest):
    consumptions = []
    consumption_str = '[{"start_time": 1612787629,' \
                          '"end_time": 1632787629,' \
                          '"consumption": 3.42},' \
                        '{"start_time": 1642787629,' \
                          '"end_time": 1652787629,' \
                          '"consumption": 3.11},' \
                        '{"start_time": 1662787629,' \
                          '"end_time": 1672787629,' \
                          '"consumption": 2.22}]'
    consumptions_dict = [
                      {
                        "start_time": 1612787629,
                        "end_time": 1632787629,
                        "consumption": 3.42
                      },
                      {
                        "start_time": 1642787629,
                        "end_time": 1652787629,
                        "consumption": 3.22
                      },
                      {
                        "start_time": 1662787629,
                        "end_time": 1672787629,
                        "consumption": 2.12
                      },
                    ]
    meter_id = 'e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973asd2'
    meter_contract_id = 'e1c5bb095bc7ee546735bebf7841b44f63953365b90f6b476777ff5e973asd2'

    def __init__(self):
        super().__init__(self.consumptions, self.meter_id, self.meter_contract_id)
        if len(self.consumptions) == 0:
            for consumption in self.consumptions_dict:
                obj_consumption = ConsumptionData(consumption['start_time'],
                                                  consumption['end_time'],
                                                  consumption['consumption'])
                self.consumptions.append(obj_consumption)


class MockBadRequest(IMockRequest):
    consumptions = []
    consumption_str = '[{"start_time": 1612787629,' \
                          '"end_time": 1632787629,' \
                          '"consumption": 3.42},' \
                        '{"start_time": 1642787629,' \
                          '"end_time": 1652787629,' \
                          '"consumption": 3.11},' \
                        '{"start_time": 1662787629,' \
                          '"end_time": 1672787629,' \
                          '"consumption": 2.22}]'
    consumptions_dict = [
                      {
                        "start_time": 1612787629,
                        "end_time": 1632787629,
                        "consumption": 3.42
                      },
                      {
                        "start_time": 1642787629,
                        "end_time": 1652787629,
                        "consumption": 3.22
                      },
                      {
                        "start_time": 1662787629,
                        "end_time": 1672787629,
                        "consumption": 2.12
                      },
                    ]

    def __init__(self):
        super().__init__(self.consumptions, self.meter_id, self.meter_contract_id)
        if len(self.consumptions) == 0:
            for consumption in self.consumptions_dict:
                obj_consumption = ConsumptionData(consumption['start_time'],
                                                  consumption['end_time'],
                                                  consumption['consumption'])
                self.consumptions.append(obj_consumption)


class MockBadConsumptionRequest(IMockRequest):
    consumptions = []
    consumption_str = '[{"start_time": -1612787629,' \
                          '"end_time": -1632787629,' \
                          '"consumption": 3.42},' \
                        '{"start_time": 1642787629,' \
                          '"end_time": -1652787629,' \
                          '"consumption": 3.11},' \
                        '{"start_time": 1662787629,' \
                          '"end_time": 1-672787629,' \
                          '"consumption": 2.22}]'
    consumptions_dict = [
                      {
                        "start_time": -1612787629,
                        "end_time": -1632787629,
                        "consumption": 3.42
                      },
                      {
                        "start_time": 1642787629,
                        "end_time": -1652787629,
                        "consumption": 3.22
                      },
                      {
                        "start_time": 1662787629,
                        "end_time": -1672787629,
                        "consumption": 2.12
                      },
                    ]

    def __init__(self):
        super().__init__(self.consumptions, self.meter_id, self.meter_contract_id)
        if len(self.consumptions) == 0:
            for consumption in self.consumptions_dict:
                obj_consumption = ConsumptionData(consumption['start_time'],
                                                  consumption['end_time'],
                                                  consumption['consumption'])
                self.consumptions.append(obj_consumption)
