class ESBSettings:
    BASE_URL: str = 'https://esb-integration.nurogate.com'
    X_ESB_USER: str = '7ccb862c572fbd7bfb6c2d7b9f57f30f7b48d4e43efe51806ff48630e16b13f6'
    X_ESB_PASSWORD: str = 'fe9c2c5276e24d558a1ee5a23978db22'

    @staticmethod
    def get_default_header():
        return {
            'accept': 'application/json',
            'X-Esb-User': ESBSettings.X_ESB_USER,
            'X-Esb-Password': ESBSettings.X_ESB_PASSWORD,
            'Content-Type': 'application/json',
        }

    @staticmethod
    def generate_header(x_esb_user: str, x_esb_password: str):
        return {
            'accept': 'application/json',
            'X-Esb-User': x_esb_user,
            'X-Esb-Password': x_esb_password,
            'Content-Type': 'application/json',
        }
