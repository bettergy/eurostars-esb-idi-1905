class EsbUserCredentials:
    esb_user_key: str
    esb_password: str

    def __init__(self, esb_user_key: str, esb_password: str):
        self.esb_user_key = esb_user_key
        self.esb_password = esb_password
