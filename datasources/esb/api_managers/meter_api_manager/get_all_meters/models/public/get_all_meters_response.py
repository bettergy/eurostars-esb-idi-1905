from typing import Optional
from datasources.esb.api_managers.meter_api_manager.models.public.registered_meter_data import RegisteredMeterData


class IGetAllMetersResponse:
    status_code: int
    status_str: str


class GetAllMetersSuccess(IGetAllMetersResponse):
    meters: [RegisteredMeterData]

    def __init__(self, status_code: int, status_str: str, meters: [RegisteredMeterData]):
        self.status_code = status_code
        self.status_str = status_str
        self.meters = meters


class GetAllMeterFailure(IGetAllMetersResponse):
    additional_info: Optional[str]

    def __init__(self, status_code: int, status_str: str, additional_info: Optional[str] = ''):
        self.status_code = status_code
        self.status_str = status_str
        self.additional_info = additional_info

