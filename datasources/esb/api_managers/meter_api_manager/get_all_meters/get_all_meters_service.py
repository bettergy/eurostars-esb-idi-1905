import json
from datasources.esb.esb_settings import ESBSettings
from datasources.esb.helper.esb_status_code import ESBStatusCode
from datasources.esb.api_managers.meter_api_manager.get_all_meters.get_all_meters_data_service import IGetAllMetersDataService, \
    GetAllMetersDataService
from datasources.esb.api_managers.meter_api_manager.get_all_meters.models.public.get_all_meters_response import GetAllMetersSuccess, \
    GetAllMeterFailure, IGetAllMetersResponse
from datasources.esb.api_managers.meter_api_manager.models.public.registered_meter_data import RegisteredMeterData


class GetAllMetersService:
    get_all_meters_data_service: IGetAllMetersDataService

    def __init__(self):
        self.get_all_meters_data_service = GetAllMetersDataService()

    def get_all_meters(self) -> IGetAllMetersResponse:
        try:

            headers = ESBSettings.get_default_header()
            response = self.get_all_meters_data_service.get_all_meters(headers=headers)

            if response.status_code != ESBStatusCode.Ok.value:
                return GetAllMeterFailure(status_code=response.status_code, status_str=response.reason)

            registered_meters = self.__extract_registered_meters(response)

            return GetAllMetersSuccess(response.status_code, response.reason, registered_meters)
        except Exception as e:
            return GetAllMeterFailure(status_code=ESBStatusCode.InternalServerError.value,
                                      status_str=ESBStatusCode.InternalServerError.name,
                                      additional_info=str(e))

    @staticmethod
    def __extract_registered_meters(response) -> [RegisteredMeterData]:
        context = json.loads(response.text)
        registered_meters: [RegisteredMeterData] = []
        for i in context:
            esb_meter_id = i['meter_id']
            esb_register_id = i['registrar_id']
            esb_provider_id = i.get('provider_id', None)
            zip = i.get('zip', None)
            region = i.get('region', None)
            city = i.get('city', None)
            enabled = i.get('enabled', None)

            registered_meter_data = RegisteredMeterData(id=esb_meter_id, registrar_id=esb_register_id,
                                                        provider_id=esb_provider_id, zip=zip, region=region,
                                                        city=city, enabled=enabled)
            registered_meters.append(registered_meter_data)

        return registered_meters
