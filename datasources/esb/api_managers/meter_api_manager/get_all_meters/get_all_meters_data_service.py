import requests
from abc import ABC, abstractmethod

from datasources.esb.esb_settings import ESBSettings


class IGetAllMetersDataService(ABC):

    @abstractmethod
    def get_all_meters(self, headers):
        """Return all meters created by the current user"""


class GetAllMetersDataService(IGetAllMetersDataService):

    def get_all_meters(self, headers):
        return requests.get(f'{ESBSettings.BASE_URL}/user/meters', headers=headers)
