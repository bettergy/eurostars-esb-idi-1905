from abc import ABC, abstractmethod

import requests

from datasources.esb.esb_settings import ESBSettings


class IGetRecommendationDataService(ABC):

    @abstractmethod
    def get_recommendation(self, headers, meter_id, payload=None, data=None):
        """Method that return recommendation of determinate meter"""


class GetRecommendationDataService(IGetRecommendationDataService):

    def get_recommendation(self, headers, meter_id, payload=None, data=None):
        return requests.get(f'{ESBSettings.BASE_URL}/meter/{meter_id}/saving-recommendation',
                            headers=headers, json=payload, data=data)
