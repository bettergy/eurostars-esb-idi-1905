class GetRecommendationRequest:
    esb_meter_id: str

    def __init__(self, esb_meter_id: str):
        self.esb_meter_id = esb_meter_id

    def is_valid(self):
        return bool(self.esb_meter_id.strip())
