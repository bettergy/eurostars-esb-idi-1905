from typing import Optional
from datasources.esb.api_managers.meter_api_manager.models.public.registered_consumption_data import RegisteredConsumptionData


class IRegisterConsumptionResponse:
    status_code: int
    status_str: str


class RegisterConsumptionSuccess(IRegisterConsumptionResponse):
    registered_consumption_data: RegisteredConsumptionData

    def __init__(self, status_code: int, status_str: str, registered_consumption_data: RegisteredConsumptionData):
        self.status_code = status_code
        self.status_str = status_str
        self.registered_consumption_data = registered_consumption_data


class RegisterConsumptionFailure(IRegisterConsumptionResponse):
    additional_info: Optional[str]

    def __init__(self, status_code: int, status_str: str, additional_info: Optional[str] = ''):
        self.status_code = status_code
        self.status_str = status_str
        self.additional_info = additional_info
