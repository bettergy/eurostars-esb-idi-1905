from datasources.esb.api_managers.meter_api_manager.models.public.consumption_data import ConsumptionData


class RegisterConsumptionRequest:
    consumptions: [ConsumptionData]
    meter_id: str
    meter_contract_id: str

    def __init__(self, consumptions: [ConsumptionData], meter_id: str, meter_contract_id: str):
        self.consumptions = consumptions
        self.meter_id = meter_id
        self.meter_contract_id = meter_contract_id

    def is_valid(self) -> bool:
        try:
            [consumption.check_data() for consumption in self.consumptions]
        except:
            return False
        return bool(self.meter_id.strip()) and bool(self.meter_contract_id.strip())

    def get_json_payload(self):
        return [consumption.__dict__ for consumption in self.consumptions]

    def get_url_settings(self):
        return {
            'meter_id': self.meter_id,
            'contract_id': self.meter_contract_id
        }
