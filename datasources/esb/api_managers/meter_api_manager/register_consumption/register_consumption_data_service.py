import requests
from abc import ABC, abstractmethod
from datasources.esb.esb_settings import ESBSettings


class IRegisterConsumptionDataService(ABC):

    @abstractmethod
    def register_consumption(self, headers, payload, meter_id, meter_contract_id, data=None):
        """Method that register new consumption"""


class RegisterConsumptionDataService(IRegisterConsumptionDataService):

    def register_consumption(self, headers, payload, meter_id, meter_contract_id, data=None):
        return requests.post(f'{ESBSettings.BASE_URL}/meter/{meter_id}/consumption?meterContract={meter_contract_id}',
                             headers=headers, json=payload, data=data)
