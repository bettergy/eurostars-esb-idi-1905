import json
from datasources.esb.api_managers.meter_api_manager.models.public.consumption_data import ConsumptionData
from datasources.esb.api_managers.meter_api_manager.models.public.registered_consumption_data import RegisteredConsumptionData
from datasources.esb.api_managers.meter_api_manager.register_consumption.models.public.register_consumption_request import \
    RegisterConsumptionRequest
from datasources.esb.api_managers.meter_api_manager.register_consumption.models.public.register_consumption_response import \
    IRegisterConsumptionResponse, RegisterConsumptionFailure, RegisterConsumptionSuccess
from datasources.esb.api_managers.meter_api_manager.register_consumption.register_consumption_data_service import \
    IRegisterConsumptionDataService, RegisterConsumptionDataService
from datasources.esb.esb_settings import ESBSettings
from datasources.esb.helper.esb_status_code import ESBStatusCode


class RegisterConsumptionService:
    register_consumption_data_service: IRegisterConsumptionDataService

    def __init__(self):
        self.register_consumption_data_service = RegisterConsumptionDataService()

    def register_consumption(self, request: RegisterConsumptionRequest) -> IRegisterConsumptionResponse:
        is_valid_request = request.is_valid()

        if is_valid_request:
            try:
                headers = ESBSettings.get_default_header()
                payload = request.get_json_payload()
                response = self.register_consumption_data_service.register_consumption(headers=headers,
                                                                                       payload=payload,
                                                                                       meter_id=request.meter_id,
                                                                                       meter_contract_id=request.meter_contract_id)

                if response.status_code != ESBStatusCode.Created.value:
                    return RegisterConsumptionFailure(status_code=response.status_code, status_str=response.reason)

                registered_consumption_data = self.__extract_registered_consumption_data(response)

                registered_consumption_data.check_data()

                return RegisterConsumptionSuccess(response.status_code, response.reason, registered_consumption_data)
            except Exception as e:
                return RegisterConsumptionFailure(status_code=ESBStatusCode.InternalServerError.value,
                                                  status_str=ESBStatusCode.InternalServerError.name,
                                                  additional_info=str(e))
        else:
            return RegisterConsumptionFailure(status_code=ESBStatusCode.BadRequest.value,
                                              status_str=ESBStatusCode.BadRequest.name)

    @staticmethod
    def __extract_registered_consumption_data(response) -> RegisteredConsumptionData:
        contract_data = json.loads(response.text)
        date = contract_data['date']
        meter_id = contract_data['meter_id']
        power_consumption_block_id = contract_data['power_consumption_block_id']
        array_of_dict_consumptions = contract_data['power_consumption']

        array_of_obj_consumptions: [ConsumptionData] = []
        for consumption in array_of_dict_consumptions:
            obj_consumption = ConsumptionData(consumption['start_time'],
                                              consumption['end_time'],
                                              consumption['consumption'])

            array_of_obj_consumptions.append(obj_consumption)

        registered_consumption_data = RegisteredConsumptionData(date=date,
                                                                meter_id=meter_id,
                                                                power_consumption_block_id=power_consumption_block_id,
                                                                power_consumption=array_of_obj_consumptions)
        return registered_consumption_data
