import json
import requests
from datasources.esb.esb_settings import ESBSettings
from datasources.esb.helper.esb_status_code import ESBStatusCode
from datasources.esb.api_managers.meter_api_manager.models.public.recommendation_data import FinancialModelData, RecommendationData
from datasources.esb.api_managers.meter_api_manager.models.public.registered_recommendation_data import RegisteredRecommendationData
from datasources.esb.api_managers.meter_api_manager.register_recommendation.models.public.register_recommendation_request import \
    RegisterRecommendationRequest
from datasources.esb.api_managers.meter_api_manager.register_recommendation.models.public.register_recommendation_response import \
    RegisterRecommendationFailure, RegisterRecommendationSuccess, IRegisterRecommendationResponse
from datasources.esb.api_managers.meter_api_manager.register_recommendation.register_recommendation_data_service import \
    RegisterRecommendationDataService


class RegisterRecommendationService:
    register_recommendation_data_service: RegisterRecommendationDataService

    def __init__(self):
        self.register_recommendation_data_service = RegisterRecommendationDataService()

    def register_recommendation(self, request: RegisterRecommendationRequest) -> IRegisterRecommendationResponse:
        is_valid_request = request.is_valid()

        if is_valid_request:
            try:
                headers = ESBSettings.get_default_header()
                payload = request.get_json_payload()

                response = self.register_recommendation_data_service.register_recommendation(headers=headers,
                                                                                             payload=payload,
                                                                                             meter_id=request.meter_id)

                if response.status_code != ESBStatusCode.Created.value:
                    return RegisterRecommendationFailure(status_code=response.status_code, status_str=response.reason)

                registered_recommendation_data = self.__extract_registered_recommendation_data(response)

                registered_recommendation_data.check_data()

                return RegisterRecommendationSuccess(response.status_code, response.reason,
                                                     registered_recommendation_data)
            except Exception as e:
                return RegisterRecommendationFailure(status_code=ESBStatusCode.InternalServerError.value,
                                                     status_str=ESBStatusCode.InternalServerError.name,
                                                     additional_info=str(e))
        else:
            return RegisterRecommendationFailure(status_code=ESBStatusCode.BadRequest.value,
                                                 status_str=ESBStatusCode.BadRequest.name)

    def __extract_registered_recommendation_data(self, response: requests.Response) -> RegisteredRecommendationData:
        contract_data = json.loads(response.text)
        meter_id = contract_data['meter_id']
        timestamp = contract_data['timestamp']
        user = contract_data['user']
        recommendation = contract_data['recommendation']
        recommendation = self.__extract_recommendation_data(recommendation)

        return RegisteredRecommendationData(meter_id=meter_id,user=user, recommendation=recommendation,
                                            timestamp=timestamp)

    def __extract_recommendation_data(self, recommendation: {}) -> RecommendationData:
        energy_saving = recommendation['energy_saving']
        cost_savings = recommendation['cost_savings']
        investment = recommendation['investment']
        payback = recommendation['payback']
        iir_recommended = recommendation['iir_recommended']
        iir_optimum = recommendation['iir_optimum']
        irr_minimum = recommendation['irr_minimum']
        minimum_peak_power = recommendation['minimum_peak_power']
        maximum_peak_power = recommendation['maximum_peak_power']
        peak_power_recommended = recommendation['peak_power_recommended']
        peak_power_optimum = recommendation['peak_power_optimum']
        financial_model = recommendation['financial']
        financial_model = self.__extract_financial_model_data(financial_model)

        return RecommendationData(energy_saving, cost_savings, investment, payback, iir_recommended, iir_optimum,
                                  irr_minimum, minimum_peak_power, maximum_peak_power, peak_power_recommended,
                                  peak_power_optimum, financial_model)

    @staticmethod
    def __extract_financial_model_data(financial_model: {}) -> FinancialModelData:
        num_years = financial_model['num_years']
        irr = financial_model['irr']
        van = financial_model['van']
        payback = financial_model['payback']
        irr_7 = financial_model['irr_7']
        van_7 = financial_model['van_7']
        payback_7 = financial_model['payback_7']
        irr_10 = financial_model['irr_10']
        van_10 = financial_model['van_10']
        payback_10 = financial_model['payback_10']
        om = financial_model['om']
        rent_ing = financial_model['rent-ing']
        insur_ance = financial_model['insur-ance']
        self_generation = financial_model['self_generation']
        fee_licence = financial_model['fee_licence']
        construction_tax = financial_model['construction_tax']
        investment_installation = financial_model['investment_installation']
        equip_ment_replacement = financial_model['equip-ment_replacement']
        subven_tion = financial_model['subven-tion']
        ener_gy_saving = financial_model['ener-gy_saving']
        ener_gy_sales = financial_model['ener-gy_sales']
        pow_er_optimization = financial_model['pow-er_optimization']
        cashflow = financial_model['cashflow']
        cashflow_without_update = financial_model['cashflow_without_update']
        van_net_updated = financial_model['van_net_updated']
        cost_10_years_financing = financial_model['cost_10_years_financing']
        saving_10_years_financing = financial_model['saving_10_years_financing']
        cost_7_years_financing = financial_model['cost_7_years_financing']
        van_7_years_financing = financial_model['van_7_years_financing']
        saving_7_years_financing = financial_model['saving_7_years_financing']
        cost_10_years_with_subvention = financial_model['cost_10_years_with_subvention']

        return FinancialModelData(num_years, irr, van, payback, irr_7, van_7,
                                  payback_7, irr_10, van_10, payback_10, om, rent_ing,
                                  insur_ance, self_generation, fee_licence, construction_tax,
                                  investment_installation, equip_ment_replacement, subven_tion,
                                  ener_gy_saving, ener_gy_sales, pow_er_optimization, cashflow,
                                  cashflow_without_update, van_net_updated, cost_10_years_financing,
                                  saving_10_years_financing, cost_7_years_financing, van_7_years_financing,
                                  saving_7_years_financing, cost_10_years_with_subvention)
