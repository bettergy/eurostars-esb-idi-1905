import requests
from abc import ABC, abstractmethod
from datasources.esb.esb_settings import ESBSettings


class IRegisterRecommendationDataService(ABC):

    @abstractmethod
    def register_recommendation(self, headers, payload, meter_id, data=None) -> requests.Response:
        """Method that should register new recommendation for meter"""


class RegisterRecommendationDataService(IRegisterRecommendationDataService):

    def register_recommendation(self, headers, payload, meter_id, data=None) -> requests.Response:
        return requests.post(f'{ESBSettings.BASE_URL}/meter/{meter_id}/saving-recommendation',
                             headers=headers, json=payload, data=data)
