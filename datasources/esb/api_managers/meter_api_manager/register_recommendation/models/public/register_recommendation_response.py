from typing import Optional
from datasources.esb.api_managers.meter_api_manager.models.public.registered_recommendation_data import RegisteredRecommendationData


class IRegisterRecommendationResponse:
    status_code: int
    status_str: str


class RegisterRecommendationSuccess(IRegisterRecommendationResponse):
    registered_consumption_data: RegisteredRecommendationData

    def __init__(self, status_code: int, status_str: str, registered_consumption_data: RegisteredRecommendationData):
        self.status_code = status_code
        self.status_str = status_str
        self.registered_consumption_data = registered_consumption_data


class RegisterRecommendationFailure(IRegisterRecommendationResponse):
    additional_info: Optional[str]

    def __init__(self, status_code: int, status_str: str, additional_info: Optional[str] = ''):
        self.status_code = status_code
        self.status_str = status_str
        self.additional_info = additional_info
