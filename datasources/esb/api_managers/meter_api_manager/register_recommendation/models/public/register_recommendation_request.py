from datasources.esb.api_managers.meter_api_manager.models.public.recommendation_data import RecommendationData


class RegisterRecommendationRequest:
    meter_id: str
    recommendation: RecommendationData

    def __init__(self, meter_id: str, recommendation: RecommendationData):
        self.meter_id = meter_id
        self.recommendation = recommendation

    def is_valid(self) -> bool:
        try:
            self.recommendation.check_data()
        except:
            return False
        return bool(self.meter_id.strip())

    def get_json_payload(self):
        recommendation_dict = self.recommendation.__dict__
        recommendation_dict['financial_model'] = self.recommendation.financial_model.__dict__
        recommendation_dict = self.change_dict_keys(recommendation_dict)
        return recommendation_dict

    def change_dict_keys(self, recommendation_dict):
        financial_model = recommendation_dict['financial_model']
        financial_model['rent-ing'] = financial_model.pop('rent_ing')
        financial_model['insur-ance'] = financial_model.pop('insur_ance')
        financial_model['equip-ment_replacement'] = financial_model.pop('equip_ment_replacement')
        financial_model['subven-tion'] = financial_model.pop('subven_tion')
        financial_model['ener-gy_saving'] = financial_model.pop('ener_gy_saving')
        financial_model['ener-gy_sales'] = financial_model.pop('ener_gy_sales')
        financial_model['pow-er_optimization'] = financial_model.pop('pow_er_optimization')
        return recommendation_dict
