class FinancialModelData:
    num_years: int
    irr: float
    van: float
    payback: float
    irr_7: float
    van_7: float
    payback_7: float
    irr_10: float
    van_10: float
    payback_10: float
    om: [float]
    rent_ing: [float]
    insur_ance: [float]
    self_generation: [float]
    fee_licence: [float]
    construction_tax: [float]
    investment_installation: [float]
    equip_ment_replacement: [float]
    subven_tion: [float]
    ener_gy_saving: [float]
    ener_gy_sales: [float]
    pow_er_optimization: [float]
    cashflow: [float]
    cashflow_without_update: [float]
    van_net_updated: [float]
    cost_10_years_financing: [float]
    saving_10_years_financing: [float]
    cost_7_years_financing: [float]
    van_7_years_financing: [float]
    saving_7_years_financing: [float]
    cost_10_years_with_subvention: [float]

    def __init__(self, num_years: int, irr: float, van: float, payback: float, irr_7: float, van_7: float,
                 payback_7: float, irr_10: float, van_10: float, payback_10: float, om: [float], rent_ing: [float],
                 insur_ance: [float], self_generation: [float], fee_licence: [float], construction_tax: [float],
                 investment_installation: [float], equip_ment_replacement: [float], subven_tion: [float],
                 ener_gy_saving: [float], ener_gy_sales: [float], pow_er_optimization: [float], cashflow: [float],
                 cashflow_without_update: [float], van_net_updated: [float], cost_10_years_financing: [float],
                 saving_10_years_financing: [float], cost_7_years_financing: [float], van_7_years_financing: [float],
                 saving_7_years_financing: [float], cost_10_years_with_subvention: [float]) -> None:
        self.num_years = num_years
        self.irr = irr
        self.van = van
        self.payback = payback
        self.irr_7 = irr_7
        self.van_7 = van_7
        self.payback_7 = payback_7
        self.irr_10 = irr_10
        self.van_10 = van_10
        self.payback_10 = payback_10
        self.om = om if om else [0.0]
        self.rent_ing = rent_ing if rent_ing else [0.0]
        self.insur_ance = insur_ance if insur_ance else [0.0]
        self.self_generation = self_generation if self_generation else [0.0]
        self.fee_licence = fee_licence if fee_licence else [0.0]
        self.construction_tax = construction_tax if construction_tax else [0.0]
        self.investment_installation = investment_installation if investment_installation else [0.0]
        self.equip_ment_replacement = equip_ment_replacement if equip_ment_replacement else [0.0]
        self.subven_tion = subven_tion if subven_tion else [0.0]
        self.ener_gy_saving = ener_gy_saving if ener_gy_saving else [0.0]
        self.ener_gy_sales = ener_gy_sales if ener_gy_sales else [0.0]
        self.pow_er_optimization = pow_er_optimization if pow_er_optimization else [0.0]
        self.cashflow = cashflow if cashflow else [0.0]
        self.cashflow_without_update = cashflow_without_update if cashflow_without_update else [0.0]
        self.van_net_updated = van_net_updated if van_net_updated else [0.0]
        self.cost_10_years_financing = cost_10_years_financing if cost_10_years_financing else [0.0]
        self.saving_10_years_financing = saving_10_years_financing if saving_10_years_financing else [0.0]
        self.cost_7_years_financing = cost_7_years_financing if cost_7_years_financing else [0.0]
        self.van_7_years_financing = van_7_years_financing if van_7_years_financing else [0.0]
        self.saving_7_years_financing = saving_7_years_financing if saving_7_years_financing else [0.0]
        self.cost_10_years_with_subvention = cost_10_years_with_subvention if cost_10_years_with_subvention else [0.0]

    def check_data(self) -> None:
        attrs = self.__dict__
        for attr in attrs:
            if type(attrs.get(attr)) == str and not bool(attrs.get(attr).strip()):
                raise Exception('Empty string value')

    @staticmethod
    def fill_data(financial):
         financial_model = FinancialModelData(financial.num_years, financial.irr, financial.van, financial.payback,
                                              financial.irr_7, financial.van_7, financial.payback_7, financial.irr_10,
                                              financial.van_10, financial.payback_10, financial.om,
                                              financial.renting, financial.insurance,
                                              financial.self_generation, financial.fee_licence,
                                              financial.construction_tax, financial.investment_installation,
                                              financial.equipment_replacement, financial.subvention,
                                              financial.energy_saving, financial.energy_sales,
                                              financial.power_optimization, financial.cashflow,
                                              financial.cashflow_without_update, financial.van_net_updated,
                                              financial.cost_10_years_financing, financial.saving_10_years_financing,
                                              financial.cost_7_years_financing, financial.van_7_years_financing,
                                              financial.saving_7_years_financing, financial.cost_10_years_with_subvention)
         return financial_model

    def __repr__(self):
        return str(self.__dict__)


class RecommendationData:
    energy_saving: float
    cost_savings: float
    investment: float
    payback: float
    iir_recommended: float
    iir_optimum: float
    irr_minimum: float
    minimum_peak_power: float
    maximum_peak_power: float
    peak_power_recommended: float
    peak_power_optimum: float
    financial_model: FinancialModelData

    def __init__(self, energy_saving: float, cost_savings: float, investment: float, payback: float,
                 iir_recommended: float, iir_optimum: float, irr_minimum: float, minimum_peak_power: float,
                 maximum_peak_power: float, peak_power_recommended: float, peak_power_optimum: float,
                 financial_model: FinancialModelData, ) -> None:
        self.energy_saving = energy_saving
        self.cost_savings = cost_savings
        self.investment = investment
        self.payback = payback
        self.iir_recommended = iir_recommended
        self.iir_optimum = iir_optimum
        self.irr_minimum = irr_minimum
        self.minimum_peak_power = minimum_peak_power
        self.maximum_peak_power = maximum_peak_power
        self.peak_power_recommended = peak_power_recommended
        self.peak_power_optimum = peak_power_optimum
        self.financial_model = financial_model

    def check_data(self) -> None:
        self.financial_model.check_data()

    @staticmethod
    def fill_data(reco, financial):
        reco_data = RecommendationData(reco.energy_saving, reco.cost_savings, reco.investment, reco.payback,
                                       reco.params['recommended_irr'], reco.params['optimum_irr'],
                                       reco.params['minimum_tir'], reco.parameters.min_peak_power,
                                       reco.parameters.max_peak_power_without_energy_sold,
                                       reco.params['recommended_peak_power'], reco.params['optimum_peak_power'],
                                       financial)
        return reco_data



