from typing import Any

from datasources.esb.api_managers.meter_api_manager.models.public.consumption_data import ConsumptionData


class RegisteredConsumptionData:
    date: Any
    meter_id: str
    power_consumption_block_id: str
    power_consumption: [ConsumptionData]

    def __init__(self, date: Any, meter_id: str, power_consumption_block_id: str, power_consumption: [ConsumptionData]):
        self.date = date
        self.meter_id = meter_id
        self.power_consumption_block_id = power_consumption_block_id
        self.power_consumption = power_consumption

    def check_data(self) -> None:
        if not bool(self.meter_id.strip()) or not bool(self.power_consumption_block_id.strip()):
            raise Exception('Empty value')
