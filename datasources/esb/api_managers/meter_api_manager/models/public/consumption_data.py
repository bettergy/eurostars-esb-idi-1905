
class ConsumptionData:
    start_time: int
    end_time: int
    consumption: float

    def __init__(self, start_time: int, end_time: int,  consumption: float):
        self.start_time = start_time
        self.end_time = end_time
        self.consumption = consumption

    def check_data(self) -> None:
        if self.consumption < 0 or self.start_time < 0 or self.end_time < 0:
            raise Exception('Empty value')
