from typing import Optional
from datasources.esb.api_managers.meter_api_manager.models.public.recommendation_data import RecommendationData


class RegisteredRecommendationData:
    meter_id: str
    user: str
    timestamp: Optional[int]
    recommendation:  RecommendationData

    def __init__(self, meter_id: str, user: str,
                 recommendation: RecommendationData, timestamp: Optional[int] = 0):
        self.meter_id = meter_id
        self.user = user
        self.recommendation = recommendation
        self.timestamp = timestamp

    def check_data(self):
        if not bool(self.meter_id.strip()) or not bool(self.user.strip()):
            raise Exception('Empty value')
