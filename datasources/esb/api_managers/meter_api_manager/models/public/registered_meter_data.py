from typing import Optional


class RegisteredMeterData:
    id: str
    registrar_id: str
    provider_id: str
    zip: Optional[str]
    region: Optional[str]
    city: Optional[str]
    enabled: Optional[bool]

    def __init__(self, id, registrar_id, provider_id, zip, region, city, enabled):
        self.id = id
        self.registrar_id = registrar_id
        self.provider_id = provider_id
        self.zip = zip
        self.region = region
        self.city = city
        self.enabled = enabled

    def check_data(self) -> None:
        if not bool(self.id.strip()) or not bool(self.registrar_id.strip()) or not bool(self.provider_id.strip()):
            raise Exception('Empty value')
