import json
from datasources.esb.helper.esb_status_code import ESBStatusCode
from datasources.esb.api_managers.meter_api_manager.register_meter.models.public.register_meter_request import RegisterMeterRequest
from datasources.esb.api_managers.meter_api_manager.register_meter.models.public.register_meter_response import IRegisterMeterResponse, \
    RegisterMeterSuccess, RegisterMeterFailure
from datasources.esb.api_managers.meter_api_manager.models.public.registered_meter_data import RegisteredMeterData
from datasources.esb.api_managers.meter_api_manager.register_meter.register_meter_data_service import IRegisterMeterDataService, \
    RegisterMeterDataService


class RegisterMeterService:
    register_meter_data_service: IRegisterMeterDataService

    def __init__(self):
        self.register_meter_data_service = RegisterMeterDataService()

    def register_meter(self, request: RegisterMeterRequest) -> IRegisterMeterResponse:
        is_valid_request = request.is_valid()

        if is_valid_request:
            try:
                headers = request.get_headers()
                payload = request.get_json_payload()

                response = self.register_meter_data_service.register_meter(headers=headers, payload=payload)

                if response.status_code != ESBStatusCode.Created.value:
                    return RegisterMeterFailure(status_code=response.status_code, status_str=response.reason)

                registered_meter_data = self.__extract_registered_meter_data(response)

                registered_meter_data.check_data()

                return RegisterMeterSuccess(response.status_code, response.reason, registered_meter_data)
            except Exception as e:
                return RegisterMeterFailure(status_code=ESBStatusCode.InternalServerError.value,
                                            status_str=ESBStatusCode.InternalServerError.name,
                                            additional_info=str(e))
        else:
            return RegisterMeterFailure(status_code=ESBStatusCode.BadRequest.value,
                                        status_str=ESBStatusCode.BadRequest.name,
                                        additional_info='Invalid request data')

    @staticmethod
    def __extract_registered_meter_data(response):
        meter_data = json.loads(response.text)
        esb_meter_id = meter_data['meter_id']
        esb_register_id = meter_data['registrar_id']
        esb_provider_id = meter_data['provider_id']
        zip = meter_data.get('zip', None)
        region = meter_data.get('region', None)
        city = meter_data.get('city', None)
        enabled = meter_data.get('enabled', None)
        registered_meter_data = RegisteredMeterData(id=esb_meter_id, registrar_id=esb_register_id,
                                                    provider_id=esb_provider_id, zip=zip, region=region,
                                                    city=city, enabled=enabled)
        return registered_meter_data
