from abc import ABC, abstractmethod
import requests
from datasources.esb.esb_settings import ESBSettings


class IRegisterMeterDataService(ABC):

    @abstractmethod
    def register_meter(self, headers, payload, data=None) -> requests.Response:
        """Method that registers a new meter"""


class RegisterMeterDataService(IRegisterMeterDataService):

    def register_meter(self, headers, payload, data=None) -> requests.Response:
        return requests.post(f'{ESBSettings.BASE_URL}/meter', headers=headers, json=payload, data=data)
