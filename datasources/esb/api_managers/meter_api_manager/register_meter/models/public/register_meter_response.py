from typing import Optional

from datasources.esb.api_managers.meter_api_manager.models.public.registered_meter_data import RegisteredMeterData


class IRegisterMeterResponse:
    status_code: int
    status_str: str


class RegisterMeterSuccess(IRegisterMeterResponse):
    registered_meter_data: RegisteredMeterData

    def __init__(self, status_code: int, status_str: str, registered_meter_data: RegisteredMeterData):
        self.status_code = status_code
        self.status_str = status_str
        self.registered_meter_data = registered_meter_data


class RegisterMeterFailure(IRegisterMeterResponse):
    additional_info: Optional[str]

    def __init__(self, status_code: int, status_str: str, additional_info: Optional[str] = ''):
        self.status_code = status_code
        self.status_str = status_str
        self.additional_info = additional_info
