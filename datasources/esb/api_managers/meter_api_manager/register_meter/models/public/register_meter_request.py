from typing import Dict
from datasources.esb.esb_settings import ESBSettings
from datasources.esb.models.public.EsbUserCredentials import EsbUserCredentials


class RegisterMeterRequest:
    zip: str
    region: str
    city: str
    esb_user_credentials: EsbUserCredentials

    def __init__(self, zip: str, region: str, city: str, esb_user_credentials: EsbUserCredentials):
        self.zip = zip if zip is not None else 'null'
        self.region = region
        self.city = city if city is not None else 'null'
        self.esb_user_credentials = esb_user_credentials

    def is_valid(self) -> bool:
        return self.zip is not None \
               and self.region is not None\
               and self.city is not None

    def get_json_payload(self) -> Dict[str, str]:
        return {
            "zip": self.zip,
            "region": self.region,
            "city": self.city
        }

    def get_headers(self) -> Dict[str, str]:
        return ESBSettings.generate_header(self.esb_user_credentials.esb_user_key,
                                           self.esb_user_credentials.esb_password)
