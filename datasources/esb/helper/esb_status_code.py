from enum import Enum


class ESBStatusCode(Enum):
    Created = 201
    Ok = 200
    BadRequest = 400
    Forbidden = 403
    InternalServerError = 500
